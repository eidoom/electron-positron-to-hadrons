(* ::Package:: *)

(* ::Title:: *)
(*Dimensional regularisation of infrared divergences by the dipole subtraction method*)


(* ::Chapter:: *)
(*Set up*)


H=3*(4*Pi)^(2*eps)*(-2+eps)*(-1+eps)*(s12)^(-2*eps)/Gamma[4-2*eps]
Series[H,{eps,0,0}]
preFactor=CF*\[Alpha]s/2/Pi


(* ::Chapter:: *)
(*Global counterterms (integrated subtraction terms)*)


(* ::Section:: *)
(*Soft, s_5*)


softIntegrand=CF*\[Alpha]s*Power[2,2*eps]*d[sh45]*d[sh35]*Power[Pi,eps-1]/Gamma[1-eps]*Power[sh45*sh35,-eps-1]*Power[-s34,-eps]


tmp0=Integrate[softIntegrand/d[sh45],{sh45,0,1}, Assumptions->Re[eps]<0]


tmp1=Integrate[tmp0/d[sh35],{sh35,0,1}, Assumptions->Re[eps]<0]//Factor


tmp2=tmp1/preFactor/H/.s34->s12//Simplify[#,Assumptions->s12>0]&


softIncl=Series[tmp2,{eps,0,0}]//Simplify


(* ::Section:: *)
(*subCol, (1-s_5)c_{45}*)


solidAngle=d[Omega,n_]:>2 Power[Pi,(n+1)/2]/Gamma[(n+1)/2];
d[Phi4parallel5]=Power[2*Pi,2*eps-2]/4*d[Omega,1-2*eps]*Power[-s45,-eps]*Power[z*(1-z),-eps]*d[z]/.solidAngle


subCol0=-4*\[Alpha]s*CF/s45*(1+z)*d[Phi4parallel5]*d[s45]*Power[2*Pi,2*eps]


Integrate[subCol0/d[z],{z,0,1},Assumptions->Re[eps]<1];
Integrate[%/d[s45],{s45,0,s12},Assumptions->Re[eps]<0];
subCol1=%


subCol1/preFactor/H
subColIncl=Series[%,{eps,0,0}]//FullSimplify


(* ::Section:: *)
(*Cross section*)


2*subColIncl+softIncl

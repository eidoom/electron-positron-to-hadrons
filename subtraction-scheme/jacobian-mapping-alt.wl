(* ::Package:: *)

(* ::Section:: *)
(*Calculate Jacobian with invariant w' s*)


pt4[m_]:=(1+w3)p4[m];
pt35[m_]:=p35[m]-w3*p4[m];


SetAttributes[dot,Orderless];
doDot={dot[a_,b_+c_]:>dot[a,b]+dot[a,c],dot[a_,b_]:>a[0]*b[0]-Plus@@(a[#]*b[#]&/@Range[3])};


P[i_]:=If[i>3,p35[i-4],p4[i]];
Pt[i_]:=If[i>3,pt35[i-4],pt4[i]];
Jm=Table[D[Pt[i],P[j]],{i,0,7},{j,0,7}];
Jm//MatrixForm


det=Det[Jm]


(* ::Section:: *)
(*Test phase space volume*)


PS=1/8/Pi;
PS//N


LHS=1/16/Pi Integrate[Sin[\[Theta]],{\[Theta],0,Pi}]


Gamma[3/2]


minkowskiMetric=DiagonalMatrix[{1,-1,-1,-1}];
performMomentaDots=dot[x_,y_]:>x.minkowskiMetric.y;
zHat={0,0,1};
Rx[a_]:={{1,0,0},{0,Cos[a],-Sin[a]},{0,Sin[a],Cos[a]}};
Ry[a_]:={{Cos[a],0,Sin[a]},{0,1,0},{-Sin[a],0,Cos[a]}};
Rz[a_]:={{Cos[a],-Sin[a],0},{Sin[a],Cos[a],0},{0,0,1}};
p[1]=Sqrt[s12]/2*Prepend[zHat,-1];
p[2]=Sqrt[s12]/2*Prepend[-zHat,-1];
p[3]=Sqrt[s12]/2*Prepend[Ry[\[Theta]].zHat,1];

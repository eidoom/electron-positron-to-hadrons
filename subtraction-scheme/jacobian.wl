(* ::Package:: *)

(* ::Subchapter:: *)
(*I*)


(* ::Section::Closed:: *)
(*Calculation*)


zIntegrand = Power[z,-epsilon]*(1+Power[z,2])*Power[1-z,-1-epsilon];
zIntegral = Integrate[zIntegrand, {z,0,1}, Assumptions->Re[epsilon]<0];
sIntegrand = Power[-s45,-1-epsilon];
sIntegral = Integrate[sIntegrand,{s45,0,s12}, Assumptions->Re[epsilon]<0];
sigmaCollinear = -Power[2,-1+4*epsilon]*Power[Pi,-1+3*epsilon]/Gamma[1-epsilon]*\[Alpha]s*CF*sIntegral*zIntegral//Factor;
Print[sigmaCollinear]


preFactor=Power[-s12,-epsilon]*Power[2,4*epsilon]*Power[Pi,3*epsilon]/Gamma[1-epsilon];
coefficientCollinear=Power[2Pi,-1]*\[Alpha]s*CF;
Series[preFactor,{epsilon,0,0}]
sigmaCollinear//Factor;
%/preFactor/coefficientCollinear;
collinear = %//Series[#,{epsilon,0,0}]&//FullSimplify


(* ::Subchapter:: *)
(*R*)


(* ::Section::Closed:: *)
(*Set - up*)


SetAttributes[dot,Orderless];
SetAttributes[s,Orderless];
unpackMomenta=p[i_,j__]:>p[i]+p[j];
sToDot=s[i_,j_]:>2*dot[p[i],p[j]];
minkowskiMetric=DiagonalMatrix[{1,-1,-1,-1}];
performMomentaDots=dot[x_,y_]:>x.minkowskiMetric.y;
zHat ={0,0,1};
Rx[a_] := {{1,0,0},{0,Cos[a],-Sin[a]},{0,Sin[a],Cos[a]}};
Ry[a_]:={{Cos[a],0,Sin[a]},{0,1,0},{-Sin[a],0,Cos[a]}};
Rz[a_]:={{Cos[a],-Sin[a],0},{Sin[a],Cos[a],0},{0,0,1}};
momentumConservation[{a__},n_]:=p[a]->-p@@Delete[Range@n,{#}&/@{a}]//.unpackMomenta;
fixedMomenta={
	p[1]->Sqrt[s12]/2 Prepend[zHat,-1],
	p[2]->Sqrt[s12]/2 Prepend[-zHat,-1],
	p[3]->Sqrt[s12]/2 x[3] Prepend[Ry[\[Theta]].zHat,1],
	p[4]->Sqrt[s12]/2 x[4] Prepend[Ry[\[Theta]].Rz[\[Alpha]].Ry[\[Beta]].zHat,1]
};
replaceMomenta=Join[fixedMomenta,{momentumConservation[{5},5]/.fixedMomenta}];
cosBeta=1+2*(1-x[3]-x[4])/(x[3]*x[4]);
rules={s[a_,p4t]:>(1+s[3,5]/(s[3,4]+s[4,5]))s[a,4],z3->s[3,4]/(s[3,4]+s[4,5])};


measure=(2^(4-3 n) \[Pi]^(1/2-n) d[Cos[\[Alpha]]] d[Cos[\[Theta]]] d[x[3]] d[x[4]] s12^(-3+n) Sin[\[Alpha]]^(-5+n) Sin[\[Theta]]^(-4+n) ((1-x[3])(1-x[4])(-1+x[3]+x[4]))^(-2+n/2))/(Gamma[1/2 (-3+n)] Gamma[1/2 (-2+n)])/.n->4-2epsilon//Factor;
integrandS=-3*Power[4*Pi,3]/Power[s12,2]*Power[1+s[4,5]/(s[3,4]+s[3,5]),2]*(Power[s[1,3],2]+Power[s[2,3],2])*2*Power[s[3,4]+s[3,5],2]/(s[3,4]*s[3,5]*s[4,5])//Factor;
integrandC45=-3*Power[4*Pi,3]/Power[s12,2]/s[4,5]*Power[1+s[4,5]/(s[3,4]+s[3,5]),2]*(Power[s[1,3],2]+Power[s[2,3],2])*(1+s[3,4]/s[3,5]*(1+s[3,4]/(s[3,4]+s[3,5])))//Factor;


(*d_s=4*)


(* ::Section::Closed:: *)
(*R(c_45)*)


input=integrandC45*measure//Factor;


input/.sToDot/.performMomentaDots/.replaceMomenta//Factor;
%/.Sin[x_]:>Sqrt[1-Power[Cos[x],2]];
%/.Cos[\[Theta]]->c\[Theta];
%/.Cos[\[Alpha]]->c\[Alpha];
%/.Cos[\[Beta]]->cosBeta;
tmp0=%//Factor


Integrate[tmp0/d[c\[Alpha]],{c\[Alpha],-1,1},Assumptions->Re[epsilon]<1/2];
tmp1=Factor[%]


Integrate[tmp1/d[c\[Theta]],{c\[Theta],-1,1},Assumptions->Re[epsilon]<1];
lessAngles=Factor[%]


lessAngles;
%/.d[x[i_]]:>d[y[i]];
%/.x[i_]:>1-y[i];
inY=%//Factor


H=-3*Power[2,6*epsilon]*(epsilon-2)*Power[Pi,3/2+2*epsilon]*Power[s12,-2*epsilon]/(8*Sin[epsilon*Pi]*Power[Gamma[1-epsilon],2]*Gamma[5/2-epsilon]*Gamma[epsilon]);
Series[H,{epsilon,0,0}]
H
int0=inY/H


int1=Integrate[int0/d[y[4]],{y[4],0,1-y[3]},Assumptions->Re[epsilon]<0]


int2=Integrate[int1/d[y[3]],{y[3],0,1},Assumptions->Re[epsilon]<0]


Series[int2,{epsilon,0,0}]


(* ::Section::Closed:: *)
(*R(s_5)*)


inputS=integrandS*measure//Factor;


inputS/.sToDot/.performMomentaDots/.replaceMomenta//Factor;
%/.Sin[x_]:>Sqrt[1-Power[Cos[x],2]];
%/.Cos[\[Theta]]->c\[Theta];
%/.Cos[\[Alpha]]->c\[Alpha];
%/.Cos[\[Beta]]->cosBeta;
tmp0S=%//Factor


Integrate[tmp0S/d[c\[Alpha]],{c\[Alpha],-1,1},Assumptions->Re[epsilon]<1/2];
tmp1S=Factor[%]


Integrate[tmp1S/d[c\[Theta]],{c\[Theta],-1,1},Assumptions->Re[epsilon]<1];
lessAnglesS=Factor[%]


lessAnglesS;
%/.d[x[i_]]:>d[y[i]];
%/.x[i_]:>1-y[i];
inYS=%//Factor


HS=-3*Power[2,6*epsilon]*(epsilon-2)*Power[Pi,3/2+2*epsilon]*Power[s12,-2*epsilon]/(8*Sin[epsilon*Pi]*Power[Gamma[1-epsilon],2]*Gamma[5/2-epsilon]*Gamma[epsilon]);
Series[HS,{epsilon,0,0}]
HS
int0S=inYS/HS//Simplify


int1S=Integrate[int0S/d[y[4]],{y[4],0,1-y[3]},Assumptions->Re[epsilon]<0]


int2S=Integrate[int1S/d[y[3]],{y[3],0,1},Assumptions->Re[epsilon]<0]


Series[int2S,{epsilon,0,0}]


(* ::Section:: *)
(*Concise*)


(* ::Subsection::Closed:: *)
(*Set - up*)


inclusiveIntegrate[input_]:=Module[{t0,t1,t2,t3,t4,t5,t6,t7,t8,H},
H=-3*Power[2,6*epsilon]*(epsilon-2)*Power[Pi,3/2+2*epsilon]*Power[s12,-2*epsilon]/(8*Sin[epsilon*Pi]*Power[Gamma[1-epsilon],2]*Gamma[5/2-epsilon]*Gamma[epsilon]);
t0=input*measure/.sToDot/.performMomentaDots/.replaceMomenta/.Sin[x_]:>Sqrt[1-Power[Cos[x],2]]/.Cos[\[Theta]]->c\[Theta]/.Cos[\[Alpha]]->c\[Alpha]/.Cos[\[Beta]]->cosBeta;
t1=Integrate[t0/d[c\[Alpha]],{c\[Alpha],-1,1},Assumptions->Re[epsilon]<1/2];
t2=Integrate[t1/d[c\[Theta]],{c\[Theta],-1,1},Assumptions->Re[epsilon]<1];
t3=t2/.d[x[i_]]:>d[y[i]]/.x[i_]:>1-y[i];
t4=Factor[t3];
t5=t4/H;
Print[t5];
t6=Integrate[t5/d[y[4]],{y[4],0,1-y[3]},Assumptions->Re[epsilon]<0];
t7=Integrate[t6/d[y[3]],{y[3],0,1},Assumptions->Re[epsilon]<0];
t8=Series[t7,{epsilon,0,0}];
Return[t8];
]


(* ::Subsection:: *)
(*R(c_45)*)


integrandC45


inclusiveIntegrate[integrandC45]


(* ::Subsection:: *)
(*R (s_ 5)*)


integrandS


inclusiveIntegrate[integrandS]


(* ::Subsection:: *)
(*R (c_ 35)*)


integrandC35=192 \[Pi]^3 (s[1,p4t]^2+s[2,p4t]^2)/s12^2/s[3,5]*(1+z3^2)/(1-z3)/.rules//Factor


inclusiveIntegrate[integrandC35]


(* ::Subchapter:: *)
(*Option 2*)


(* ::Section:: *)
(*\[Integral][1-c45]d\[Sigma]R*)


integrand=1/(s[4,5]*s12)*((s[1,3]^2+s[1,4]^2+s[2,3]^2+s[2,4]^2)/(s[3,5])-(s[1,p3t]^2+s[2,p3t]^2)/s12*(1+z4^2)/(1-z4))//Factor


inclusiveIntegrate[integrand]

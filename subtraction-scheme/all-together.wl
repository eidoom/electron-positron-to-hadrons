(* ::Package:: *)

Print[N[(5/2-Pi^2)/(2^6 3 Pi^2)]];


SetAttributes[dot,Orderless];
SetAttributes[s,Orderless];
s12=1.;
unpackMomenta=p[i_,j__]:>p[i]+p[j];
unpackDots1=dot[a_,b_+c_]:>dot[a,b]+dot[a,c];
unpackDots2=dot[a_,b_*c_p]:>dot[a,c]*b;
massless=dot[i_p,i_p]:>0;
sToDot=s[i_,j_]:>2*dot[p[i],p[j]];
p3tilde=p[p3t]:>p[3]+dot[p[4],p[5]]*p[3]/(dot[p[4,5],p[3]]);
p4tilde=p[p4t]:>p[4]+dot[p[3],p[5]]*p[4]/(dot[p[3,5],p[4]]);
replaceDeformedMomenta={p3tilde,p4tilde};
performMomentaDots=dot[x_List,y_List]:>x[[1]]*y[[1]]-x[[2]]*y[[2]]-x[[3]]*y[[3]]-x[[4]]*y[[4]];
zHat ={0,0,1};
Rx[a_] := {{1,0,0},{0,Cos[a],-Sin[a]},{0,Sin[a],Cos[a]}};
Ry[a_]:={{Cos[a],0,Sin[a]},{0,1,0},{-Sin[a],0,Cos[a]}};
Rz[a_]:={{Cos[a],-Sin[a],0},{Sin[a],Cos[a],0},{0,0,1}};
momentumConservation[{a__},n_]:=p[a]->-p@@Delete[Range@n,{#}&/@{a}]//.unpackMomenta;
fixedMomenta={
	p[1]->Sqrt[s12]/2 Prepend[zHat,-1],
	p[2]->Sqrt[s12]/2 Prepend[-zHat,-1],
	p[3]->Sqrt[s12]/2 (1-y[3]) Prepend[Ry[\[Theta]].zHat,1],
	p[4]->Sqrt[s12]/2 (1-y[4]) Prepend[Ry[\[Theta]].Rz[\[Alpha]].Ry[\[Beta]].zHat,1]
};
replaceMomenta=Join[fixedMomenta,{momentumConservation[{5},5]/.fixedMomenta}]/.
	Sin[\[Beta]]:>Sqrt[1-Power[cosBeta,2]]/.Cos[\[Beta]]->cosBeta;
cosBeta=1+2*(y[3]+y[4]-1)/((1-y[3])*(1-y[4]));
(* measure=d[\[Alpha]]*d[\[Theta]]*d[x[3]]*d[x[4]]*d[Cos[\[Beta]]]; *)
measureCoefficient=(s12*Sin[\[Theta]])/((4*Pi)^4);
evalDifferential[expr_]:=expr*measureCoefficient/.sToDot/.replaceDeformedMomenta/.
	unpackMomenta//.unpackDots1//.unpackDots2/.massless/.replaceMomenta/.performMomentaDots;
Ar=(s[1,3]^2+s[1,4]^2+s[2,3]^2+s[2,4]^2)/(s[3,5]*s[4,5]*s[1,2]);
Ac45=J4*(s[1,p3t]^2+s[2,p3t]^2)/(s[4,5]*s[1,2]^2)*(1+z4^2)/(1-z4);
Ac35=J3*(s[1,p4t]^2+s[2,p4t]^2)/(s[3,5]*s[1,2]^2)*(1+z3^2)/(1-z3);
As5c45=J4*2*(s[1,p3t]^2+s[2,p3t]^2)/(s[4,5]*s[1,2]^2*(1-z4));
As5c35=J3*2*(s[1,p4t]^2+s[2,p4t]^2)/(s[3,5]*s[1,2]^2*(1-z3));
Asp=J4*2*(s[1,p3t]^2+s[2,p3t]^2)*s[3,4]/(s[1,2]^2*s[3,5]*s[4,5]);
Aspp=J3*2*(s[1,p4t]^2+s[2,p4t]^2)*s[3,4]/(s[1,2]^2*s[3,5]*s[4,5]);
f=s[4,5]/(s[4,5]+s[3,5]);
J4=1+s[4,5]/(s[3,4]+s[3,5]);
J3=1+s[3,5]/(s[3,4]+s[4,5]);
z3=dot[p[3],p[4]]/dot[p[3,5],p[4]];
z4=dot[p[4],p[3]]/dot[p[4,5],p[3]];
integrand=Ar-Ac45+As5c45-Ac35+As5c35-f*Asp-(1-f)*Aspp;
differential=evalDifferential[integrand]


differential/.{\[Alpha]->1.,\[Theta]->1.,y[3]->N[10^(-1)],y[4]->N[10^(-1)]}
differential/.{\[Alpha]->1.,\[Theta]->1.,y[3]->N[10^(-1)],y[4]->N[10^(-12)]}
differential/.{\[Alpha]->1.,\[Theta]->1.,y[3]->N[10^(-12)],y[4]->N[10^(-1)]}
differential/.{\[Alpha]->1.,\[Theta]->1.,y[3]->10^(-12),y[4]->10^(-12)}


int0=NIntegrate[differential,{\[Alpha],0,\[Pi]}]


int1=NIntegrate[int0,{\[Theta],0,\[Pi]}]


int2=NIntegrate[int1,{y[4],0.3,0.4},{y[3],0.2,0.3}]

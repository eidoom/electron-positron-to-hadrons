(* ::Package:: *)

(* ::Title:: *)
(*Dimensional regularisation of infrared divergences by the dipole subtraction method*)


(* ::Subchapter:: *)
(*Integrated subtraction terms*)


(* ::Section::Closed:: *)
(*Collinear limit 4 || 5, c_{45}*)


zIntegrand = Power[z,-epsilon]*(1+Power[z,2])*Power[1-z,-1-epsilon];
zIntegral = Integrate[zIntegrand, {z,0,1}, Assumptions->Re[epsilon]<0];
sIntegrand = Power[-s45,-1-epsilon];
sIntegral = Integrate[sIntegrand,{s45,0,s12}, Assumptions->Re[epsilon]<0];
sigmaCollinear = -Power[2,-1+4*epsilon]*Power[Pi,-1+3*epsilon]/Gamma[1-epsilon]*\[Alpha]s*CF*sIntegral*zIntegral//Factor;
Print[sigmaCollinear]


preFactor=Power[-s12,-epsilon]*Power[2,4*epsilon]*Power[Pi,3*epsilon]/Gamma[1-epsilon];
coefficientCollinear=Power[2Pi,-1]*\[Alpha]s*CF;
Series[preFactor,{epsilon,0,0}]
sigmaCollinear//Factor;
%/preFactor/coefficientCollinear;
collinear = %//Series[#,{epsilon,0,0}]&//FullSimplify


preFactor


(* ::Section:: *)
(*Soft limit, s_5*)


softIntegrand=CF*\[Alpha]s*Power[2,2*epsilon]*d[sh45]*d[sh35]*Power[Pi,epsilon-1]/Gamma[1-epsilon]*Power[sh45*sh35,-epsilon-1]*Power[-s34,-epsilon]


tmp0=Integrate[softIntegrand/d[sh45],{sh45,0,1}, Assumptions->Re[epsilon]<0]


tmp1=Integrate[tmp0/d[sh35],{sh35,0,1}, Assumptions->Re[epsilon]<0]//Factor


coefficientSoft=Power[2*Pi,-1]*\[Alpha]s*CF;
softPreFactor=Power[-s12,-epsilon]*Power[4*Pi,epsilon]/Gamma[1-epsilon];
tmp1/softPreFactor/coefficientSoft/.s34->s12;
soft = %//FullSimplify


softPreFactor


coefficientSoft


Series[softPreFactor,{epsilon,0,1}]


(* ::Section::Closed:: *)
(*Soft gluon collinear 4 || 5, i.e. s_5 c_{45}*)


coeff=-2*\[Alpha]s*CF*Power[2\[Pi],4*epsilon-2]*2*Power[Pi,1-epsilon]/Gamma[1-epsilon];
softGluonCollinearIntegrand=Power[-s45,-1-epsilon]*Power[z,-epsilon]*Power[1-z,-1-epsilon]*coeff;
sIntegrate=Integrate[softGluonCollinearIntegrand,{s45,0,s12},Assumptions->Re[epsilon]<0];
Integrate[sIntegrate,{z,0,1},Assumptions->Re[epsilon]<0];
softCollinearGluon=%


softCollinearCoefficient=CF*\[Alpha]s/(2\[Pi]);
softCollinearGluon/preFactor/softCollinearCoefficient;
softCollinear45gluon=%//Series[#,{epsilon,0,0}]&


(* ::Section::Closed:: *)
(*Alternative: (1-s_5)c_{45}*)


collinear-softCollinear45gluon (*This is c_{45} - s_5 c_{45} (individually intergrated)*)


solidAngle=d[Omega,n_]:>2 Power[Pi,(n+1)/2]/Gamma[(n+1)/2];
d[Phi4parallel5]=Power[2*Pi,2*epsilon-2]/4*d[Omega,1-2*epsilon]*Power[-s45,-epsilon]*Power[z*(1-z),-epsilon]*d[z]/.solidAngle


alternative=-4*\[Alpha]s*CF/s45*(1+z)*d[Phi4parallel5]*d[s45]*Power[2*Pi,2*epsilon]


Integrate[alternative/d[z],{z,0,1},Assumptions->Re[epsilon]<1];
Integrate[%/d[s45],{s45,0,s12},Assumptions->Re[epsilon]<0];
alternativeIntegrated=%


altCoeff=\[Alpha]s*CF*Power[2*Pi,-1];
alternativeIntegrated/preFactor/altCoeff;
Series[%,{epsilon,0,0}]//FullSimplify


altCoeff


preFactor


(* ::Section::Closed:: *)
(*Cross section*)


2*(collinear-softCollinear45gluon)+soft


(* ::Subchapter:: *)
(*Differential counter-terms*)


(* ::Section::Closed:: *)
(*Testing NIntegrate*)


testIntegrand=(x[3]^2+x[4]^2)/((1-x[3])(1-x[4]))


tmp2=NIntegrate[testIntegrand,{x[4],0,1},{x[3],0,1-x[4]}]


tmp3=NIntegrate[testIntegrand,{x[3],0,1}]


tmp4=Integrate[testIntegrand,{x[3],0,1-x[4]},Assumptions->0<=x[4]<=1&&Im[x[4]]==0]


tmp5=Integrate[tmp4,{x[4],0,1}]


tmp6=NIntegrate[tmp4,{x[4],0,1}]


tmp6-tmp2


tmp4/.x[4]->1


tmp7=Integrate[testIntegrand,{x[4],0,1},{x[3],0,1-x[4]}]


tmp8=Integrate[((1-y[3])^2+(1-y[4])^2)/(y[3]*y[4]),{y[4],1-y[3],1},Assumptions->Im[y[3]]==0&&0<y[3]<=1]


tmp9=Integrate[((1-y[3])^2+(1-y[4])^2)/(y[3]*y[4]),{y[4],0,1-y[3]}] (*Correct limits*)


tmp10=Integrate[((1-y[3])^2+(1-y[4])^2)/(y[3]*y[4]),{y[3],0,1},{y[4],1-y[3],1},Assumptions->Im[y[3]]==0&&0<y[3]<=1]


tmp11=Integrate[((1-y[3])^2+(1-y[4])^2)/(y[3]*y[4]),{y[3],0,1},{y[4],0,1-y[3]}] (*Correct limits*)


tmp12=Integrate[testIntegrand,{x[4],0,1},{x[3],0,1}]





(* ::Section::Closed:: *)
(*Plotting limits*)


limitsPlotRaw=RegionPlot[y[3]+y[4]<=1,{y[3],0,1},{y[4],0,1},FrameLabel->{Subscript["y","3"],Subscript["y","4"]}];
divergences=Plot[0,{y[3],0,1},
	Epilog->{
		{AbsoluteThickness[8],Red,Line[{{0,0},{0,1}}]},
		{AbsoluteThickness[8],Red,Line[{{0,0},{1,0}}]},
		{PointSize[0.1],Red,Point[{0,0}]}
		},
	AxesLabel->{Subscript["y","3"],Subscript["y","4"]},
	PlotRange->{{0,1},{0,1}},
	AspectRatio->1
	];
limitsPlot=Show[{divergences,limitsPlotRaw}]
Export[StringJoin[NotebookDirectory[],"limitsPlot.pdf"],limitsPlot];


(* ::Section::Closed:: *)
(*The slightly less big integral: \[Integral][1-2(1-s5)c45]d\[Sigma]R*)


integrand=1/s[4,5]*((s[1,3]^2+s[1,4]^2+s[2,3]^2+s[2,4]^2)/(s[3,5])-2*(s[1,3t]^2+s[1,Pt]^2)/(s[1,2]^2)*(1+z^2)/(1-z))//Factor


measure=(2^(4-3 n) \[Pi]^(1/2-n) d[Cos[\[Alpha]]] d[Cos[\[Theta]]] d[x[3]] d[x[4]] s[1,2]^(-3+n) Sin[\[Alpha]]^(-5+n) Sin[\[Theta]]^(-4+n) (-1+x[3])^(-2+n/2) (-1+x[4])^(-2+n/2) (-1+x[3]+x[4])^(-2+n/2))/(Gamma[1/2 (-3+n)] Gamma[1/2 (-2+n)])/.n->4;


SetAttributes[dot,Orderless];
SetAttributes[s,Orderless];
unpackMomenta=p[i_,j__]:>p[i]+p[j];
unpackDots=dot[a_,b_+c_]:>dot[a,b]+dot[a,c];
sToDot=s[i_,j_]:>2*dot[p[i],p[j]];
threeTilde=s[a_,3t]:>s[a,3]+s[4,5]*s[a,n]/(2*dot[p[4,5],p[n]]);
pTilde=s[a_,Pt]:>s[a,4]+s[a,5]-s[4,5]*s[a,n]/(2*dot[p[4,5],p[n]]);
minkowskiMetric=DiagonalMatrix[{1,-1,-1,-1}];
performMomentaDots=dot[x_,y_]:>x.minkowskiMetric.y;
zHat ={0,0,1};
Rx[a_] := {{1,0,0},{0,Cos[a],-Sin[a]},{0,Sin[a],Cos[a]}};
Ry[a_]:={{Cos[a],0,Sin[a]},{0,1,0},{-Sin[a],0,Cos[a]}};
Rz[a_]:={{Cos[a],-Sin[a],0},{Sin[a],Cos[a],0},{0,0,1}};
momentumConservation[{a__},n_]:=p[a]->-p@@Delete[Range@n,{#}&/@{a}]//.unpackMomenta;
fixedMomenta={
	p[1]->Sqrt[s[1,2]]/2 Prepend[zHat,-1],
	p[2]->Sqrt[s[1,2]]/2 Prepend[-zHat,-1],
	p[3]->Sqrt[s[1,2]]/2 x[3] Prepend[Ry[\[Theta]].zHat,1],
	p[4]->Sqrt[s[1,2]]/2 x[4] Prepend[Ry[\[Theta]].Rz[\[Alpha]].Ry[\[Beta]].zHat,1]
};
replaceN=p[n]->p[3];
replaceMomenta=Join[fixedMomenta,{momentumConservation[{5},5]/.fixedMomenta,replaceN/.unpackMomenta/.fixedMomenta}];
replaceZ=z:>dot[p[4],p[n]]/dot[p[4,5],p[n]];
cosBeta=1+2*(1-x[3]-x[4])/(x[3]*x[4]);


dot[p[n],p[n]]/.performMomentaDots/.replaceMomenta//Simplify


integrandProcessed=integrand/.threeTilde/.pTilde/.replaceZ/.unpackMomenta/.unpackDots/.sToDot/.performMomentaDots/.replaceMomenta//Factor


integrandProcessed*measure;
%/.Sin[x_]:>Sqrt[1-Power[Cos[x],2]];
%/.Cos[\[Theta]]->c\[Theta];
%/.Csc[\[Alpha]]->Power[1-Power[c\[Alpha],2],-1/2];
%/.Cos[\[Alpha]]->c\[Alpha];
%/.Cos[\[Beta]]->cosBeta;
Factor[%];
Integrate[%/d[c\[Alpha]],{c\[Alpha],-1,1}];
Factor[%];
Integrate[%/d[c\[Theta]],{c\[Theta],-1,1}];
lessAngles=Factor[%]


lessAngles;
%/.d[x[i_]]:>d[y[i]];
%/.x[i_]:>1-y[i];
inY=%//Factor


int1=Integrate[inY/d[y[4]],{y[4],\[Delta],1-y[3]},Assumptions->Re[\[Delta]]+y[3]<1&&Re[\[Delta]]>0&&Im[\[Delta]]==0]


int2=Integrate[int1/d[y[3]],{y[3],\[Delta],1-\[Delta]},Assumptions->0<Re[\[Delta]]<1/2&&Im[\[Delta]]==0]


(* ::Section::Closed:: *)
(*The big integral: \[Integral][1-2(1-s_5)c_{45}-s_5]d\[Sigma]R*)


integrand=(s[1,3]^2+s[1,4]^2+s[2,3]^2+s[2,4]^2)/(s[3,5]*s[4,5])+(s[1,3t]^2+s[1,P45t]^2)/(s[4,5]*s[1,2]^2)*(1+z4)+(s[1,4t]^2+s[1,P35t]^2)/(s[3,5]*s[1,2]^2)*(1+z3)-2*(s[1,3p]^2+s[1,4p]^2)*s[3p,4p]/(s[1,2]^2*s[3p,5]*s[4p,5])


measure=(2^(4-3 n) \[Pi]^(1/2-n) d[Cos[\[Alpha]]] d[Cos[\[Theta]]] d[x[3]] d[x[4]] s[1,2]^(-3+n) Sin[\[Alpha]]^(-5+n) Sin[\[Theta]]^(-4+n) (-1+x[3])^(-2+n/2) (-1+x[4])^(-2+n/2) (-1+x[3]+x[4])^(-2+n/2))/(Gamma[1/2 (-3+n)] Gamma[1/2 (-2+n)])/.n->4;


SetAttributes[dot,Orderless];
SetAttributes[s,Orderless];
unpackMomenta=p[i_,j__]:>p[i]+p[j];
unpackDots1=dot[a_,b_+c_]:>dot[a,b]+dot[a,c];
unpackDots2=dot[a_,b_*c_p]:>dot[a,c]*b;
massless=dot[i_p,i_p]:>0;
sToDot=s[i_,j_]:>2*dot[p[i],p[j]];
p3tilde=p[3t]:>p[3]+s[4,5]*p[3]/(2*dot[p[4,5],p[3]]);
p4tilde=p[4t]:>p[4]+s[3,5]*p[4]/(2*dot[p[3,5],p[4]]);
P45tilde=p[P45t]:>p[4,5]-s[4,5]*p[3]/(2*dot[p[4,5],p[3]]);
P35tilde=p[P35t]:>p[3,5]-s[3,5]*p[4]/(2*dot[p[3,5],p[4]]);
p3prime=p[3p]:>p[3]+s[4,5]*p[3]/(2*dot[p[4,5],p[3]]);
p4prime=p[4p]:>p[4,5]-s[4,5]*p[3]/(2*dot[p[4,5],p[3]]);
replaceZ3=z3:>dot[p[3],p[4]]/dot[p[3,5],p[4]];
replaceZ4=z4:>dot[p[4],p[3]]/dot[p[4,5],p[3]];
replaceDeformedMomenta={p3tilde,p4tilde,P45tilde,P35tilde,p3prime,p4prime,replaceZ3,replaceZ4}/.sToDot;
minkowskiMetric=DiagonalMatrix[{1,-1,-1,-1}];
performMomentaDots=dot[x_,y_]:>x.minkowskiMetric.y;
zHat ={0,0,1};
Rx[a_] := {{1,0,0},{0,Cos[a],-Sin[a]},{0,Sin[a],Cos[a]}};
Ry[a_]:={{Cos[a],0,Sin[a]},{0,1,0},{-Sin[a],0,Cos[a]}};
Rz[a_]:={{Cos[a],-Sin[a],0},{Sin[a],Cos[a],0},{0,0,1}};
momentumConservation[{a__},n_]:=p[a]->-p@@Delete[Range@n,{#}&/@{a}]//.unpackMomenta;
fixedMomenta={
	p[1]->Sqrt[s[1,2]]/2 Prepend[zHat,-1],
	p[2]->Sqrt[s[1,2]]/2 Prepend[-zHat,-1],
	p[3]->Sqrt[s[1,2]]/2 x[3] Prepend[Ry[\[Theta]].zHat,1],
	p[4]->Sqrt[s[1,2]]/2 x[4] Prepend[Ry[\[Theta]].Rz[\[Alpha]].Ry[\[Beta]].zHat,1]
};
replaceMomenta=Join[fixedMomenta,{momentumConservation[{5},5]/.fixedMomenta}];
cosBeta=1+2*(1-x[3]-x[4])/(x[3]*x[4]);


tmpm1=integrand/.sToDot/.replaceDeformedMomenta/.unpackMomenta//.unpackDots1//.unpackDots2/.massless//Factor;


integrandProcessed=tmpm1/.performMomentaDots/.replaceMomenta//Factor;


integrandProcessed*measure;
%/.Sin[x_]:>Sqrt[1-Power[Cos[x],2]];
%/.Cos[\[Theta]]->c\[Theta];
%/.Csc[\[Alpha]]->Power[1-Power[c\[Alpha],2],-1/2];
%/.Cos[\[Alpha]]->c\[Alpha];
%/.Cos[\[Beta]]->cosBeta;
tmp0=%;


tmp0


Integrate[tmp0/d[c\[Alpha]],{c\[Alpha],-1,1}];
tmp1=Factor[%];


Integrate[tmp1/d[c\[Theta]],{c\[Theta],-1,1}];
lessAngles=Factor[%]


lessAngles;
%/.d[x[i_]]:>d[y[i]];
%/.x[i_]:>1-y[i];
inY=%//Factor


int1=Integrate[inY/d[y[4]],{y[4],0,1-y[3]},Assumptions->0<=y[3]<=1]


int2=Integrate[int1/d[y[3]],{y[3],0,1}]


(* ::Section::Closed:: *)
(*NInt[(1-(1-s_ 5)c_{45}-(1-s_ 5)c_{35}-s_ 5)d\[Sigma]R]*)


SetAttributes[dot,Orderless];
SetAttributes[s,Orderless];
unpackMomenta=p[i_,j__]:>p[i]+p[j];
unpackDots1=dot[a_,b_+c_]:>dot[a,b]+dot[a,c];
unpackDots2=dot[a_,b_*c_p]:>dot[a,c]*b;
massless=dot[i_p,i_p]:>0;
sToDot=s[i_,j_]:>2*dot[p[i],p[j]];
p3tilde=p[3t]:>p[3]+s[4,5]*p[3]/(2*dot[p[4,5],p[3]]);
p4tilde=p[4t]:>p[4]+s[3,5]*p[4]/(2*dot[p[3,5],p[4]]);
P45tilde=p[P45t]:>p[4,5]-s[4,5]*p[3]/(2*dot[p[4,5],p[3]]);
P35tilde=p[P35t]:>p[3,5]-s[3,5]*p[4]/(2*dot[p[3,5],p[4]]);
p3prime=p[3p]:>p[3]+s[4,5]*p[3]/(2*dot[p[4,5],p[3]]);
p4prime=p[4p]:>p[4,5]-s[4,5]*p[3]/(2*dot[p[4,5],p[3]]);
p3primeprime=p[3pp]:>p[3,5]+s[3,5]*p[4]/(2*dot[p[3,5],p[4]]);
p4primeprime=p[4pp]:>p[4]-s[3,5]*p[4]/(2*dot[p[3,5],p[4]]);
replaceZ3=z3:>dot[p[3],p[4]]/dot[p[3,5],p[4]];
replaceZ4=z4:>dot[p[4],p[3]]/dot[p[4,5],p[3]];
replaceDeformedMomenta={p3tilde,p4tilde,P45tilde,P35tilde,p3prime,p4prime,p3primeprime,p4primeprime,replaceZ3,replaceZ4}/.sToDot;
minkowskiMetric=DiagonalMatrix[{1,-1,-1,-1}];
performMomentaDots=dot[x_,y_]:>x.minkowskiMetric.y;
zHat ={0,0,1};
Rx[a_] := {{1,0,0},{0,Cos[a],-Sin[a]},{0,Sin[a],Cos[a]}};
Ry[a_]:={{Cos[a],0,Sin[a]},{0,1,0},{-Sin[a],0,Cos[a]}};
Rz[a_]:={{Cos[a],-Sin[a],0},{Sin[a],Cos[a],0},{0,0,1}};
momentumConservation[{a__},n_]:=p[a]->-p@@Delete[Range@n,{#}&/@{a}]//.unpackMomenta;
fixedMomenta={
	p[1]->Sqrt[s[1,2]]/2 Prepend[zHat,-1],
	p[2]->Sqrt[s[1,2]]/2 Prepend[-zHat,-1],
	p[3]->Sqrt[s[1,2]]/2 x[3] Prepend[Ry[\[Theta]].zHat,1],
	p[4]->Sqrt[s[1,2]]/2 x[4] Prepend[Ry[\[Theta]].Rz[\[Alpha]].Ry[\[Beta]].zHat,1]
};
replaceMomenta=Join[fixedMomenta,{momentumConservation[{5},5]/.fixedMomenta}];
cosBeta=1+2*(1-x[3]-x[4])/(x[3]*x[4]);
measure=(d[Cos[\[Alpha]]]*d[Cos[\[Theta]]]*d[x[3]]*d[x[4]]*s[1, 2])/((4*Pi)^4*Sin[\[Alpha]]);
removeMeasures[expr_]:=expr/(d[y[4]] d[Cos[\[Alpha]]] d[Cos[\[Theta]]] d[y[3]]);
evalDifferential[expr_]:=Module[{t0,t1,t2,t3,t4,t5},
	t0=(expr/.sToDot/.replaceDeformedMomenta/.unpackMomenta//.unpackDots1//.unpackDots2/.massless/.performMomentaDots/.replaceMomenta);
	t1=t0*measure;
	t2=t1/.Sin[\[Beta]]:>Sqrt[1-Power[Cos[\[Beta]],2]];
	t3=t2/.Cos[\[Beta]]->cosBeta;
	t4=t3/.d[x[i_]]:>d[y[i]];
	t5=t4/.x[i_]:>1-y[i];
	Return[t5];
];
evalPhaseSpacePoint[expr_,alpha_:\[Alpha],theta_:\[Theta],y3_:y[3],y4_:y[4]]:=removeMeasures[expr]/.{y[4]->y4,y[3]->y3,\[Alpha]->alpha,\[Theta]->theta};
Ar=(s[1,3]^2+s[1,4]^2+s[2,3]^2+s[2,4]^2)/(s[3,5]*s[4,5]*s[1,2]);
Ac45=(s[1,3t]^2+s[1,P45t]^2)/(s[4,5]*s[1,2]^2)*(1+z4^2)/(1-z4);
Ac35=(s[1,4t]^2+s[1,P35t]^2)/(s[3,5]*s[1,2]^2)*(1+z3^2)/(1-z3);
As5c45=2*(s[1,3t]^2+s[1,P45t]^2)/(s[4,5]*s[1,2]^2*(1-z4));
As5c35=2*(s[1,4t]^2+s[1,P35t]^2)/(s[3,5]*s[1,2]^2*(1-z3));
As1=2*(s[1,3p]^2+s[1,4p]^2)*s[3p,4p]/(s[1,2]^2*s[3p,5]*s[4p,5]);
As2=2*(s[1,3pp]^2+s[1,4pp]^2)*s[3pp,4pp]/(s[1,2]^2*s[3pp,5]*s[4pp,5]);
(*Ja=Sqrt[s[1,2]*(1-y[3])];
Jb=Sqrt[s[1,2]*(1-y[4])];*)
integrand=Ar-Ac45-Ac35-As1/2-As2/2+As5c45+As5c35;


each=(removeMeasures[evalDifferential[#]]/.{s[1,2]->1,y[4]->0.4,y[3]->10^(-3),\[Alpha]->0.2,\[Theta]->0.3})&/@{Ar,-Ac45,As5c45,-Ac35,As5c35,-As1/2,-As2/2}
Plus@@each


eachInt=NIntegrate[removeMeasures[evalDifferential[#]],{\[Alpha],1,1.1},{\[Theta],2,2.1},{y[4],0.3,0.4},{y[3],0.2,0.3}]&/@{Ar,-Ac45,As5c45,-Ac35,As5c35,-As1/2,-As2/2}//Factor
Plus@@eachInt


removeMeasures[evalDifferential[Plus@@{Ar,-Ac45,As5c45,-Ac35,As5c35,-As1/2,-As2/2}]]/.{y[4]->0.,y[3]->0.4,\[Alpha]->1,\[Theta]->2}//Factor


NIntegrate[removeMeasures[evalDifferential[Plus@@{Ar,-Ac45,As5c45,-Ac35,As5c35,-As1/2,-As2/2}]],{\[Alpha],1,1.1},{\[Theta],2,2.1},{y[4],0.3,0.4},{y[3],0.1,0.6}]//Factor


(* ::Section::Closed:: *)
(*Jacobians*)


s[4,5]-s[3,4]-s[3,5]/.sToDot/.replaceMomenta/.performMomentaDots/.Cos[\[Beta]]->cosBeta/.x[i_]:>1-y[i]//Simplify


a=s[3,5]/.sToDot/.replaceMomenta/.performMomentaDots/.Cos[\[Beta]]->cosBeta/.x[i_]:>1-y[i]//Simplify


b=s[3,4]/.sToDot/.replaceMomenta/.performMomentaDots/.Cos[\[Beta]]->cosBeta/.x[i_]:>1-y[i]//Simplify


c=s[5,4]/.sToDot/.replaceMomenta/.performMomentaDots/.Sin[\[Beta]]:>Sqrt[1-Cos[\[Beta]]^2]/.Cos[\[Beta]]->cosBeta/.x[i_]:>1-y[i]//Simplify


b+c


(* ::Section::Closed:: *)
(*Debug*)


(* ::Subsection::Closed:: *)
(*Set - up*)


SetAttributes[dot,Orderless];
SetAttributes[s,Orderless];
unpackMomenta=p[i_,j__]:>p[i]+p[j];
sToDot=s[i_,j_]:>2*dot[p[i],p[j]];
performMomentaDotsExplicitly=dot[x_List,y_List]:>x[[1]]*y[[1]]-x[[2]]*y[[2]]-x[[3]]*y[[3]]-x[[4]]*y[[4]];
zHat ={0,0,1};
Rx[a_] := {{1,0,0},{0,Cos[a],-Sin[a]},{0,Sin[a],Cos[a]}};
Ry[a_]:={{Cos[a],0,Sin[a]},{0,1,0},{-Sin[a],0,Cos[a]}};
Rz[a_]:={{Cos[a],-Sin[a],0},{Sin[a],Cos[a],0},{0,0,1}};
momentumConservation[{a__},n_]:=-p@@Delete[Range@n,{#}&/@{a}]//.unpackMomenta;
cosBeta=1+2*(1-x[3]-x[4])/(x[3]*x[4]);
measure=d[\[Alpha]]*d[\[Theta]]*d[x[3]]*d[x[4]]*d[Cos[\[Beta]]];
measureCoefficient=(s[1, 2]*Sin[\[Theta]])/((4*Pi)^4);
Options[evalDifferential]={\[Alpha]->\[Alpha],\[Theta]->\[Theta],y3->y[3],y4->y[4],s->s[1,2]};
evalDifferential[termsStrings_,OptionsPattern[]]:=Module[
	{
		terms,solutions,tmp,values,sToN,sDeformedToN,p1,p2,p3,p4,p5,s13,s14,s15,s23,s24,s25,s34,s35,s45,c\[Beta],m,
		z3N,z4N,JaN,JbN,p3tilde,p4tilde,P45tilde,P35tilde,s1P45t,s1p3t,s1P35t,s1p4t,s5P45t,s5p3t,s3P45t,s5P35t,s4P35t,
		alpha=N[OptionValue[\[Alpha]]],theta=N[OptionValue[\[Theta]]],y3=N[OptionValue[y3]],y4=N[OptionValue[y4]],s12=N[OptionValue[s]]
	},
	terms=ToExpression/@termsStrings;
	c\[Beta]=cosBeta/.{x[3]->1-y3,x[4]->1-y4};
	p1=Sqrt[s[1,2]]/2 Prepend[zHat,-1]/.s[1,2]->s12;
	p2=Sqrt[s[1,2]]/2 Prepend[-zHat,-1]/.s[1,2]->s12;
	p3=Sqrt[s[1,2]]/2 x[3] Prepend[Ry[\[Theta]].zHat,1]/.s[1,2]->s12/.x[3]->1-y3/.x[4]->1-y4/.\[Theta]->theta;
	p4=Sqrt[s[1,2]]/2 x[4] Prepend[Ry[\[Theta]].Rz[\[Alpha]].Ry[\[Beta]].zHat,1]/.s[1,2]->s12/.x[3]->1-y3/.x[4]->1-y4/.\[Theta]->theta/.\[Alpha]->alpha/.Sin[\[Beta]]:>Sqrt[1-Power[c\[Beta],2]]/.Cos[\[Beta]]->c\[Beta];
	replaceFixedMomenta={
		p[1]->p1,
		p[2]->p2,
		p[3]->p3,
		p[4]->p4
	};
	p5=momentumConservation[{5},5]/.replaceFixedMomenta;
	replaceMomenta=Append[replaceFixedMomenta,p[5]->p5];
	sToN[ss_s]:=ss/.sToDot/.replaceMomenta/.performMomentaDotsExplicitly;
	s13=sToN[s[1,3]];
	s14=sToN[s[1,4]];
	s15=sToN[s[1,5]];
	s23=sToN[s[2,3]];
	s24=sToN[s[2,4]];
	s25=sToN[s[2,5]];
	s34=sToN[s[3,4]];
	s35=sToN[s[3,5]];
	s45=sToN[s[4,5]];
	replaceS={
		s[1,2]->s12,
		s[3,4]->s34,
		s[3,5]->s35,
		s[4,5]->s45,
		s[1,3]->s13,
		s[1,4]->s14,
		s[2,3]->s23,
		s[2,4]->s24
	};
	p3tilde=(p[3]+s[4,5]*p[3]/(s[3,4]+s[3,5]))/.replaceS/.replaceMomenta;
	p4tilde=(p[4]+s[3,5]*p[4]/(s[3,4]+s[4,5]))/.replaceS/.replaceMomenta;
	P45tilde=(p[4,5]-s[4,5]*p[3]/(s[3,4]+s[3,5]))/.replaceS/.unpackMomenta/.replaceMomenta;
	P35tilde=(p[3,5]-s[3,5]*p[4]/(s[3,4]+s[4,5]))/.replaceS/.unpackMomenta/.replaceMomenta;
	replaceDeformedMomenta={
		p[p3t]->p3tilde,
		p[p4t]->p4tilde,
		p[P45t]->P45tilde,
		p[P35t]->P35tilde
	};
	sDeformedToN[ss_s]:=ss/.sToDot/.replaceMomenta/.replaceDeformedMomenta/.performMomentaDotsExplicitly;
	z4N=1-s[3,4]/(s[3,4]+s[3,5])/.replaceS;
	z3N=s[3,4]/(s[3,4]+s[4,5])/.replaceS;
	JaN=1+s[4,5]/(s[3,4]+s[3,5])/.replaceS;
	JbN=1+s[3,5]/(s[3,4]+s[4,5])/.replaceS;
	m=measureCoefficient/.\[Theta]->theta/.s[1,2]->s12;
	s1P45t=sDeformedToN[s[1,P45t]];
	s1p3t=sDeformedToN[s[1,p3t]];
	s1P35t=sDeformedToN[s[1,P35t]];
	s1p4t=sDeformedToN[s[1,p4t]];
	s2p4t=sDeformedToN[s[2,p4t]];
	s5P45t=sDeformedToN[s[5,P45t]];
	s5p3t=sDeformedToN[s[5,p3t]];
	s3P45t=sDeformedToN[s[3,P45t]];
	s5P35t=sDeformedToN[s[5, P35t]];
	s4P35t=sDeformedToN[s[4, P35t]];
	solutions=(#*m)/.replaceS/.
		{
			z3->z3N,
			z4->z4N,
			Ja->JaN,
			Jb->JbN,
			s[1,P45t]->s1P45t,
			s[1,p3t]->s1p3t,
			s[5,P45t]->s5P45t,
			s[5,p3t]->s5p3t,
			s[1,P35t]->s1P35t,
			s[1,p4t]->s1p4t,
			s[3, P45t]->s3P45t,
			s[5, P35t]->s5P35t,
			s[4, P35t]->s4P35t,
			s[2,p4t]->s2p4t
		}&/@terms;
	values=Transpose[{
		{
			"\[Alpha]",
			"\[Theta]",
			"y3",
			"y4",
			"p1",
			"p2",
			"p3",
			"p4",
			"p5",
			"d\[CapitalPhi]",
			"s[1,2]",
			"s[1,3]",
			"s[1,4]",
			"s[1,5]",
			"s[2,3]",
			"s[2,4]",
			"s[2,5]",
			"s[3,4]",
			"s[3,5]",
			"s[4,5]",
			"z3",
			"z4",
			"Ja",
			"Jb",
			"p3~",
			"p4~",
			"P45~",
			"P35~",
			"s[1,P45t]",
			"s[1,p3t]",
			"s[1,P35t]",
			"s[1,p4t]",
			"s[3,P45t]",
			"s[4,P35t]",
			"s[5,P45t]",
			"s[5,p3t]",
			"s[5,P35t]"
		},
		{
			alpha,
			theta,
			y3,
			y4,
			p1,
			p2,
			p3,
			p4,
			p5,
			m,
			s12,
			s13,
			s14,
			s15,
			s23,
			s24,
			s25,
			s34,
			s35,
			s45,
			z3N,
			z4N,
			JaN,
			JbN,
			p3tilde,
			p4tilde,
			P45tilde,
			P35tilde,
			s1P45t,
			s1p3t,
			s1P35t,
			s1p4t,
			s3P45t,
			s4P35t,
			s5P45t,
			s5p3t,
			s5P35t
		}
		}];
	output=Transpose[{termsStrings,solutions}];
	For[i=1,i<=Length[values],i++,Print[values[[i]]]];
	For[i=1,i<=Length[output],i++,Print[output[[i]]]];
	Print[{"sum",Plus@@solutions}];
	tmp=Transpose[{termsStrings,terms}];
	(* For[i=1,i<=Length[tmp],i++,Print[tmp[[i]]]]; *)
	Return[solutions];
];


(* ::Subsection::Closed:: *)
(*Testing*)


Ar=(s[1,3]^2+s[1,4]^2+s[2,3]^2+s[2,4]^2)/(s[3,5]*s[4,5]*s[1,2]);
Ac35=(s[1,p4t]^2+s[2,p4t]^2)/(s[3,5]*s[1,2]^2)*(1+z3^2)/(1-z3);
evalDifferential[{"Ar","-Ac35"},s->1.,\[Alpha]->1.,\[Theta]->2.,y3->10^(-1),y4->10^(-15)];

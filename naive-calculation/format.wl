(* ::Package:: *)

(*Format[x[i_]]:=Subscript["x",ToString[i]];
Format[y[i_]]:=Subscript["y",ToString[i]];
Format[\[Alpha]s]=Subscript["\[Alpha]","s"];
Format[\[Alpha]e]=Subscript["\[Alpha]","e"];
Format[\[Sigma]0]=Subscript["\[Sigma]","0"];
Format[Cf]=Subscript["C","F"];
Format[Qq]=Subscript["Q","q"];
Format[gs]=Subscript["g","s"];
Format[Nc]=Subscript["N","C"];
Format[s[a__]]:=Subscript["s",StringJoin[Map[ToString,{a}]]];
Format[trace[a__]]:=tr[TableForm[{{a}},TableSpacing\[Rule]{0, 0}]];
Format[m[i_]]:=Subscript[\[Mu],ToString[i]];
Format[p[i__]]:=Subscript[p,TableForm[{{i}},TableSpacing\[Rule]{0, 0}]];
Format[dot[gamma,a_m]]:=Power[\[Gamma],a];
Format[dot[gamma,a_p]]:=slashed[a];*)


(*Format[trace[a__]]:=tr[a];
Format[dot[gamma,a_m]]:=Power[\[Gamma],a];
Format[dot[gamma,a_p]]:=slashed[a];
Format[\[Alpha]s]=alphaqcd;
Format[\[Alpha]e]=alphaqed;
Format[Qq]=fractionalcharge;
Format[gs]=couplingqcd;
Format[Nc]=numbercolours;
Format[s[a__]]:=Subscript["s",StringJoin[Map[ToString,{a}]]];
Format[p[i_]]:=Subscript["p",ToString[i]];
Format[m[i_]]:=Subscript[\[Mu],ToString[i]];*)


convert[expr_]:=expr/.{s[a__Integer,b___Symbol]:>s@@Join[#+2&/@{a},{b}],p[a__Integer,b___Symbol]:>p@@Join[#+2&/@{a},{b}]}/.{a->2,b->1};

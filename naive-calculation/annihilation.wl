(* ::Package:: *)

(* ::Title:: *)
(*Naive dimensional regularisation of infrared divergences*)


(* ::Section::Closed:: *)
(*Some set up*)


SetAttributes[dot,Orderless];
SetAttributes[s,Orderless];
unpackMomenta=p[i_,j__]:>p[i]+p[j];
sToDot=s[i_,j_]:>2*dot[p[i],p[j]];
dotToS=dot[p[i_],p[j_]]:>s[i,j]/2;
rules={
	traceGamma[a__]:>trace@@(dot[gamma,#]&/@{a}),
	trace[a___,dot[gamma,q_p],b___]:>Module[{j},dot[q,m[j]]trace[a,dot[gamma,m[j]],b]],
	dot[p[a_],p[a_]]->0,
	Power[dot[p[i_],m_m],2]->0,
	dot[p[x_],i_m]*dot[p[x_],j_m]*trace[a___,dot[gamma,i_m],dot[gamma,j_m],b___]->0,
	trace[a___,dot[gamma,i_m],dot[gamma,i_m],b___]:>dot[i,i]*trace[a,b],
	Condition[dot[m[i_],m[i_]],Or[i==1,i==2]]:>4,(*4 here is the number of Lorentz indices on the photon*)
	Power[dot[m[1],m[2]],2]:>4,(*4 here is the number of Lorentz indices on the photon*)
	dot[m[3],m[3]]->ds, (*ds is the number of Lorentz indices on the gluon*)
	dot[i_,j_m]*dot[j_m,k_]:>dot[i,k],
	dot[i_m,j_m]*trace[a___,dot[gamma,j_m],b___]:>trace[a,dot[gamma,i],b],
	Condition[dot[i_m,q_p]*dot[j_m,q_p]*trace[a___,dot[gamma,i_m],dot[gamma,k_m],b___,dot[gamma,j_m],c___],
Length[{b}]<((Length[{a}]+Length[{b}]+Length[{c}]+3)/2)]:>
	2*dot[k,q]*dot[j,q]*trace[a,b,dot[gamma,j],c]-dot[i,q]*dot[j,q]*trace[a,dot[gamma,k],dot[gamma,i],b,dot[gamma,j],c],
	Condition[dot[i_m,q_p]*dot[j_m,q_p]*trace[a___,dot[gamma,i_m],b__,dot[gamma,j_m],c___],
Length[{b}]>((Length[{a}]+Length[{b}]+Length[{c}]+2)/2-1)]:>
	dot[i,q]*dot[j,q]*trace[dot[gamma,j],c,a,dot[gamma,i],b],
	Condition[trace[a___,dot[gamma,i_m],dot[gamma,j_m],b___,dot[gamma,i_m],c___],
Length[{b}]<((Length[{a}]+Length[{b}]+Length[{c}]+3)/2)]:>
	-trace[a,dot[gamma,j],dot[gamma,i],b,dot[gamma,i],c]+2*trace[a,b,dot[gamma,j],c],
	Condition[trace[a___,dot[gamma,i_m],b__,dot[gamma,i_m],c___],
Length[{b}]>((Length[{a}]+Length[{b}]+Length[{c}]+2)/2-1)]:>
	trace[dot[gamma,i],c,a,dot[gamma,i],b],
	unpackMomenta,
	dot[a_,b_p+c_p]:>dot[a,b]+dot[a,c],
	trace[dot[gamma,i_m],dot[gamma,j_m]]:>4*dot[i,j],(*4 here is the number of spinor indices*)
	trace[dot[gamma,i_m],dot[gamma,j_m],dot[gamma,k_m],dot[gamma,l_m]]:>4*(dot[i,j]*dot[k,l]-dot[i,k]*dot[j,l]+dot[i,l]*dot[j,k])(*4 here from Clifford algebra*)
};
applyRules[expr_]:=FixedPoint[Expand[#/.rules]&,expr];
momentumConservationInvariants[{a__},n_]:=s[a]->s@@Delete[Range@n,{#}&/@{a}];
invariantSet=Join[momentumConservationInvariants[#,5]&/@{{1,5},{2,5},{3,5},{4,5}},{s[3,4]->-(s[1,2,3]+s[1,4]+s[2,4]),s[a__]:>Plus@@DeleteDuplicates[s@@@Permutations[{a},{2}]]}];
minkowskiMetric=DiagonalMatrix[{1,-1,-1,-1}];
performMomentaDots={dot[x_,y_]:>x.minkowskiMetric.y};
z ={0,0,1};
Rx[a_] := {{1,0,0},{0,Cos[a],-Sin[a]},{0,Sin[a],Cos[a]}};
Ry[a_]:={{Cos[a],0,Sin[a]},{0,1,0},{-Sin[a],0,Cos[a]}};
Rz[a_]:={{Cos[a],-Sin[a],0},{Sin[a],Cos[a],0},{0,0,1}};
momentumConservation[{a__},n_]:=p[a]->-p@@Delete[Range@n,{#}&/@{a}]//.unpackMomenta;
fluxFactor=2*s[1,2];
dimReg=4-2*\[Epsilon];


(* ::Section::Closed:: *)
(*Two body phase space*)


d[\[CapitalPhi]2]=Power[s[1,2],(n-4)/2]*Power[2,4-2*n]*Power[\[Pi],(2-n)/2]*Power[Sin[\[Theta]],n-4]*d[Cos[\[Theta]]]/Gamma[(n-2)/2]


d[\[CapitalPhi]2]/.n->dimReg


d[\[CapitalPhi]2]/.n->4


(* ::Section::Closed:: *)
(*Three body phase space*)


s[1,2]/.sToDot/.performMomentaDots/.momentaReal


cosBeta=1+2*(1-x[1]-x[2])/(x[1] x[2]);


Product[d[p[i],n]*Power[2*\[Pi],1-n]*\[Delta][Power[p[i],2],"+"],{i,1,3}]\[Delta][Sum[p[i],{i,3}]-q,n]Power[2*\[Pi],n];
%/d[p[3],n]/\[Delta][Sum[p[i],{i,3}]-q,n];
(%/Product[\[Delta][Power[p[i],2],"+"],{i,1,2}])/.d[p[i_],n]:>d[p[i],n-1]/(2*en[i]);
(%*2/(s[1,2]*x[1]*x[2]))/.Power[p[3],2]->(cosBeta-Cos[\[Beta]]);
%/.d[p[i_],n-1]:>Power[en[i],n-2]d[p[i]]d[Omega[i],n-2];
%/.en[i_]:>Sqrt[s[1,2]]*x[i]/2;
%/.d[p[i_]]:>Sqrt[s[1,2]]*d[x[i]]/2;
%/.d[Omega[2],n_]:>Power[Sin[\[Beta]],n-2]*d[Cos[\[Beta]]]*Power[Sin[\[Alpha]],n-3]*d[Cos[\[Alpha]]]*d[Omega[2],n-2];
%/.d[Omega[1],n_]:>Power[Sin[\[Theta]],n-2]*d[Cos[\[Theta]]]*d[Omega[1],n-1];
%/.{\[Theta][1]->\[Theta],\[Phi][1]->\[Phi],\[Theta][2]->\[Beta],\[Phi][2]->\[Alpha]};
%/.d[\[Beta]]->d[Cos[\[Beta]]]/Sin[\[Beta]];
%/(\[Delta][cosBeta-Cos[\[Beta]],"+"]*d[Cos[\[Beta]]]);
%/.Sin[\[Beta]]:>Sqrt[1-Power[Cos[\[Beta]],2]];
%/.Cos[\[Beta]]->cosBeta;
%//Simplify[#,s[1,2]>=0]&;
%/.d[a_Omega,d_]:>2 Power[\[Pi],(d+1)/2]/Gamma[(d+1)/2];
d[\[CapitalPhi]3]=%//Simplify[#,0<=x[1]<=1&&0<=x[2]<=1]&


d[\[CapitalPhi]3]/.n->dimReg


d[\[CapitalPhi]3]/.n->4


(* ::Section::Closed:: *)
(*ee -> qq, square amplitude*)


zerothFromFeynmanRules=Power[e,4]*Power[Qq,2]*Nc*traceGamma[m[1],p[1],m[2],p[2]]*traceGamma[m[1],p[4],m[2],p[3]]/(4*Power[s[1,2],2]);
zerothSquareAmplitude=applyRules[zerothFromFeynmanRules]/.dotToS//.(momentumConservationInvariants[#,4]&/@{{2,3},{2,4}})//Factor;
zerothFromFeynmanRules;
zerothSquareAmplitude


(* ::Section::Closed:: *)
(*ee-> qqg, real radiation, square amplitude*)


(* ::Subsection::Closed:: *)
(*This is the one that doesn' t work for the dsm4 part*)


part[a_,b_]:=-traceGamma[m[1],p[b],m[2],p[a,5],m[3],p[a],m[3],p[a,5]]/Power[s[a,5],2];
fromFeynmanRules=(Power[gs,2]*Power[e,4]*Nc*Cf*Power[Qq,2]*traceGamma[m[1],p[1],m[2],p[2]])/(4*Power[s[1,2],2])*( 
	part[3,4]+part[4,3]+2*traceGamma[m[1],p[4],m[3],p[4,5],m[2],p[3],m[3],p[3,5]]/(s[3,5]*s[4,5])
	)//Factor;
Module[{sum=applyRules[fromFeynmanRules]/.dotToS//Factor},((Numerator[sum]//.invariantSet)/Denominator[sum])//Factor];
%/.ds->dsm4+4//Collect[#,dsm4,Factor]&;
%/.dsm4->0


(* ::Subsection:: *)
(*Here' s the one that does work*)


replacementsReal = {
  tr[a___,m[i_],x_,y_,z_,m[i_],b___]:>-tr[a,m[i],x,y,m[i],z,b]+2*tr[a,z,x,y,b],
  tr[a___,m[i_],x_,y_,m[i_],b___]:>-tr[a,m[i],x,m[i],y,b]+2*tr[a,y,x,b],
  tr[a___,i_m,x_,i_m,b___]:>(2-dot[i,i])*tr[a,x,b],
  tr[a_,b_,c_,d_,e_,f_]:>(dot[f,e]*tr[a,b,c,d]-dot[f,d]*tr[a,b,c,e]+dot[f,c]*tr[a,b,d,e]-dot[f,b]*tr[a,c,d,e]+dot[f,a]*tr[b,c,d,e]),
  tr[a_,b_,c_,d_]:>4*(dot[a,b]*dot[c,d]-dot[a,c]*dot[b,d]+dot[a,d]*dot[b,c]),
  dot[a_,p[i_,j__]]:>dot[a,p[i]]+dot[a,p[j]],
  dot[a_m,b_]^2:>dot[b,b],
  dot[a_m,b_]*dot[a_m,c_]:>dot[b,c],
  dot[m[3],m[3]]:>ds,
  Condition[dot[m[i_],m[i_]],Or[i==1,i==2]]:>4,
  Power[dot[m[1],m[2]],2]:>4,
  dot[a_,p[i_,j__]]:>dot[a,p[i]]+dot[a,p[j]],
  dot[p[i_],p[j_]]:>s[i,j]/2,
  s[i_,i_]:>0,
  s[a__]:>Plus@@DeleteDuplicates[s@@@Permutations[{a},{2}]],
  ds->dsm4+4
};
applyRulesReal[expr_]:=FixedPoint[Expand[#/.replacementsReal]&,expr];
part[a_,b_]:=-tr[m[1],p[b],m[2],p[a,5],m[3],p[a],m[3],p[a,5]]/Power[s[a,5],2];
tmp1=(tr[m[1],p[1],m[2],p[2]])/(4*Power[s[1,2],2])*( 
	part[3,4]+part[4,3]+2*tr[m[1],p[4],m[3],p[4,5],m[2],p[3],m[3],p[3,5]]/(s[3,5]*s[4,5])
	)//Factor;
tmp2=applyRulesReal[tmp1]//Factor;
tmp3=((Numerator[tmp2]//.invariantSet)/Denominator[tmp2])//Collect[#,dsm4,Factor]&


Numerator[Coefficient[tmp3,dsm4]]/2-(s[1,5]^2+s[2,5]^2)//.invariantSet//Factor


(* ::Subsubsection:: *)
(*Therefore we know that the result can be expressed as*)


squareAmplitudeReal=Power[gs,2]*Power[e,4]*Nc*Cf*Power[Qq,2]*2 /(s[1,2] s[3,5] s[4,5])*(2*(s[1,3]^2+s[1,4]^2+s[2,3]^2+s[2,4]^2)+dsm4*(s[1,5]^2+s[2,5]^2))


(* ::Section::Closed:: *)
(*ee -> qq, Born, \[Sigma]*)


n[0]=Ry[\[Theta]].z;
fixedMomentaZeroth={
	p[1]->Sqrt[s[1,2]]/2 Prepend[z,-1],
	p[2]->Sqrt[s[1,2]]/2 Prepend[-z,-1],
	p[3]->Sqrt[s[1,2]]/2 Prepend[n[0],1]
};
momentaZeroth=Append[fixedMomentaZeroth,momentumConservation[{4},4]/.fixedMomentaZeroth];
diffSigmaZeroth=Power[fluxFactor,-1]*zerothSquareAmplitude*d[\[CapitalPhi]2]/.n->4/.Power[e,4]->Power[\[Alpha]e 4 \[Pi],2]/.sToDot/.performMomentaDots/.momentaZeroth//Factor
sigmaZeroth=%/.Cos[\[Theta]]->c\[Theta]//Integrate[#/d[c\[Theta]],{c\[Theta],-1,1}]&


momentaZeroth


(* ::Section:: *)
(*ee-> qqg, real radiation, \[Sigma]R*)


(* ::Subsection:: *)
(*Define momenta*)


n[1]=Ry[\[Theta]].z;
n[2]=Ry[\[Theta]].Rz[\[Alpha]].Ry[\[Beta]].z;
fixedMomentaReal={
	p[1]->Sqrt[s[1,2]]/2 Prepend[z,-1],
	p[2]->Sqrt[s[1,2]]/2 Prepend[-z,-1],
	p[3]->Sqrt[s[1,2]]/2 x[1] Prepend[n[1],1],
	p[4]->Sqrt[s[1,2]]/2 x[2] Prepend[n[2],1]
};
momentaReal=Append[fixedMomentaReal,momentumConservation[{5},5]/.fixedMomentaReal];


({MatrixForm/@(p/@Range[5]/.momentaReal)}//TableForm)/.s[1,2]->Subscript["s","12"]


Manipulate[
(({MatrixForm/@(p/@Range[5]/.momentaReal)}/.Sin[\[Beta]]->Sqrt[1-Power[Cos[\[Beta]],2]]/.Cos[\[Beta]]->cosBeta/.\[Alpha]->alpha/.\[Theta]->theta/.x[1]->x1/.x[2]->x2/.s[1,2]->1.)//TableForm),
{x1,0,1},
{x2,0,1},
{alpha,0,Pi},
{theta,0,Pi}
]





(* ::Subsection::Closed:: *)
(*Cross section n=4*)


squareAmplitudeReal*d[\[CapitalPhi]3]/fluxFactor;
%/.Power[e,4]->Power[4*\[Pi]*\[Alpha]e,2];
%/.Power[gs,2]->4*\[Pi]*\[Alpha]s;
%/.sToDot;
%/.performMomentaDots;
%/.momentaReal;
%/.n->4;
%/.Sin[x_]:>Sqrt[1-Power[Cos[x],2]];
%/.Csc[x_]:>Power[1-Power[Cos[x],2],-1/2];
%/.Cos[\[Theta]]->c\[Theta];
%/.Cos[\[Alpha]]->c\[Alpha];
%/.Cos[\[Beta]]->cosBeta;
Factor[%];
Integrate[%/d[c\[Alpha]],{c\[Alpha],-1,1},Assumptions->-1/2<\[Epsilon]<0];
Factor[%];
Integrate[%/d[c\[Theta]],{c\[Theta],-1,1},Assumptions->-1/2<\[Epsilon]<0];
Factor[%];
%/sigmaZeroth*\[Sigma]0;
%//Collect[#,dsm4,Factor]&


(* ::Subsection:: *)
(*Cross section n = dimReg*)


squareAmplitudeReal*d[\[CapitalPhi]3];
%/.sToDot;
%/fluxFactor;
%/.Power[e,4]->Power[4*\[Pi]*\[Alpha]e,2];
%/.Power[gs,2]->4*\[Pi]*\[Alpha]s;
%/.performMomentaDots;
%/.momentaReal;
%/.n->dimReg;
%/.Sin[x_]:>Sqrt[1-Power[Cos[x],2]];
%/.Cos[\[Theta]]->c\[Theta];
%/.Cos[\[Alpha]]->c\[Alpha];
%/.Cos[\[Beta]]->cosBeta;
Factor[%];
Integrate[%/d[c\[Alpha]],{c\[Alpha],-1,1},Assumptions->-1/2<\[Epsilon]<0];
Factor[%];
Integrate[%/d[c\[Theta]],{c\[Theta],-1,1},Assumptions->-1/2<\[Epsilon]<0];
Factor[%];
%/sigmaZeroth*\[Sigma]0;
diffRealSigmaX=%


(4 dsm4-4 dsm4 x[1]+2 x[1]^2+dsm4 x[1]^2-4 dsm4 x[2]+2 dsm4 x[1] x[2]+2 x[2]^2+dsm4 x[2]^2)/2//Collect[#,dsm4,Factor]&


diffRealSigmaX/(Cf \[Alpha]s \[Sigma]0 d[x[1]] d[x[2]]/2/\[Pi])/.\[Epsilon]->0//Collect[#,dsm4,Factor]&


preFactor=3*Power[-s[1, 2],-2*\[Epsilon]]*Power[4*\[Pi],2*\[Epsilon]]*(-2+\[Epsilon])*(-1+\[Epsilon])/Gamma[4-2*\[Epsilon]];
expansionPreFactor=(Cf*H*\[Alpha]s*\[Sigma]0)/(2*Pi);
Series[preFactor,{\[Epsilon],0,0}]
diffRealSigmaX/preFactor*H


diffRealSigmaXXtrue=diffRealSigmaX/preFactor*H/expansionPreFactor//Simplify


diffRealSigmaXX=1/2 d[x[1]] d[x[2]] (1-x[1])^(-1-\[Epsilon]) (1-x[2])^(-1-\[Epsilon]) (-1+x[1]+x[2])^-\[Epsilon] (dsm4 (-2+x[1]+x[2])^2+2 (x[1]^2+x[2]^2))


diffRealSigmaXX;
%/.d[x[i_]]:>d[y[i]];
%/.x[i_]:>1-y[i];
Factor[%];
%//PowerExpand;
diffRealSigmaY=%


diffRealSigmaY/.dsm4->0


(4-4 y[1]+2 y[1]^2+dsm4 y[1]^2-4 y[2]+2 dsm4 y[1] y[2]+2 y[2]^2+dsm4 y[2]^2)/2//Collect[#,dsm4,Factor]&


2-2 y[1]+y[1]^2-2 y[2]+y[2]^2-((1-y[1])^2+(1-y[2])^2)//Simplify


Integrate[diffRealSigmaY/d[y[2]],{y[2],0,1-y[1]},Assumptions->Re[\[Epsilon]]<0&&y[1]<1];
Integrate[%/d[y[1]],{y[1],0,1},Assumptions->Re[\[Epsilon]]<0];
realSigma=%/.dsm4->-2*\[Epsilon]*\[Delta]


realSigmaExpansion=Series[realSigma,{\[Epsilon],0,0}]


(* ::Subsection:: *)
(*Cross section DR*)


squareAmplitudeReal*d[\[CapitalPhi]3];
%/.dsm4->0;
%/.sToDot;
%/fluxFactor;
%/.Power[e,4]->Power[4*\[Pi]*\[Alpha]e,2];
%/.Power[gs,2]->4*\[Pi]*\[Alpha]s;
%/.performMomentaDots;
%/.momentaReal;
%/.n->dimReg;
%/.Sin[x_]:>Sqrt[1-Power[Cos[x],2]];
%/.Cos[\[Theta]]->c\[Theta];
%/.Cos[\[Alpha]]->c\[Alpha];
%/.Cos[\[Beta]]->cosBeta;
Factor[%];
Integrate[%/d[c\[Alpha]],{c\[Alpha],-1,1},Assumptions->-1/2<\[Epsilon]<0];
Factor[%];
Integrate[%/d[c\[Theta]],{c\[Theta],-1,1},Assumptions->-1/2<\[Epsilon]<0];
Factor[%];
%/sigmaZeroth*\[Sigma]0;
diffRealSigmaXdr=%


diffRealSigmaXdr/.d[x[i_]]:>d[y[i]];
%/.x[i_]:>1-y[i];
Factor[%];
diffRealSigmaYdr=%


Hdr2=-3 Sqrt[\[Pi]]/8*(2)^(6\[Epsilon])*(Pi)^(2\[Epsilon])*(s[1,2])^(-2\[Epsilon])*Gamma[3-\[Epsilon]]/((-1+\[Epsilon]) Gamma[1-\[Epsilon]]^2 Gamma[5/2-\[Epsilon]])
Series[Hdr2,{\[Epsilon],0,0}]
preFacDR=Cf*\[Alpha]s*\[Sigma]0/2/Pi;
diffRealSigmaYdr2=diffRealSigmaYdr/Hdr2/preFacDR


Integrate[diffRealSigmaYdr2/d[y[2]],{y[2],0,1-y[1]},Assumptions->Re[\[Epsilon]]<0&&y[1]<1];
Integrate[%/d[y[1]],{y[1],0,1},Assumptions->Re[\[Epsilon]]<0&&s[1,2]>0];
realSigmaDR2=%


realSigmaExpansionDR2=Series[realSigmaDR2,{\[Epsilon],0,0}]


(* ::Section::Closed:: *)
(*Simon's e+e- \[RightArrow] qq virtual*)


VolumeIntegrals = {dOmega[d_,p_]:>Simplify[2 Pi^((d+1)/2)/Gamma[(d+1)/2]]};


dot[p1_MOM4, p2_MOM4]:=p1[[1]]*p2[[1]]-(p1[[2]]*p2[[2]]+p1[[3]]*p2[[3]]+p1[[4]]*p2[[4]]);
sq[p1_MOM4]:=dot[p1,p1];
MSUM4[args__]:= MOM4@@Plus@@(List@@@{args})
Rx[a_] := {{1,0,0},{0,Cos[a],Sin[a]},{0,-Sin[a],Cos[a]}};
Ry[a_] := {{Cos[a],0,Sin[a]},{0,1,0},{-Sin[a],0,Cos[a]}};
Rz[a_] := {{Cos[a],Sin[a],0},{-Sin[a],Cos[a],0},{0,0,1}};


ampqq = tr[mu[1],p[1],mu[2],p[2]]/s[1,2] /. tr[a_,b_,c_,d_]:>4*(dot[a,b]*dot[c,d]-dot[a,c]*dot[b,d]+dot[a,d]*dot[b,c]);


ee = tr[mu[2],p[a],mu[1],p[b]]/s[a,b] /. tr[a_,b_,c_,d_]:>4*(dot[a,b]*dot[c,d]-dot[a,c]*dot[b,d]+dot[a,d]*dot[b,c]);


ee*ampqq/4 // Expand;
% //. {
  dot[a_mu,b_]^2:>dot[b,b],
  dot[a_mu,b_]*dot[a_mu,c_]:>dot[b,c],
  dot[b_,a_mu]*dot[a_mu,c_]:>dot[b,c],
  dot[b_,a_mu]*dot[c_,a_mu]:>dot[b,c],
  dot[mu[1],mu[2]]^2->4, 
  dot[m_mu,m_mu]:>4};
% //. {
	dot[a_,p[i_,j__]]:>dot[a,p[i]]+dot[a,p[j]],
	dot[p[i_,j__],a_]:>dot[a,p[i]]+dot[a,p[j]]
	};
	% /. ds->4;
MSQ2 = % /. dot[p[i_],p[j_]]:>s@@Sort[{i,j}]/2  /. s[i_,i_]:>0 /. {s[1,b]->s[2,a], s[1,2]->s[a,b], s[2,b]->s[1,a]} // Factor


pa = -Sqrt[s[a,b]]/2*{1,0,0,1};
pb = -Sqrt[s[a,b]]/2*{1,0,0,-1};
p1 = x1*Sqrt[s[a,b]]/2*Join[{1},Rz[\[Phi]].Ry[\[Theta]].{0,0,1}];
p2 = -pa-pb-p1;

{pa,pb,p1,p2} = MOM4@@@{pa,pb,p1,p2};

(* d-dimesional phase-space *)
1/(2*Pi)^(d-2) DD[d-1,p[1]]/2/E[1]*Delta[Simplify[sq[p2]]] /. {E[1]->p1[[1]]};
% /. {DD[d-1,p[1]]->p1[[1]]^(d-2)DD[p1[[1]]]*dOmega[d-2,p[1]]} // Simplify[#,Assumptions->s[a,b]>0]&;
% /. dOmega[d-2,p[1]]->Sin[\[Theta]]^(d-3)DD[\[Theta]]dOmega[d-3,p[1]];
% /. DD[1/2*x1*Sqrt[s[a,b]]]->1/2*Sqrt[s[a,b]]*DD[x1];
% // Simplify[#,Assumptions->s[a,b]>0]&
% //. {Cos[\[Theta]]->c\[Theta], Sin[\[Theta]]->(1-c\[Theta]^2)^(1/2), DD[\[Theta]]->DD[c\[Theta]]/(1-c\[Theta]^2)^(1/2)} //Simplify;
% /. A_*Delta[x_]:>(A/DD[x1]/Abs[Coefficient[x,x1]] /. Flatten[Solve[x==0,x1]]);
DD[\[CapitalPhi]2] = FullSimplify[%,Assumptions->s[a,b]>0&&-1<c\[Theta]<1]
{pa,pb,p1,p2} =  {pa,pb,p1,p2} /. x1->1;


(* replacement rules for invariants *)
ToEnergyAngles2 = {
s[1,a]->2*dot[p1,pa],
s[1,b]->2*dot[p1,pb],
s[2,a]->2*dot[p2,pa],
s[2,b]->2*dot[p2,pb],
s[1,2]->2*dot[p1,p2]
} /. Sin[x_]^2:>1-Cos[x]^2 // Factor;


d\[Sigma] = ge^4*Nc*1/2/s[a,b]*MSQ2*DD[\[CapitalPhi]2] /. d->4 /. dOmega[1,p[1]]->DD[\[Phi]];
% /. ToEnergyAngles2 // Factor;
% /. Sin[x_]^2:>1-Cos[x]^2 // Factor;
% //. {Cos[\[Theta]]->c\[Theta], Sin[\[Theta]]->(1-c\[Theta]^2)^(1/2), DD[\[Theta]]->DD[c\[Theta]]/(1-c\[Theta]^2)^(1/2)} //Factor;
Integrate[%/DD[\[Phi]],{\[Phi],0,2*Pi}];
Integrate[%/DD[c\[Theta]],{c\[Theta],-1,1}];
\[Sigma]LO = % /. {ge^4->(\[Alpha]*4*Pi)^2, gs^2->Subscript[\[Alpha], s]*4*Pi,ds->dsm4+4} // Collect[#,dsm4,Factor]&


(* 1-loop vertex divided by tree-level vertex *)
cGamma = 1/(4*Pi)^(2-eps)*Gamma[1+eps]*Gamma[1-eps]^2/Gamma[1-2*eps];
Amp1Loop = (-2*s[1,2]*INT[3,s[1,2]]+(3-dsm4/2)*INT[2,s[1,2]]+(2+dsm4)*INT[\[Mu]11][3,s[1,2]]);


LoopIntegrals = {
INT[3,s[1,2]] -> cGamma/s[1,2]/eps^2*(-muR^2/s[1,2])^eps,
INT[2,s[1,2]] -> -cGamma/eps/(1-2*eps)*(-muR^2/s[1,2])^eps,
INT[\[Mu]11][3,s[1,2]] -> -1/2/(4*Pi)^2 (* extra dim. triangle *)
};


pa = -Sqrt[s[a,b]]/2*{1,0,0,1};
pb = -Sqrt[s[a,b]]/2*{1,0,0,-1};
p1 = x1*Sqrt[s[a,b]]/2*Join[{1},Rz[\[Phi]].Ry[\[Theta]].{0,0,1}];
p2 = -pa-pb-p1;

{pa,pb,p1,p2} = MOM4@@@{pa,pb,p1,p2};

(* d-dimesional phase-space *)
1/(2*Pi)^(d-2) DD[d-1,p[1]]/2/E[1]*Delta[Simplify[sq[p2]]] /. {E[1]->p1[[1]]};
% /. {DD[d-1,p[1]]->p1[[1]]^(d-2)DD[p1[[1]]]*dOmega[d-2,p[1]]} // Simplify[#,Assumptions->s[a,b]>0]&;
% /. dOmega[d-2,p[1]]->Sin[\[Theta]]^(d-3)DD[\[Theta]]dOmega[d-3,p[1]];
% /. DD[1/2*x1*Sqrt[s[a,b]]]->1/2*Sqrt[s[a,b]]*DD[x1];
% // Simplify[#,Assumptions->s[a,b]>0]&
% //. {Cos[\[Theta]]->c\[Theta], Sin[\[Theta]]->(1-c\[Theta]^2)^(1/2), DD[\[Theta]]->DD[c\[Theta]]/(1-c\[Theta]^2)^(1/2)} //Simplify;
% /. A_*Delta[x_]:>(A/DD[x1]/Abs[Coefficient[x,x1]] /. Flatten[Solve[x==0,x1]]);
DD[\[CapitalPhi]2] = FullSimplify[%,Assumptions->s[a,b]>0&&-1<c\[Theta]<1]
{pa,pb,p1,p2} =  {pa,pb,p1,p2} /. x1->1;


(Sin[\[Theta]])^(d-4)*(4*Pi)^(2-d)*(s[a,b])^((d-4)/2)/2
DD[\[CapitalPhi]2]/% //. -1+c\[Theta]^2->-Sin[\[Theta]]^2 // FullSimplify[#,Assumptions->s[a,b]>0&&0<\[Theta]<Pi]&


d\[Sigma]Virtual = ge^4*1/2/s[a,b]*gs^2*CF*Nc*MSQ2*Amp1Loop*DD[\[CapitalPhi]2] /. LoopIntegrals /. muR->1;
% /. d->4-2*eps /. ToEnergyAngles2 // Factor;
% /. Sin[x_]^2:>1-Cos[x]^2 // Factor;
tmp = % //. {Cos[\[Theta]]->c\[Theta], Sin[\[Theta]]->(1-c\[Theta]^2)^(1/2), DD[\[Theta]]->DD[c\[Theta]]/(1-c\[Theta]^2)^(1/2)} // FullSimplify[#,Assumptions->s[a,b]>0]&
(* Integrate on theta *)
tmp = Integrate[tmp/DD[c\[Theta]],{c\[Theta],-1,1},Assumptions->-1/2<eps<0]


PreFactor = 3*(2-eps)/(3-2*eps)/(4*Pi)^(-4*eps)/s[a,b]^(2*eps)*Pi^(1/2-2*eps)*2^(-2-2*eps)/Gamma[1-eps]/Gamma[3/2-eps] // FullSimplify


tmp/PreFactor/\[Sigma]LO/(CF*Subscript[\[Alpha], s]/(2*Pi)) /. VolumeIntegrals /. {ge^4->(\[Alpha]*4*Pi)^2, gs^2->Subscript[\[Alpha], s]*4*Pi,ds->dsm4+4};
\[Sigma]Virtual = 2*Re[Expand[Normal[Series[% /. dsm4->-2*eps*\[Delta],{eps,0,0}]]]]//FullSimplify[#,Assumptions->s[a,b]>0&&-1/2<eps<0&&Im[\[Delta]]==0]&;
Expand[%]


(* ::Section:: *)
(*Total NLO correction*)

(* ::Package:: *)

SetAttributes[dot,Orderless];
SetAttributes[s,Orderless];
unpackMomenta=p[i_,j__]:>p[i]+p[j];
sToDot=s[i_,j_]:>2*dot[p[i],p[j]];
dotToS=dot[p[i_],p[j_]]:>s[i,j]/2;
rules={
	traceGamma[a__]:>trace@@(dot[gamma,#]&/@{a}),
	trace[a___,dot[gamma,q_p],b___]:>Module[{j},dot[q,m[j]]trace[a,dot[gamma,m[j]],b]],
	dot[p[a_],p[a_]]->0,
	Power[dot[p[i_],m_m],2]->0,
	dot[p[x_],i_m]*dot[p[x_],j_m]*trace[a___,dot[gamma,i_m],dot[gamma,j_m],b___]->0,
	trace[a___,dot[gamma,i_m],dot[gamma,i_m],b___]:>dot[i,i]*trace[a,b],
	Condition[dot[m[i_],m[i_]],Or[i==1,i==2]]:>4,(*4 here is the number of Lorentz indices on the photon*)
	Power[dot[m[1],m[2]],2]:>4,(*4 here is the number of Lorentz indices on the photon*)
	dot[m[3],m[3]]->ds, (*ds is the number of Lorentz indices on the gluon*)
	dot[i_,j_m]*dot[j_m,k_]:>dot[i,k],
	dot[i_m,j_m]*trace[a___,dot[gamma,j_m],b___]:>trace[a,dot[gamma,i],b],
	Condition[dot[i_m,q_p]*dot[j_m,q_p]*trace[a___,dot[gamma,i_m],dot[gamma,k_m],b___,dot[gamma,j_m],c___],
Length[{b}]<((Length[{a}]+Length[{b}]+Length[{c}]+3)/2)]:>
	2*dot[k,q]*dot[j,q]*trace[a,b,dot[gamma,j],c]-dot[i,q]*dot[j,q]*trace[a,dot[gamma,k],dot[gamma,i],b,dot[gamma,j],c],
	Condition[dot[i_m,q_p]*dot[j_m,q_p]*trace[a___,dot[gamma,i_m],b__,dot[gamma,j_m],c___],
Length[{b}]>((Length[{a}]+Length[{b}]+Length[{c}]+2)/2-1)]:>
	dot[i,q]*dot[j,q]*trace[dot[gamma,j],c,a,dot[gamma,i],b],
	Condition[trace[a___,dot[gamma,i_m],dot[gamma,j_m],b___,dot[gamma,i_m],c___],
Length[{b}]<((Length[{a}]+Length[{b}]+Length[{c}]+3)/2)]:>
	-trace[a,dot[gamma,j],dot[gamma,i],b,dot[gamma,i],c]+2*trace[a,b,dot[gamma,j],c],
	Condition[trace[a___,dot[gamma,i_m],b__,dot[gamma,i_m],c___],
Length[{b}]>((Length[{a}]+Length[{b}]+Length[{c}]+2)/2-1)]:>
	trace[dot[gamma,i],c,a,dot[gamma,i],b],
	unpackMomenta,
	dot[a_,b_p+c_p]:>dot[a,b]+dot[a,c],
	trace[dot[gamma,i_m],dot[gamma,j_m]]:>4*dot[i,j],(*4 here is the number of spinor indices*)
	trace[dot[gamma,i_m],dot[gamma,j_m],dot[gamma,k_m],dot[gamma,l_m]]:>4*(dot[i,j]*dot[k,l]-dot[i,k]*dot[j,l]+dot[i,l]*dot[j,k])(*4 here from Clifford algebra*)
};
applyRules[expr_]:=FixedPoint[Expand[#/.rules]&,expr];
momentumConservationInvariants[{a__},n_]:=s[a]->s@@Delete[Range@n,{#}&/@{a}];
invariantSet=Join[momentumConservationInvariants[#,5]&/@{{1,5},{2,5},{3,5},{4,5}},{s[3,4]->-(s[1,2,3]+s[1,4]+s[2,4]),s[a__]:>Plus@@DeleteDuplicates[s@@@Permutations[{a},{2}]]}];
minkowskiMetric=DiagonalMatrix[{1,-1,-1,-1}];
performMomentaDots={dot[x_,y_]:>x.minkowskiMetric.y};
z ={0,0,1};
Rx[a_] := {{1,0,0},{0,Cos[a],-Sin[a]},{0,Sin[a],Cos[a]}};
Ry[a_]:={{Cos[a],0,Sin[a]},{0,1,0},{-Sin[a],0,Cos[a]}};
Rz[a_]:={{Cos[a],-Sin[a],0},{Sin[a],Cos[a],0},{0,0,1}};
momentumConservation[{a__},n_]:=p[a]->-p@@Delete[Range@n,{#}&/@{a}]//.unpackMomenta;
fluxFactor=2*s[1,2];
dimReg=4-2*\[Epsilon];
sigmaZeroth=(4 Nc \[Pi] Qq^2 \[Alpha]e^2)/(3 s[1,2]);
d[\[CapitalPhi]3]=(2^(4-3 n) \[Pi]^(1/2-n) d[Cos[\[Alpha]]] d[Cos[\[Theta]]] d[x[1]] d[x[2]] s[1,2]^(-3+n) Sin[\[Alpha]]^(-5+n) Sin[\[Theta]]^(-4+n) ((-1+x[1]) (-1+x[2]) (-1+x[1]+x[2]))^(n/2))/(Gamma[1/2 (-3+n)] Gamma[1/2 (-2+n)] (-1+x[1])^2 (-1+x[2])^2 (-1+x[1]+x[2])^2);
squareAmplitudeReal=Power[gs,2]*Power[e,4]*Nc*Cf*Power[Qq,2]*2 /(s[1,2] s[3,5] s[4,5])*(2*(s[1,3]^2+s[1,4]^2+s[2,3]^2+s[2,4]^2)+dsm4*(s[1,5]^2+s[2,5]^2));
n[1]=Ry[\[Theta]].z;
n[2]=Ry[\[Theta]].Rz[\[Alpha]].Ry[\[Beta]].z;
fixedMomentaReal={
	p[1]->Sqrt[s[1,2]]/2 Prepend[z,-1],
	p[2]->Sqrt[s[1,2]]/2 Prepend[-z,-1],
	p[3]->Sqrt[s[1,2]]/2 x[1] Prepend[n[1],1],
	p[4]->Sqrt[s[1,2]]/2 x[2] Prepend[n[2],1]
};
momentaReal=Append[fixedMomentaReal,momentumConservation[{5},5]/.fixedMomentaReal];
expansionPreFactor=(Cf*\[Alpha]s*\[Sigma]0)/(2*Pi);
cosBeta=1+2*(1-x[1]-x[2])/(x[1] x[2]);


squareAmplitudeReal*d[\[CapitalPhi]3];
%/.sToDot;
%/fluxFactor;
%/.Power[e,4]->Power[4*\[Pi]*\[Alpha]e,2];
%/.Power[gs,2]->4*\[Pi]*\[Alpha]s;
%/.performMomentaDots;
%/.momentaReal;
%/.n->dimReg;
tmp0=%//Factor


tmp0/.Sin[x_]:>Sqrt[1-Power[Cos[x],2]];
%/.Cos[\[Theta]]->c\[Theta];
%/.Cos[\[Alpha]]->c\[Alpha];
%/.Cos[\[Beta]]->cosBeta;
Factor[%];
Integrate[%/d[c\[Alpha]],{c\[Alpha],-1,1},Assumptions->-1/2<\[Epsilon]<0];
Factor[%];
Integrate[%/d[c\[Theta]],{c\[Theta],-1,1},Assumptions->-1/2<\[Epsilon]<0];
Factor[%];
%/sigmaZeroth*\[Sigma]0;
tmp1=%//FullSimplify


expr0=tmp1/preFactor1/expansionPreFactor


tmp1/.d[x[i_]]:>d[y[i]];
%/.x[i_]:>1-y[i];
diffRealSigmaY=%//FullSimplify[#,0<=y[1]<=1&&0<=y[2]<=1]&


expr1=diffRealSigmaY/preFactor1/expansionPreFactor


int1=Integrate[diffRealSigmaY/d[y[2]],{y[2],0,1-y[1]},Assumptions->Re[\[Epsilon]]<0&&y[1]<1]


int2=Integrate[int1/d[y[1]],{y[1],0,1},Assumptions->Re[\[Epsilon]]<0&&s[1,2]>0]


realSigma=int2/.dsm4->-2*\[Epsilon]*\[Delta]//FullSimplify


preFactor1=3*Power[s[1, 2],-2*\[Epsilon]]*Power[4*\[Pi],2*\[Epsilon]]*(-2+\[Epsilon])*(-1+\[Epsilon])/Gamma[4-2*\[Epsilon]]
Series[preFactor1,{\[Epsilon],0,0}]//Simplify
Series[realSigma/preFactor1/expansionPreFactor,{\[Epsilon],0,0}]//FullSimplify


preFactor2=3*(2-\[Epsilon])/(2*(3-2\[Epsilon]))*(4\[Pi])^(2\[Epsilon])/Gamma[2-2\[Epsilon]]*s[1,2]^(-2\[Epsilon])
Series[preFactor2,{\[Epsilon],0,0}]//Simplify
Series[realSigma/preFactor2/expansionPreFactor,{\[Epsilon],0,0}]//FullSimplify


preFactor0=Power[s[1, 2],-2*\[Epsilon]]*Power[4*\[Pi],2*\[Epsilon]]*Exp[-2EulerGamma \[Epsilon]]Exp[(13/6)\[Epsilon]]
Series[preFactor0,{\[Epsilon],0,0}]
Series[realSigma/preFactor0/expansionPreFactor,{\[Epsilon],0,0}]//FullSimplify




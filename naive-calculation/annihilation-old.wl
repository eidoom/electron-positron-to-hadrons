(* ::Package:: *)

(* ::Section:: *)
(*Some set up*)


SetAttributes[dot,Orderless];
SetAttributes[s,Orderless];
unpackMomenta=p[i_,j__]:>p[i]+p[j];
sToDot=s[i_,j_]:>2*dot[p[i],p[j]];
dotToS=dot[p[i_],p[j_]]:>s[i,j]/2;
rules={
	traceGamma[a__]:>trace@@(dot[gamma,#]&/@{a}),
	trace[a___,dot[gamma,q_p],b___]:>Module[{j},dot[q,m[j]]trace[a,dot[gamma,m[j]],b]],
	dot[p[a_],p[a_]]->0,
	Power[dot[p[i_],m_m],2]->0,
	dot[p[x_],i_m]*dot[p[x_],j_m]*trace[a___,dot[gamma,i_m],dot[gamma,j_m],b___]->0,
	trace[a___,dot[gamma,i_m],dot[gamma,i_m],b___]:>dot[i,i]*trace[a,b],
	Condition[dot[m[i_],m[i_]],Or[i==1,i==2]]:>4,(*4 here is the number of Lorentz indices on the photon*)
	Power[dot[m[1],m[2]],2]:>4,(*4 here is the number of Lorentz indices on the photon*)
	dot[m[3],m[3]]->ds, (*ds is the number of Lorentz indices on the gluon*)
	dot[i_,j_m]*dot[j_m,k_]:>dot[i,k],
	dot[i_m,j_m]*trace[a___,dot[gamma,j_m],b___]:>trace[a,dot[gamma,i],b],
	Condition[dot[i_m,q_p]*dot[j_m,q_p]*trace[a___,dot[gamma,i_m],dot[gamma,k_m],b___,dot[gamma,j_m],c___],
Length[{b}]<((Length[{a}]+Length[{b}]+Length[{c}]+3)/2)]:>
	2*dot[k,q]*dot[j,q]*trace[a,b,dot[gamma,j],c]-dot[i,q]*dot[j,q]*trace[a,dot[gamma,k],dot[gamma,i],b,dot[gamma,j],c],
	Condition[dot[i_m,q_p]*dot[j_m,q_p]*trace[a___,dot[gamma,i_m],b__,dot[gamma,j_m],c___],
Length[{b}]>((Length[{a}]+Length[{b}]+Length[{c}]+2)/2-1)]:>
	dot[i,q]*dot[j,q]*trace[dot[gamma,j],c,a,dot[gamma,i],b],
	Condition[trace[a___,dot[gamma,i_m],dot[gamma,j_m],b___,dot[gamma,i_m],c___],
Length[{b}]<((Length[{a}]+Length[{b}]+Length[{c}]+3)/2)]:>
	-trace[a,dot[gamma,j],dot[gamma,i],b,dot[gamma,i],c]+2*trace[a,b,dot[gamma,j],c],
	Condition[trace[a___,dot[gamma,i_m],b__,dot[gamma,i_m],c___],
Length[{b}]>((Length[{a}]+Length[{b}]+Length[{c}]+2)/2-1)]:>
	trace[dot[gamma,i],c,a,dot[gamma,i],b],
	unpackMomenta,
	dot[a_,b_p+c_p]:>dot[a,b]+dot[a,c],
	trace[dot[gamma,i_m],dot[gamma,j_m]]:>4*dot[i,j],(*4 here is the number of spinor indices*)
	trace[dot[gamma,i_m],dot[gamma,j_m],dot[gamma,k_m],dot[gamma,l_m]]:>4*(dot[i,j]*dot[k,l]-dot[i,k]*dot[j,l]+dot[i,l]*dot[j,k])(*4 here from Clifford algebra*)
};
applyRules[expr_]:=FixedPoint[Expand[#/.rules]&,expr];
momentumConservationInvariants[{a__},n_]:=s[a]->s@@Delete[Range@n,{#}&/@{a}];
invariantSet=Join[momentumConservationInvariants[#,5]&/@{{1,5},{2,5},{3,5},{4,5}},{s[3,4]->-(s[1,2,3]+s[1,4]+s[2,4]),s[a__]:>Plus@@DeleteDuplicates[s@@@Permutations[{a},{2}]]}];
minkowskiMetric=DiagonalMatrix[{1,-1,-1,-1}];
performMomentaDots={dot[x_,y_]:>x.minkowskiMetric.y};
z ={0,0,1};
Rx[a_] := {{1,0,0},{0,Cos[a],-Sin[a]},{0,Sin[a],Cos[a]}};
Ry[a_]:={{Cos[a],0,Sin[a]},{0,1,0},{-Sin[a],0,Cos[a]}};
Rz[a_]:={{Cos[a],-Sin[a],0},{Sin[a],Cos[a],0},{0,0,1}};
momentumConservation[{a__},n_]:=p[a]->-p@@Delete[Range@n,{#}&/@{a}]//.unpackMomenta;
fluxFactor=2s[1,2];
dimReg=4-2*\[Epsilon];


(* ::Section::Closed:: *)
(*Two body phase space*)


d[\[CapitalPhi]2]=Power[s[1,2],(n-4)/2]*Power[2,4-2*n]*Power[\[Pi],(2-n)/2]*Power[Sin[\[Theta]],n-4]*d[Cos[\[Theta]]]/Gamma[(n-2)/2]


d[\[CapitalPhi]2]/.n->4


(* ::Section::Closed:: *)
(*Three body phase space*)


cosBeta=1+2*(1-x[1]-x[2])/(x[1] x[2]);


Product[d[p[i],n]*Power[2*\[Pi],1-n]*\[Delta][Power[p[i],2],"+"],{i,1,3}]\[Delta][Sum[p[i],{i,3}]-q,n]Power[2*\[Pi],n];
%/d[p[3],n]/\[Delta][Sum[p[i],{i,3}]-q,n];
(%/Product[\[Delta][Power[p[i],2],"+"],{i,1,2}])/.d[p[i_],n]:>d[p[i],n-1]/(2*en[i]);
(%*2/(s[1,2]*x[1]*x[2]))/.Power[p[3],2]->(cosBeta-Cos[\[Beta]]);
%/.d[p[i_],n-1]:>Power[en[i],n-2]d[p[i]]d[\[CapitalOmega][i],n-2];
%/.en[i_]:>Sqrt[s[1,2]]*x[i]/2;
%/.d[p[i_]]:>Sqrt[s[1,2]]*d[x[i]]/2;
%/.d[\[CapitalOmega][2],n_]:>Power[Sin[\[Beta]],n-2]*d[Cos[\[Beta]]]*Power[Sin[\[Alpha]],n-3]*d[Cos[\[Alpha]]]*d[\[CapitalOmega][2],n-2];
%/.d[\[CapitalOmega][1],n_]:>Power[Sin[\[Theta]],n-2]*d[Cos[\[Theta]]]*d[\[CapitalOmega][1],n-1];
%/.{\[Theta][1]->\[Theta],\[Phi][1]->\[Phi],\[Theta][2]->\[Beta],\[Phi][2]->\[Alpha]};
%/.d[\[Beta]]->d[Cos[\[Beta]]]/Sin[\[Beta]];
%/(\[Delta][cosBeta-Cos[\[Beta]],"+"]*d[Cos[\[Beta]]]);
%/.Sin[\[Beta]]:>Sqrt[1-Power[Cos[\[Beta]],2]];
%/.Cos[\[Beta]]->cosBeta;
%//Simplify[#,s[1,2]>0]&;
%//PowerExpand;
%/.d[x[i_]]:>d[y[i]];
%/.x[i_]:>1-y[i];
%/.d[\[CapitalOmega]_ \[CapitalOmega],d_]:>2 Power[\[Pi],(d+1)/2]/Gamma[(d+1)/2];
d[\[CapitalPhi]3]=%//Simplify[#,y[1]>0&&y[2]>0]&//PowerExpand


d[\[CapitalPhi]3]/.n->4


(* ::Section::Closed:: *)
(*ee -> qq, square amplitude*)


zerothFromFeynmanRules=Power[e,4]*Power[Qq,2]*Nc*traceGamma[m[1],p[1],m[2],p[2]]*traceGamma[m[1],p[4],m[2],p[3]]/(4*Power[s[1,2],2]);
zerothSquareAmplitude=applyRules[zerothFromFeynmanRules]/.dotToS//.(momentumConservationInvariants[#,4]&/@{{2,3},{2,4}})//Factor;
zerothFromFeynmanRules;
zerothSquareAmplitude


(* ::Section::Closed:: *)
(*ee -> qq, Born, \[Sigma]*)


n[0]=Ry[\[Theta]].z;
fixedMomentaZeroth={
	p[1]->Sqrt[s[1,2]]/2 Prepend[z,-1],
	p[2]->Sqrt[s[1,2]]/2 Prepend[-z,-1],
	p[3]->Sqrt[s[1,2]]/2 Prepend[n[0],1]
};
momentaZeroth=Append[fixedMomentaZeroth,momentumConservation[{4},4]/.fixedMomentaZeroth];
diffSigmaZeroth=Power[fluxFactor,-1]*zerothSquareAmplitude*d[\[CapitalPhi]2]/.n->4/.Power[e,4]->Power[\[Alpha]e 4 \[Pi],2]/.sToDot/.performMomentaDots/.momentaZeroth//Factor
sigmaZeroth=%/.Cos[\[Theta]]->c\[Theta]//Integrate[#/d[c\[Theta]],{c\[Theta],-1,1}]&


momentaZeroth


(* ::Section:: *)
(*ee-> qqg, real radiation, square amplitude*)


part[a_,b_]:=-traceGamma[m[1],p[b],m[2],p[a,5],m[3],p[a],m[3],p[a,5]]/Power[s[a,5],2];
fromFeynmanRules=(Power[gs,2]*Power[e,4]*Nc*Cf*Power[Qq,2]*traceGamma[m[1],p[1],m[2],p[2]])/(4*Power[s[1,2],2])*( 
	part[3,4]+part[4,3]+2*traceGamma[m[1],p[4],m[3],p[4,5],m[2],p[3],m[3],p[3,5]]/(s[3,5]*s[4,5])
	)//Factor;
squareAmplitude=Module[{sum=applyRules[fromFeynmanRules]/.dotToS//Factor},((Numerator[sum]//.invariantSet)/Denominator[sum])//Factor];
fromFeynmanRules;
squareAmplitude/.ds->dsm4+4//Collect[#,dsm4,Factor]&;
%/.dsm4->0


(*Subsets[Range@5,{2}]
{#,Mod[#,5]+1}&/@Range@5*)
momentumConservationInvariantsEquations[{a__},n_]:=s[a]==s@@Delete[Range@n,{#}&/@{a}];
expandInvariants=s[a__]:>Plus@@DeleteDuplicates[s@@@Permutations[{a},{2}]];
complementarySet={{1,3},{1,4},{2,4},{2,5},{3,5}};
independentInvariantEquations=momentumConservationInvariantsEquations[#,5]&/@complementarySet/.expandInvariants;
independentInvariantReplacements=Solve[independentInvariantEquations,s@@@complementarySet][[1]];
independentInvariantReplacements//TableForm


DeleteCases[Join[{#,Mod[#,5]+2}&/@Range@5,{#,Mod[#,5]+4}&/@Range@5],{}]


mine=s[1,3]^2+s[1,4]^2-2 s[1,4] s[2,3]+s[2,3]^2-2 s[1,3] s[2,4]+s[2,4]^2/.independentInvariantReplacements//Factor


simons=(s[1,a]^2+s[1,b]^2+2 s[1,a] s[2,a]+s[2,a]^2+2 s[1,b] s[2,b]+s[2,b]^2+2 s[1,a] s[a,b]+2 s[1,b] s[a,b]+2 s[2,a] s[a,b]+2 s[2,b] s[a,b]+2 s[a,b]^2)/.
	{s[a_,i_Integer]->s[a,i+2]}/.{a->1,b->2}/.independentInvariantReplacements//Factor


simons-mine/.independentInvariantReplacements


(* ::Section::Closed:: *)
(*ee -> qq, virtual corrections, square amplitude*)


(*TODO*)


(* ::Section::Closed:: *)
(*ee-> qqg, real radiation, \[Sigma]R*)


n[1]=Ry[\[Theta]].z;
n[2]=Ry[\[Theta]].Rz[\[Alpha]].Ry[\[Beta]].z;
fixedMomentaReal={
	p[1]->Sqrt[s[1,2]]/2 Prepend[z,-1],
	p[2]->Sqrt[s[1,2]]/2 Prepend[-z,-1],
	p[3]->Sqrt[s[1,2]]/2 x[1] Prepend[n[1],1],
	p[4]->Sqrt[s[1,2]]/2 x[2]  Prepend[n[2],1]
};
momentaReal=Append[fixedMomentaReal,momentumConservation[{5},5]/.fixedMomentaReal];


Power[fluxFactor,-1]*squareAmplitude*d[\[CapitalPhi]3]/.{Power[e,4]->12 s[1,2] \[Pi] \[Sigma]0,Power[gs,2]->4\[Pi] \[Alpha]s}//Factor;
%/.sToDot/.performMomentaDots/.momentaReal;
%/.{n->dimReg,ds->4}//Factor;
%/.Sin[x_]:>Sqrt[1-Power[Cos[x],2]]//Factor;
%/.{Cos[\[Theta]]->c\[Theta],Cos[\[Alpha]]->c\[Alpha]};
%/.Cos[\[Beta]]->cosBeta;
%/.x[i_]:>1-y[i];
%//Simplify[#,y[1]>0&&y[2]>0]&;
%//PowerExpand;
Integrate[%/d[c\[Alpha]],{c\[Alpha],-1,1},Assumptions->-1/2<\[Epsilon]<0]//Factor;
Integrate[%/d[c\[Theta]],{c\[Theta],-1,1},Assumptions->-1/2<\[Epsilon]<0]//Factor;
diffRealSigma=%


%/.\[Epsilon]->0


MatrixForm/@(p/@Range[5]/.momentaReal)

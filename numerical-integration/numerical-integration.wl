(* ::Package:: *)

zHat={0,0,1};
Rx[a_]:={{1,0,0},{0,Cos[a],-Sin[a]},{0,Sin[a],Cos[a]}};
Ry[a_]:={{Cos[a],0,Sin[a]},{0,1,0},{-Sin[a],0,Cos[a]}};
Rz[a_]:={{Cos[a],-Sin[a],0},{Sin[a],Cos[a],0},{0,0,1}};
(* measure=d[\[Alpha]]*d[\[Theta]]*d[x[3]]*d[x[4]]; *)
evalIntegrand[s12_?NumericQ,\[Alpha]_?NumericQ,\[Theta]_?NumericQ,y3_?NumericQ,y4_?NumericQ]:=Module[
	{Ar,Ac45,As5c45,Ac35,As5c35,Asp,Aspp,integrand,sol,c\[Beta],s\[Beta],m,z,J,x,p,s},
	x[3]=1-y3;
	x[4]=1-y4;
	c\[Beta]=1+2*(1-x[3]-x[4])/(x[3]*x[4]);
	s\[Beta]=Sqrt[1-Power[c\[Beta],2]];
	p[1]=Sqrt[s12]/2*Prepend[zHat,-1];
	p[2]=Sqrt[s12]/2*Prepend[-zHat,-1];
	p[3]=Sqrt[s12]/2*x[3]*Prepend[Ry[\[Theta]].zHat,1];
	p[4]=Sqrt[s12]/2*x[4]*Prepend[Ry[\[Theta]].Rz[\[Alpha]].Ry[\[Beta]].zHat,1]/.Sin[\[Beta]]->s\[Beta]/.Cos[\[Beta]]->c\[Beta];
	p[5]=-p[1]-p[2]-p[3]-p[4];
	s[i_,j_]:=2*(p[i][[1]]*p[j][[1]]-p[i][[2]]*p[j][[2]]-p[i][[3]]*p[j][[3]]-p[i][[4]]*p[j][[4]]);
	p[p3t]=p[3]+s[4,5]*p[3]/(s[3,4]+s[3,5]);
	p[p4t]=p[4]+s[3,5]*p[4]/(s[3,4]+s[4,5]);
	z[3]=s[3,4]/(s[3,4]+s[4,5]);
	z[4]=s[3,4]/(s[3,4]+s[3,5]);
    J[3]=(Power[(s[3,5]+s[4,5])/(s[3,4]+s[3,5]+s[4,5]),3]/s[4,5]);
	J[4]=(Power[(s[3,5]+s[4,5])/(s[3,4]+s[3,5]+s[4,5]),3]/s[3,5]);
	m=s12*Sin[\[Theta]]/Power[4*Pi,4];
	Ar=(s[1,3]^2+s[1,4]^2+s[2,3]^2+s[2,4]^2)/(s[3,5]*s[4,5]*s12);
	Ac45=J[4]*(s[1,p3t]^2+s[2,p3t]^2)/(s[4,5]*s12^2)*(1+z[4]^2)/(1-z[4]);
	Ac35=J[3]*(s[1,p4t]^2+s[2,p4t]^2)/(s[3,5]*s12^2)*(1+z[3]^2)/(1-z[3]);
	As5c45=J[4]*2*(s[1,p3t]^2+s[2,p3t]^2)/(s[4,5]*s12^2*(1-z[4]));
	As5c35=J[3]*2*(s[1,p4t]^2+s[2,p4t]^2)/(s[3,5]*s12^2*(1-z[3]));
	(*Aspp=2*(s[1,p3t]^2+s[2,p3t]^2)*(s[3,4]+s[3,5]+s[4,5])/(s12^2*s[3,5]*s[4,5]);*)
	(*Asp=J[4]*2*(s[1,p3t]^2+s[2,p3t]^2)*(s[3,4]+s[3,5])^2/(s12^2*s[3,5]*s[4,5]*s[3,4]);*)
	Aspp=J[3]*2*(s[1,p4t]^2+s[2,p4t]^2)*(s[3,4]+s[4,5])^2/(s12^2*s[3,5]*s[4,5]*s[3,4]);
	(* integrand=Ar-Ac45+As5c45-Ac35+As5c35-f*Asp-(1-f)*Aspp; *)
	integrand=Ar-Ac45+As5c45-Ac35+As5c35-Aspp;
	sol=integrand*m;
	Return[sol];
];


(* sf=2;
pointsRaw=Range[8];
AMCPSs=ParallelMap[
	Module[{\[Epsilon]=1.*10^(-#)},
		NIntegrate[evalIntegrand[1.,a,th,y3,y4],{a,0.,N[Pi]},{th,0.,N[Pi]},{y3,0.+\[Epsilon],1.-\[Epsilon]},{y4,0.+\[Epsilon],1.-y3-\[Epsilon]},Method->"AdaptiveMonteCarlo",PrecisionGoal->sf]
	]&,pointsRaw
];
points=1.*10^(-#)&/@pointsRaw;
data=Transpose[{points,AMCPSs}];
(* Print[data]; *)
(*Export[StringJoin[NotebookDirectory[],"numerical-integration.txt"],data,"Table"];*)
Export[StringJoin["out",ToString[sf],".txt"],data,"Table"]; *)


eps = 1.*10^(-16);
sol = NIntegrate[evalIntegrand[1.,a,th,y3,y4],{a,0.,N[Pi]},{th,0.,N[Pi]},{y3,eps,1.-eps},{y4,eps,1.-y3-eps},Method->"AdaptiveMonteCarlo",PrecisionGoal->2, MaxRecursion->50];
Print[sol];


(* ::Print:: *)
(*-7.72858*10^6*)


(* ::Print:: *)
(*-207.981*)


(* ::Print:: *)
(*-1390.58*)


NIntegrate[evalIntegrand[1.,a,th,y3,y4],{a,0.,N[Pi]},{th,0.,N[Pi]},{y3,eps,1.-eps},{y4,eps,1.-y3-eps},Method->"AdaptiveMonteCarlo",PrecisionGoal->2,MaxRecursion->50]
-1390.5769709033798`
-207.98091524922165`


(*plot=ListLogLogPlot[data];
Export[StringJoin[NotebookDirectory[],"numerical-integration.pdf"],plot];*)


evalIntegrand[1.,0.3,0.2,0.4,0.1]

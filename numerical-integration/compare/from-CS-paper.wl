(* ::Package:: *)

expect=1./2^5/Pi^2;
evalPoint[z_?NumericQ,y_?NumericQ]:=N[1/16/Pi^2*(1-y)];
nInt = NIntegrate[
	evalPoint[z,y],{z,0.,1.},{y,0.,1.},
	Method->{"AdaptiveMonteCarlo","SymbolicProcessing"->False},
	PrecisionGoal->6
	]
expect


D[sij/(sij+sik+sjk),sij]//Simplify

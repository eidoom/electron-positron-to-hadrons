(* ::Package:: *)

zHat={0,0,1};
Rx[a_]:={{1,0,0},{0,Cos[a],-Sin[a]},{0,Sin[a],Cos[a]}};
Ry[a_]:={{Cos[a],0,Sin[a]},{0,1,0},{-Sin[a],0,Cos[a]}};
Rz[a_]:={{Cos[a],-Sin[a],0},{Sin[a],Cos[a],0},{0,0,1}};
RyBeta[x3_?NumericQ,x4_?NumericQ]:=Module[{c\[Beta],s\[Beta]},
	c\[Beta]=1+2*(1-x3-x4)/(x3*x4);
	s\[Beta]=Sqrt[1-Power[c\[Beta],2]];
	Return[{{c\[Beta],0,s\[Beta]},{0,1,0},{-s\[Beta],0,c\[Beta]}}];
	];
(* measure=d[\[Alpha]]*d[\[Theta]]*d[x3]*d[x4]; *)
(* jacobian=1+x *)
evalIntegrand[s12_?NumericQ,\[Alpha]_?NumericQ,\[Theta]_?NumericQ,y3_?NumericQ,y4_?NumericQ]:=Module[
	{Ar,Ac45,As5c45,Ac35,As5c35,Asp,Aspp,integrand,sol,m,z3,z4,J3,J4,x3,x4,p1,p2,p3,p4,p5,p3t,p4t,s,w3,w4},
	x3=1-y3;
	x4=1-y4;
	p1=Sqrt[s12]/2*Prepend[zHat,-1];
	p2=Sqrt[s12]/2*Prepend[-zHat,-1];
	p3=Sqrt[s12]/2*x3*Prepend[Ry[\[Theta]].zHat,1];
	p4=Sqrt[s12]/2*x4*Prepend[Ry[\[Theta]].Rz[\[Alpha]].RyBeta[x3,x4].zHat,1];
	p5=-p1-p2-p3-p4;
	s[pi_,pj_]:=2*(pi[[1]]*pj[[1]]-pi[[2]]*pj[[2]]-pi[[3]]*pj[[3]]-pi[[4]]*pj[[4]]);
	w3=s[p3,p5]/(s[p3,p4]+s[p4,p5]);
	w4=s[p4,p5]/(s[p3,p4]+s[p3,p5]);
	p3t=p3*(1+w4);
	p4t=p4*(1+w3);
	(* P35t=p3+p5-w3*p4; *)
	z3=s[p3,p4]/(s[p3,p4]+s[p4,p5]);
	z4=s[p3,p4]/(s[p3,p4]+s[p3,p5]);
    J3=(s[p3,p4]+s[p3,p5])^2/s[p4,p5]/(s[p3,p4]+s[p3,p5]+s[p4,p5]);
	J4=(s[p3,p4]+s[p3,p5])^2/s[p3,p5]/(s[p3,p4]+s[p3,p5]+s[p4,p5]); 
	m=s12*Sin[\[Theta]]/Power[4*Pi,4];
	Ar=(s[p1,p3]^2+s[p1,p4]^2+s[p2,p3]^2+s[p2,p4]^2)/(s[p3,p5]*s[p4,p5]*s12);
	Ac45=J4*(s[p1,p3t]^2+s[p2,p3t]^2)/(s[p4,p5]*s12^2)*(1+z4^2)/(1-z4);
	Ac35=J3*(s[p1,p4t]^2+s[p2,p4t]^2)/(s[p3,p5]*s12^2)*(1+z3^2)/(1-z3);
	As5c45=J4*2*(s[p1,p3t]^2+s[p2,p3t]^2)/(s[p4,p5]*s12^2*(1-z4));
	As5c35=J3*2*(s[p1,p4t]^2+s[p2,p4t]^2)/(s[p3,p5]*s12^2*(1-z3));
	(*Asp=J4*2*(s[p1,p3t]^2+s[p2,p3t]^2)*(s[p3,p4]+s[p3,p5])^2/(s12^2*s[p3,p5]*s[p4,p5]*s[p3,p4]);*)
	Aspp=2*(s[p1,p4t]^2+s[p2,p4t]^2)*(s[p3,p4]+s[p3,p5]+s[p4,p5])/(s12^2*s[p4,p5]*s[p3,p5]);
	(*Aspp=2*(s[p1,p4t]^2+s[p2,p4t]^2)*(s[p3,p4]+s[p3,p5]+s[p4,p5])^2/(s12^2*s[p4,p5]*s[p3,p5]*s[p3,p4]);*)
	integrand=Ar-Ac45+As5c45-Ac35+As5c35-Aspp;
	sol=integrand*m;
	Return[sol];
];


PS=0.000125983


point=evalIntegrand[1.,0.1,0.2,0.3,0.4]/PS


eps = 1.*10^(-16);
(*sol = NIntegrate[evalIntegrand[1.,a,th,y3,y4],{a,0.,N[Pi]},{th,0.,N[Pi]},{y3,eps,1.-eps},{y4,eps,1.-y3-eps},
	Method->{"AdaptiveMonteCarlo","SymbolicProcessing"->0},PrecisionGoal->2,MaxPoints->10^7,WorkingPrecision->16];*)
sol = NIntegrate[evalIntegrand[1.,a,th,y3,y4],{a,0.,N[Pi]},{th,0.,N[Pi]},{y3,eps,1.-eps},{y4,eps,1.-y3-eps},
	Method->{"AdaptiveMonteCarlo","SymbolicProcessing"->0},PrecisionGoal->1];
Print[sol];


0.0046674685063285077262743782133258736`16.
0.00439057446129494398624152903023842037`16.
0.00305570586004037610779546331210612421`16.
0.00361224709157439564879889747583551136`16.
0.00324221277257354811835108440857510527`16.
0.00336152956330965933480802543214979216`16.
0.0037737390196931251920202647564395928`16.


sol/PS

(* ::Package:: *)

zHat={0,0,1};
Rx[a_]:={{1,0,0},{0,Cos[a],-Sin[a]},{0,Sin[a],Cos[a]}};
Ry[a_]:={{Cos[a],0,Sin[a]},{0,1,0},{-Sin[a],0,Cos[a]}};
Rz[a_]:={{Cos[a],-Sin[a],0},{Sin[a],Cos[a],0},{0,0,1}};
RyBeta[x3_?NumericQ,x4_?NumericQ]:=Module[{c\[Beta],s\[Beta]},
	c\[Beta]=1+2*(1-x3-x4)/(x3*x4);
	s\[Beta]=Sqrt[1-Power[c\[Beta],2]];
	Return[{{c\[Beta],0,s\[Beta]},{0,1,0},{-s\[Beta],0,c\[Beta]}}];
	];


eps = 0.;
evalPhaseSpaceBornThree[s12_?NumericQ,\[Alpha]_?NumericQ,\[Theta]_?NumericQ,y3_?NumericQ,y4_?NumericQ]:=s12*Sin[\[Theta]]/Power[4*Pi,4];
numericPhaseSpaceBornThree = NIntegrate[
	evalPhaseSpaceBornThree[1.,a,th,y3,y4],{a,0.,N[Pi]},{th,0.,N[Pi]},{y3,eps,1.-eps},{y4,eps,1.-y3-eps},
	Method->{"AdaptiveMonteCarlo","SymbolicProcessing"->False},
	PrecisionGoal->3
	];
analyticPhaseSpaceBornThree=N[Power[2,-8]*Power[Pi,-3]];
Print[numericPhaseSpaceBornThree];
Print[analyticPhaseSpaceBornThree];


evalPhaseSpaceFactorised2[s12_?NumericQ,\[Alpha]_?NumericQ,\[Theta]_?NumericQ,s45_?NumericQ,z_?NumericQ]:=Module[
	{m,c,J4},
	m=Sin[\[Theta]]/16./Pi;
	J4=1/2;
	c=1./32./Pi^3;
	Return[J4*c*m];
];
numericPhaseSpaceFactorised2 = NIntegrate[
	evalPhaseSpaceFactorised2[1.,a,th,s45,z],{a,0.,N[2*Pi]},{th,0.,N[Pi]},{s45,0.,1.},{z,0.,1.},
	Method->{"AdaptiveMonteCarlo","SymbolicProcessing"->False},
	PrecisionGoal->3
	];
Print[numericPhaseSpaceFactorised2];
Print[analyticPhaseSpaceBornThree];


evalPhaseSpaceFactorised3[s12_?NumericQ,\[Alpha]_?NumericQ,\[Theta]_?NumericQ,s45_?NumericQ,z_?NumericQ]:=Module[
	{m,c,J4},
	m=Sin[\[Theta]]/16./Pi;
	J4=1.+s45/s12;
	c=1./32./Pi^3;
	Return[J4*c*m];
];
numericPhaseSpaceFactorised3 = NIntegrate[
	evalPhaseSpaceFactorised3[1.,a,th,s45,z],{a,0.,N[2*Pi]},{th,0.,N[Pi]},{s45,0.,1.},{z,0.,1.},
	Method->{"AdaptiveMonteCarlo","SymbolicProcessing"->False},
	PrecisionGoal->3
	];
Print[numericPhaseSpaceFactorised3];
Print[analyticPhaseSpaceBornThree];


evalPhaseSpaceFactorised4[s12_?NumericQ,\[Alpha]_?NumericQ,\[Theta]_?NumericQ,s45_?NumericQ,z_?NumericQ]:=Module[
	{m,c,J4},
	m=Sin[\[Theta]]/16./Pi;
	J4=1+s45*z*(2-z)/(s12-s45);
	c=1./32./Pi^3;
	Return[J4*c*m];
];
numericPhaseSpaceFactorised4 = NIntegrate[
	evalPhaseSpaceFactorised4[1.,a,th,s45,z],{a,0.,N[2*Pi]},{th,0.,N[Pi]},{s45,0.,1.},{z,0.,1.},
	Method->{"AdaptiveMonteCarlo","SymbolicProcessing"->False},
	PrecisionGoal->2
	];
Print[numericPhaseSpaceFactorised4];
Print[analyticPhaseSpaceBornThree];


(*evalPhaseSpaceFactorised3[s12_?NumericQ,\[Alpha]_?NumericQ,\[Theta]_?NumericQ,y3_?NumericQ,y4_?NumericQ]:=Module[
	{m,c,x3,x4,p1,p2,p3,p4,p5,s,J4},
	m=Sin[\[Theta]]/16/Pi;
	x3=1-y3;
	x4=1-y4;
	p1=Sqrt[s12]/2*Prepend[zHat,-1];
	p2=Sqrt[s12]/2*Prepend[-zHat,-1];
	p3=Sqrt[s12]/2*x3*Prepend[Ry[\[Theta]].zHat,1];
	p4=Sqrt[s12]/2*x4*Prepend[Ry[\[Theta]].Rz[\[Alpha]].RyBeta[x3,x4].zHat,1];
	p5=-p1-p2-p3-p4;
	s[pi_,pj_]:=2*(pi[[1]]*pj[[1]]-pi[[2]]*pj[[2]]-pi[[3]]*pj[[3]]-pi[[4]]*pj[[4]]);
	z3=s[p3,p4]/(s[p3,p4]+s[p4,p5]);
	J4=(s[p3,p4]+s[p3,p5])^2/s[p3,p5]/(s[p3,p4]+s[p3,p5]+s[p4,p5]);
	c=1.;
	Return[J4*c*m];
];*)


(s[3,4]+s[3,5])^3/s[3,5]/(s[3,4]+s[3,5]+s45)^3/.s[3,5]->s[3,4]*(1-z)/.s[3,4]->(s12-s45)/(2-z)//Factor


evalPhaseSpaceFactorised5[s12_?NumericQ,\[Alpha]_?NumericQ,\[Theta]_?NumericQ,s45_?NumericQ,z_?NumericQ]:=Module[
	{m,c,J4},
	m=Sin[\[Theta]]/16./Pi;
	J4=((s12 - s45)^2*(-2 + z))/(s12^3*(-1 + z));
	c=1./32./Pi^3;
	Return[J4*c*m];
];
numericPhaseSpaceFactorised5 = NIntegrate[
	evalPhaseSpaceFactorised5[1.,a,th,s45,z],{a,0.,N[2*Pi]},{th,0.,N[Pi]},{s45,0.,1.},{z,0.,1.},
	Method->{"AdaptiveMonteCarlo","SymbolicProcessing"->False},
	PrecisionGoal->2
	];
Print[numericPhaseSpaceFactorised5];
Print[analyticPhaseSpaceBornThree];


analyticPhaseSpaceBornThree*Pi*2

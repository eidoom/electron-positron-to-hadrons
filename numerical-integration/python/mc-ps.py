from math import sin, pi, pow
from random import uniform

from mcint import integrate


def integrand(v):
    theta = v[0]
    alpha = v[1]
    y3 = v[2]
    y4 = v[3]
    return sin(theta) / pow(4 * pi, 4)


def sampler():
    while True:
        theta = uniform(0., pi)
        alpha = uniform(0., pi)
        y4 = uniform(0., 1.)
        y3 = uniform(0., 1. - y4)
        yield (theta, alpha, y3, y4)


domain_size = pow(pi, 2) * 0.5
P3 = pow(2, -8) * pow(pi, -3)

result, error = integrate(integrand, sampler(), measure=domain_size, n=10 ** 6)

print(result, error)

print(result / P3)

from math import pi, sin

from scipy.integrate import nquad, quad

P3 = 2 ** (-8) * pi ** (-3)
print(P3)


def integrand(y4):
    def tmp(theta, alpha, y3):
        return sin(theta) / ((4 * pi) ** 4)

    return nquad(tmp, [[0, pi], [0, pi], [0, 1 - y4]])[0]


integral = quad(integrand, 0, 1)[0]

print(integral)
print(integral/P3)

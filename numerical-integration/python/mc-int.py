from math import sqrt, cos, sin, pi, pow
from random import uniform

from mcint import integrate
from numpy import empty, array, insert, dot


def s(p_i, p_j):
    return 2 * (p_i[0] * p_j[0] - p_i[1] * p_j[1] - p_i[2] * p_j[2] - p_i[3] * p_j[3])


def rotation_matrix_y(angle):
    return array(((cos(angle), 0, sin(angle)), (0, 1, 0), (-sin(angle), 0, cos(angle))))


def rotation_matrix_y_fixed(x3, x4):
    cos_beta = 1 + 2 * (1 - x3 - x4) / (x3 * x4)
    sin_beta = sqrt(1 - cos_beta ** 2)
    return array(((cos_beta, 0, sin_beta), (0, 1, 0), (-sin_beta, 0, cos_beta)))


def rotation_matrix_z(angle):
    return array(((cos(angle), -sin(angle), 0), (sin(angle), cos(angle), 0), (0, 0, 1)))


def eval_point(s12, theta, alpha, y3, y4):
    x3 = 1 - y3
    x4 = 1 - y4

    p = empty([5, 4])
    z = array((0, 0, 1))

    p[0] = 0.5 * sqrt(s12) * insert(z, 0, -1)
    p[1] = 0.5 * sqrt(s12) * insert(-z, 0, -1)
    p[2] = 0.5 * sqrt(s12) * x3 * insert(dot(rotation_matrix_y(theta), z), 0, 1)
    p[3] = 0.5 * sqrt(s12) * x4 * insert(
        dot(rotation_matrix_y(theta), dot(rotation_matrix_z(alpha), dot(rotation_matrix_y_fixed(x3, x4), z))), 0, 1)
    p[4] = - p[0] - p[1] - p[2] - p[3]

    measure = s12 * sin(theta) / pow(4 * pi, 4)

    sq_amp = (s(p[0], p[2]) ** 2 + s(p[0], p[3]) ** 2 + s(p[1], p[2]) ** 2 + s(p[1], p[3]) ** 2) / (
            s(p[2], p[4]) * s(p[3], p[4]) * s12)

    z3 = s(p[2], p[3]) / (s(p[2], p[3]) + s(p[3], p[4]))
    z4 = s(p[2], p[3]) / (s(p[2], p[3]) + s(p[2], p[4]))

    j3 = 1.
    j4 = 1.

    pt = empty([2, 4])
    # pt[0]=p3t, pt[1]=p4t
    pt[0] = (p[3] + s(p[3], p[4]) * p[3] / (s(p[2], p[3]) + s(p[2], p[4])))
    pt[1] = (p[4] + s(p[2], p[4]) * p[4] / (s(p[2], p[3]) + s(p[3], p[4])))

    sq_amp_c45 = j4 * (s(p[0], pt[0]) ** 2 + s(p[1], pt[0]) ** 2) / (s(p[3], p[4]) * s12 ** 2) * (1 + z4 ** 2) / (
            1 - z4)

    sq_amp_c35 = j3 * (s(p[0], pt[1]) ** 2 + s(p[1], pt[1]) ** 2) / (s(p[2], p[4]) * s12 ** 2) * (1 + z3 ** 2) / (
            1 - z3)

    sq_amp_s5c45 = j4 * 2 * (s(p[0], pt[0]) ** 2 + s(p[1], pt[0]) ** 2) / (s(p[3], p[4]) * s12 ** 2 * (1 - z4))

    sq_amp_s5c35 = j3 * 2 * (s(p[0], pt[1]) ** 2 + s(p[1], pt[1]) ** 2) / (s(p[2], p[4]) * s12 ** 2 * (1 - z3))

    sq_amp_sp = j4 * 2 * (s(p[0], pt[0]) ** 2 + s(p[1], pt[0]) ** 2) * s(p[2], p[3]) / (
            s12 ** 2 * s(p[2], p[4]) * s(p[3], p[4]))

    sq_amp_spp = j3 * 2 * (s(p[0], pt[1]) ** 2 + s(p[1], pt[1]) ** 2) * s(p[2], p[3]) / (
            s12 ** 2 * s(p[2], p[4]) * s(p[3], p[4]))

    f = s(p[3], p[4]) / (s(p[3], p[4]) + s(p[2], p[4]))

    expr = sq_amp - sq_amp_c45 + sq_amp_s5c45 - sq_amp_c35 + sq_amp_s5c35 - f * sq_amp_sp - (1 - f) * sq_amp_spp

    return expr * measure


def integrand(v):
    s12 = 1.
    theta = v[0]
    alpha = v[1]
    y3 = v[2]
    y4 = v[3]
    return eval_point(s12, theta, alpha, y3, y4)


eps = pow(10, -3)


def sampler():
    while True:
        theta = uniform(0., pi)
        alpha = uniform(0., pi)
        y4 = uniform(0. + eps, 1. - eps)
        y3 = uniform(0. + eps, 1. - y4 - eps)
        yield (theta, alpha, y3, y4)


domain_size = pow(pi, 2) * 0.5

result, error = integrate(integrand, sampler(), measure=domain_size, n=10 ** 5)

print(result, error)

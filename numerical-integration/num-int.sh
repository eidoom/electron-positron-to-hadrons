#!/bin/sh
# First we set up the SLURM environment.
# All the SBATCH directives below are optional but they do
# allow you to modify the behaviour of your job.
 
#SBATCH --mail-type=END,FAIL                   	# When to send mail (BEGIN, END, FAIL, REQUEUE, ALL) 
#SBATCH --mail-user=ryan.i.moodie@durham.ac.uk  # Where to send mail.  A valid email address
#SBATCH --error="%j.err"                  	# Direct STDERR here (file identifier), %j is substituted for the job number
#SBATCH --output="%j.out"                 	# Direct STDOUT here (file identifier), %j is substituted for the job number 
#SBATCH --exclude=login

# Set working directory
cd /mt/batch/rmoodie/git/electron-positron-to-hadrons/numerical-integration

# Execute program
srun wolframscript -f numerical-integration.wl $1 $2

exit 0

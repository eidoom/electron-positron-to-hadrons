(* ::Package:: *)

SetAttributes[dot,Orderless];
SetAttributes[s,Orderless];
unpackMomenta=p[i_,j__]:>p[i]+p[j];
sToDot=s[i_,j_]:>2*dot[p[i],p[j]];
performMomentaDots=dot[x_List,y_List]:>x[[1]]*y[[1]]-x[[2]]*y[[2]]-x[[3]]*y[[3]]-x[[4]]*y[[4]];
zHat ={0,0,1};
Rx[a_] := {{1,0,0},{0,Cos[a],-Sin[a]},{0,Sin[a],Cos[a]}};
Ry[a_]:={{Cos[a],0,Sin[a]},{0,1,0},{-Sin[a],0,Cos[a]}};
Rz[a_]:={{Cos[a],-Sin[a],0},{Sin[a],Cos[a],0},{0,0,1}};
momentumConservation[{a__},n_]:=-p@@Delete[Range@n,{#}&/@{a}]//.unpackMomenta;
cosBeta=1+2*(1-x[3]-x[4])/(x[3]*x[4]);
(* measure=d[\[Alpha]]*d[\[Theta]]*d[x[3]]*d[x[4]]*d[Cos[\[Beta]]]; *)
measureCoefficient=(s[1, 2]*Sin[\[Theta]])/((4*Pi)^4);
Options[evalDifferential]={\[Alpha]->\[Alpha],\[Theta]->\[Theta],y3->y[3],y4->y[4],s->s[1,2]};
evalDifferential[termsStrings_,OptionsPattern[]]:=Module[
	{
		terms,solutions,values,xToY,sToN,sDeformedToN,replaceFixedMomenta,replaceMomenta,replaceS,
		replaceDeformedMomenta,output,info,p1,p2,p3,p4,p5,s13,s14,s15,s23,s24,s25,s34,s35,s45,c\[Beta],m,
		z3N,z4N,J4N,J3N,p3tilde,p4tilde,P45tilde,P35tilde,s1p3t,s1p4t,s2p3t,s2p4t,s12=N[OptionValue[s]],
		alpha=N[OptionValue[\[Alpha]]],theta=N[OptionValue[\[Theta]]],y3=N[OptionValue[y3]],y4=N[OptionValue[y4]]
	},
	terms=ToExpression/@termsStrings;
	xToY={x[3]->1-y3,x[4]->1-y4};
	c\[Beta]=cosBeta/.xToY;
	p1=Sqrt[s[1,2]]/2 Prepend[zHat,-1]/.s[1,2]->s12;
	p2=Sqrt[s[1,2]]/2 Prepend[-zHat,-1]/.s[1,2]->s12;
	p3=Sqrt[s[1,2]]/2 x[3] Prepend[Ry[\[Theta]].zHat,1]/.s[1,2]->s12/.xToY/.\[Theta]->theta;
	p4=Sqrt[s[1,2]]/2 x[4] Prepend[Ry[\[Theta]].Rz[\[Alpha]].Ry[\[Beta]].zHat,1]/.
		s[1,2]->s12/.xToY/.\[Theta]->theta/.\[Alpha]->alpha/.Sin[\[Beta]]:>Sqrt[Chop[1-Power[c\[Beta],2]]]/.Cos[\[Beta]]->c\[Beta];
	replaceFixedMomenta={
		p[1]->p1,
		p[2]->p2,
		p[3]->p3,
		p[4]->p4
	};
	p5=momentumConservation[{5},5]/.replaceFixedMomenta;
	replaceMomenta=Append[replaceFixedMomenta,p[5]->p5];
	sToN[ss_s]:=ss/.sToDot/.replaceMomenta/.performMomentaDots;
	s13=sToN[s[1,3]];
	s14=sToN[s[1,4]];
	s15=sToN[s[1,5]];
	s23=sToN[s[2,3]];
	s24=sToN[s[2,4]];
	s25=sToN[s[2,5]];
	s34=sToN[s[3,4]];
	s35=sToN[s[3,5]];
	s45=sToN[s[4,5]];
	replaceS={
		s[1,2]->s12,
		s[3,4]->s34,
		s[3,5]->s35,
		s[4,5]->s45,
		s[1,3]->s13,
		s[1,4]->s14,
		s[2,3]->s23,
		s[2,4]->s24
	};
	p3tilde=(p[3]+s[4,5]*p[3]/(s[3,4]+s[3,5]))/.replaceS/.replaceMomenta;
	p4tilde=(p[4]+s[3,5]*p[4]/(s[3,4]+s[4,5]))/.replaceS/.replaceMomenta;
	P45tilde=(p[4,5]-s[4,5]*p[3]/(s[3,4]+s[3,5]))/.replaceS/.unpackMomenta/.replaceMomenta;
	P35tilde=(p[3,5]-s[3,5]*p[4]/(s[3,4]+s[4,5]))/.replaceS/.unpackMomenta/.replaceMomenta;
	replaceDeformedMomenta={
		p[p3t]->p3tilde,
		p[p4t]->p4tilde
	};
	sDeformedToN[ss_s]:=ss/.sToDot/.replaceMomenta/.replaceDeformedMomenta/.performMomentaDots;
	z3N=s[3,4]/(s[3,4]+s[4,5])/.replaceS;
	z4N=s[3,4]/(s[3,4]+s[3,5])/.replaceS;
	(* J4N=1+s[4,5]/(s[3,4]+s[3,5])/.replaceS;
	J3N=1+s[3,5]/(s[3,4]+s[4,5])/.replaceS; *)
	J3N=1.;
	J4N=1.;
	m=measureCoefficient/.\[Theta]->theta/.s[1,2]->s12;
	s1p3t=sDeformedToN[s[1,p3t]];
	s1p4t=sDeformedToN[s[1,p4t]];
	s2p3t=sDeformedToN[s[2,p3t]];
	s2p4t=sDeformedToN[s[2,p4t]];
	values=Transpose[{
		{
			"\[Alpha]       ",
			"\[Theta]       ",
			"y3      ",
			"y4      ",
			"p1      ",
			"p2      ",
			"p3      ",
			"p4      ",
			"p5      ",
			"d\[CapitalPhi]      ",
			"s[1,2]  ",
			"s[1,3]  ",
			"s[1,4]  ",
			"s[1,5]  ",
			"s[2,3]  ",
			"s[2,4]  ",
			"s[2,5]  ",
			"s[3,4]  ",
			"s[3,5]  ",
			"s[4,5]  ",
			"z3      ",
			"z4      ",
			"J3      ",
			"J4      ",
			"p3~     ",
			"p4~     ",
			"P45~    ",
			"P35~    ",
			"s[1,p3~]",
			"s[1,p4~]",
			"s[2,p3~]",
			"s[2,p4~]"
		},
		{
			alpha,
			theta,
			y3,
			y4,
			p1,
			p2,
			p3,
			p4,
			p5,
			m,
			s12,
			s13,
			s14,
			s15,
			s23,
			s24,
			s25,
			s34,
			s35,
			s45,
			z3N,
			z4N,
			J3N,
			J4N,
			p3tilde,
			p4tilde,
			P45tilde,
			P35tilde,
			s1p3t,
			s1p4t,
			s2p3t,
			s2p4t
		}
		}];
	solutions=(#*m)/.replaceS/.
		{
			z3->z3N,
			z4->z4N,
			J4->J4N,
			J3->J3N,
			s[1,p3t]->s1p3t,
			s[1,p4t]->s1p4t,
			s[2,p3t]->s2p3t,
			s[2,p4t]->s2p4t
		}&/@terms;
	output=Transpose[{termsStrings,solutions}];
	info={
		StringJoin[
			StringTrim[termsStrings[[#[[1]]]]],
			"+",
			StringTrim[termsStrings[[#[[2]]]]]
		],
		solutions[[#[[1]]]]+solutions[[#[[2]]]]
		}&/@{{2,3},{4,5},{6,7}};
	printList[list_]:=For[
		i=1,i<=Length[list],i++,
		Module[
			{couplet=list[[i]]},
			Print[couplet[[1]]," ",couplet[[2]]]
		]
	];
	(*printList[values];*)
	(*printList[output];*)
	(*printList[info];*)
	Print["sum     "," ",Plus@@solutions];
	(*Print["ratio   "," ",-solutions[[1]]/Plus@@(solutions[[2;;]])];*)
	Return[solutions];
];
Ar=(s[1,3]^2+s[1,4]^2+s[2,3]^2+s[2,4]^2)/(s[3,5]*s[4,5]*s[1,2]);
Ac45=J4*(s[1,p3t]^2+s[2,p3t]^2)/(s[4,5]*s[1,2]^2)*(1+z4^2)/(1-z4);
Ac35=J3*(s[1,p4t]^2+s[2,p4t]^2)/(s[3,5]*s[1,2]^2)*(1+z3^2)/(1-z3);
As5c45=J4*2*(s[1,p3t]^2+s[2,p3t]^2)/(s[4,5]*s[1,2]^2*(1-z4));
As5c35=J3*2*(s[1,p4t]^2+s[2,p4t]^2)/(s[3,5]*s[1,2]^2*(1-z3));
Asp=J4*2*(s[1,p3t]^2+s[2,p3t]^2)*s[3,4]/(s[1,2]^2*s[3,5]*s[4,5]);
Aspp=J3*2*(s[1,p4t]^2+s[2,p4t]^2)*s[3,4]/(s[1,2]^2*s[3,5]*s[4,5]);
f=s[4,5]/(s[4,5]+s[3,5]);


termsStrings={"Ar      ","-Ac45   ","As5c45  ","-Ac35   ","As5c35  ","-f*Asp  ","-(1-f)*Aspp"};
evalDifferential[termsStrings,s->1.,\[Alpha]->1.,\[Theta]->1.,y3->10^(-1),y4->10^(-1)];
evalDifferential[termsStrings,s->1.,\[Alpha]->1.,\[Theta]->1.,y3->10^(-1),y4->10^(-12)];
evalDifferential[termsStrings,s->1.,\[Alpha]->1.,\[Theta]->1.,y3->10^(-12),y4->10^(-1)];
evalDifferential[termsStrings,s->1.,\[Alpha]->1.,\[Theta]->1.,y3->10^(-12),y4->10^(-12)];


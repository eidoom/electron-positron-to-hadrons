(* ::Package:: *)

(* measure=d[\[Alpha]]*d[\[Theta]]*d[x[3]]*d[x[4]]; *)
integrand[\[Alpha]_?NumericQ,\[Theta]_?NumericQ,y3_?NumericQ,y4_?NumericQ]:=Module[{},Return[Sin[\[Theta]]/Power[4*Pi,4]];];


AMC0PS=NIntegrate[integrand[a,th,y3,y4],{a,0.,Pi},{th,0.,Pi},{y3,0.,1.},{y4,0.,1.-y3},Method->"AdaptiveMonteCarlo"];
(* Print[AMC0PS]; *)


(* AMC1PS=NIntegrate[integrand[a,th,y3,y4],{a,0.,Pi},{th,0.,Pi},{y3,0.,1.},{y4,0.,1.-y3},Method->"AdaptiveMonteCarlo",PrecisionGoal->3]; *)
(* Print[AMC1PS]; *)


(* AMC2PS=NIntegrate[integrand[a,th,y3,y4],{a,0.,Pi},{th,0.,Pi},{y3,0.,1.},{y4,0.,1.-y3},Method->"AdaptiveMonteCarlo",WorkingPrecision->16,PrecisionGoal->4]; *)
(* Print[AMC2PS]; *)


(* APS=NIntegrate[integrand[a,th,y3,y4],{a,0.,Pi},{th,0.,Pi},{y3,0.,1.},{y4,0.,1.-y3}]; *)
(* Print[APS]; *)


P3=N[Power[2,-8]*Power[Pi,-3],16];
(* Print[P3]; *)
Print[AMC0PS/P3];
(* Print[AMC1PS/P3]; *)
(* Print[AMC2PS/P3]; *)
(* Print[APS/P3]; *)




(* ::Package:: *)

file=StringJoin[NotebookDirectory[],"wp20-pg6-methAdaptiveMonteCarlo-mp7.txt"];
data=ReadList[file];
ListLogLogPlot[data,AxesLabel->{"Pole cut out radius",Superscript["R","'"]}]


file=StringJoin[NotebookDirectory[],"wp20-pg4-methAdaptiveMonteCarlo-mp7.txt"];
data=Import[file,"Table"];
ListLogLogPlot[data,AxesLabel->{"Pole cut out radius",Superscript["R","'"]}]

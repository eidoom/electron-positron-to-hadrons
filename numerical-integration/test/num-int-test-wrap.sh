#!/bin/sh
# To run jobs on the batch, do:
# ./num-int-test-wrap.sh wp pg meth mp sc
# where:
# wp = $WorkingPrecision (precision of intermediate steps of calculation)
# pg = $PrecisionGoal (significant figures in answer)
# meth = NIntegrate method (e.g. AdaptiveMonteCarlo)
# mp = Log[10,$MaxPoints] (maximum number of integrand integrations, format: 10^(mp))
# sc = smallest cutoff (evaluate integral cutting out poles, iterate to smallest width 10^(-sc))

for pwr in $(seq 1 $5)
do
	par=wp$1-pg$2-meth$3-mp$4-sc$pwr
	name=$par.sh
	sed "s/%j/${par}/g" num-int-test.sh > $name
	sbatch $name $1 $2 $3 $4 $pwr
	rm -f $name
done

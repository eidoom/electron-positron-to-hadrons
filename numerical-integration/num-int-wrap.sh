#!/bin/sh
# To run jobs on the batch, do:
# ./num-int-wrap.sh f

sf=$1

for f in 1 0
	do
	par=f${f}-sf${sf}
	name=${par}.sh
	sed "s/%j/${par}/g" num-int.sh > $name
	sbatch $name $f $sf
	rm -f $name
	done

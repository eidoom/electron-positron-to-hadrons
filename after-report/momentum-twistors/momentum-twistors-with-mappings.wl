(* ::Package:: *)

(* ::Chapter:: *)
(*Start*)


(* ::Section:: *)
(*Setup*)


(*all outgoing*)
momenta={
	p[1]->-{Sqrt[s]/2,0,Sqrt[s]/2,0},
	p[2]->-{Sqrt[s]/2,0,-(Sqrt[s]/2),0},
	p[3]->{(Sqrt[s] (1+u^2))/(2 (1+u^2+v^2)),-((Sqrt[s] u v (1+u^2+Sqrt[1+u^2+v^2]))/((1+u^2+v^2) (2+u^2+v^2+2 Sqrt[1+u^2+v^2]))),0,-((Sqrt[s] (2+u^4+v^2+2 Sqrt[1+u^2+v^2]+u^2 (3-v^2+2 Sqrt[1+u^2+v^2])))/(2 (1+u^2+v^2) (2+u^2+v^2+2 Sqrt[1+u^2+v^2])))},
	p[4]->{(Sqrt[s] (1+v^2))/(2 (1+u^2+v^2)),-((Sqrt[s] u v (1+v^2+Sqrt[1+u^2+v^2]))/((1+u^2+v^2) (2+u^2+v^2+2 Sqrt[1+u^2+v^2]))),0,(Sqrt[s] (-u^2 (-1+v^2)+(1+v^2) (2+v^2+2 Sqrt[1+u^2+v^2])))/(2 (1+u^2+v^2) (2+u^2+v^2+2 Sqrt[1+u^2+v^2]))},
	p[5]->{(Sqrt[s] (u^2+v^2))/(2 (1+u^2+v^2)),(Sqrt[s] u v)/(1+u^2+v^2),0,(Sqrt[s] (u-v) (u+v))/(2 (1+u^2+v^2))}
	};
sToDot=s[i_Integer,j_Integer]:>2*dot[p[i],p[j]];
dotToN=dot[i_List,j_List]:>(i[[1]]*j[[1]]-i[[2]]*j[[2]]-i[[3]]*j[[3]]-i[[4]]*j[[4]]);
PS3=(2^(-3 + 4*\[Epsilon])*Pi^(-3 + 2*\[Epsilon])*s^(1 - 2*\[Epsilon])*((u^2*v^2)/(1 + u^2 + v^2)^3)^(1 - \[Epsilon])*d[u]*d[v])/
 (3*u*v*Gamma[2 - 2*\[Epsilon]]);
PS3d4=PS3/.\[Epsilon]->0;


{(p[#]/Sqrt[s]//MatrixForm)&/@Range[5]/.momenta}//TableForm


Sum[p[i],{i,1,2}]^2/.momenta//Simplify


Sum[p[i],{i,3,5}]^2/.momenta//Simplify


H=(4 \[Pi])^(2\[Epsilon])*s^(-2\[Epsilon])/Gamma[2-2 \[Epsilon]]
Series[H,{\[Epsilon],0,0}]
preFactor=\[Sigma]0*CF*\[Alpha]s/2/Pi


toMomTwiPar[expr_]:=expr/.sToDot/.momenta/.dotToN;


evaluateMTP[expr_,PS_,H_,lim_]:=Module[
	{diff,incl},
	diff=PS*expr/H/preFactor//FullSimplify;
	Print[diff];
	incl=Series[Integrate[diff/d[u]/d[v],{u,0,lim},{v,0,lim},Assumptions->Re[\[Epsilon]]<0],{\[Epsilon],0,0}];
	Print[incl];
	Return[{diff,incl}];
	];
evaluate[expr_,PS_,H_]:=evaluateMTP[toMomTwiPar[expr],PS,H,Infinity];


SetAttributes[s,Orderless];
w[3]=s[3,5]/(s[3,4]+s[4,5]);
w[4]=s[4,5]/(s[3,4]+s[3,5]);
m3[expr_]:=expr/.s[i_,4]:>s[i,p4]/.s[i_,3]:>s[i,p3]/.
	s[i_,p4]:>s[i,4]*(1+w[3])/.s[i_,p3]:>s[i,3]-w[3]*s[i,4];
m4[expr_]:=expr/.s[i_,3]:>s[i,p3]/.s[i_,4]:>s[i,p4]/.
	s[i_,p3]->s[i,3]*(1+w[4])/.s[i_,p4]:>s[i,4]-w[4]*s[i,3];


(* ::Section::Closed:: *)
(*Dim reg*)


realCoeff=(96*CF*Pi^2*\[Alpha]s*\[Sigma]0)/s
realKinematics=(s[1, 3]^2 + s[1, 4]^2 + s[2, 3]^2 + s[2, 4]^2)/(s[3, 5]*s[4, 5])


{realDiff,realIncl}=evaluate[realKinematics*realCoeff,PS3,H];


(* ::Chapter:: *)
(*Mapping Born only*)


(* ::Section:: *)
(*Soft*)


realCoeff=(96*CF*Pi^2*\[Alpha]s*\[Sigma]0)/s;
softCoeff=realCoeff*2/s
softKin=m3[(s[1,3]^2+s[1,4]^2)]*s/s[3,5]/s[4,5]//Simplify


{softDiff,softIncl}=evaluate[softCoeff*softKin,PS3,H];


(* ::Section::Closed:: *)
(*Soft alt*)


(*softKinAlt1=m4[(s[1,3]^2+s[1,4]^2)]*s[3,4]/s[3,5]/s[4,5]//Simplify(*Same differential as 0*)
softKinAlt2=m3[(s[2,4]^2+s[1,4]^2)]*s[3,4]/s[3,5]/s[4,5]//Simplify
softKinAlt3=m4[(s[2,4]^2+s[1,4]^2)]*s[3,4]/s[3,5]/s[4,5]//Simplify(*Same differential as 5*)
softKinAlt4=m4[(s[2,3]^2+s[1,3]^2)]*s[3,4]/s[3,5]/s[4,5]//Simplify(*Same differential as 2*)
softKinAlt5=m3[(s[2,3]^2+s[1,3]^2)]*s[3,4]/s[3,5]/s[4,5]//Simplify*)


{softDiffAlt1,softInclAlt1}=evaluate[softCoeff*softKinAlt1,PS3,H];


{softDiffAlt2,softInclAlt2}=evaluate[softCoeff*softKinAlt2,PS3,H];


{softDiffAlt3,softInclAlt3}=evaluate[softCoeff*softKinAlt3,PS3,H];


{softDiffAlt4,softInclAlt4}=evaluate[softCoeff*softKinAlt4,PS3,H];


{softDiffAlt5,softInclAlt5}=evaluate[softCoeff*softKinAlt5,PS3,H];


(* ::Section:: *)
(*(1-s_5)c_{45}*)


realCoeff=(96*CF*Pi^2*\[Alpha]s*\[Sigma]0)/s;
collCoeff=realCoeff/s
z4[]:=s[3,4]/(s[3,4]+s[3,5]);
subCol45Kin=-m4[(s[1,3]^2+s[2,3]^2)]/s[4,5]*(1+z4[])//Simplify


{subCol45Diff,subCol45Incl}=evaluate[collCoeff*subCol45Kin,PS3,H];


(* ::Section::Closed:: *)
(*(1-s_5)c_{35}*)


realCoeff=(96*CF*Pi^2*\[Alpha]s*\[Sigma]0)/s;
collCoeff=realCoeff/s;
z3[]:=s[3,4]/(s[3,4]+s[4,5]);
subCol35Kin=-m3[(s[1,4]^2+s[2,4]^2)]/s[3,5]*(1+z3[])//Simplify


(*{subCol35Diff,subCol35Incl}=evaluate[collCoeff*subCol35Kin,PS3,H];*)


(* ::Section::Closed:: *)
(*Aim*)


realIncl


softIncl


subCol45Incl


(*regulated term = dim reg answer - global counterterms*)
aim=realIncl-softIncl-2*subCol45Incl//Simplify
aimN=N[aim]


(* ::Section:: *)
(*Regulated term d=4*)


regCoeff=\[Sigma]0*3*2^5*Pi^2*\[Alpha]s*CF/s
realKin=(s[1, 3]^2 + s[1, 4]^2 + s[2, 3]^2 + s[2, 4]^2)/(s[3, 5]*s[4, 5]);
softMapKin=m3[(s[1,4]^2+s[2,4]^2)]/s*2*s/s[3,5]/s[4,5]//Factor;
z4[]:=s[3,4]/(s[3,4]+s[3,5]);
subCol45MapKin=m4[(s[1,3]^2+s[2,3]^2)]/s/s[4,5]*(-1-z4[])//Factor;
z3[]:=s[3,4]/(s[3,4]+s[4,5]);
subCol35MapKin=m3[(s[1,4]^2+s[2,4]^2)]/s/s[3,5]*(-1-z3[])//Factor;
regMapKin=realKin-softMapKin-subCol45MapKin-subCol35MapKin


softMapKinMTP=toMomTwiPar[softMapKin]//Factor
realKinMTP=toMomTwiPar[realKin]//Factor
subCol45MapKinMTP=toMomTwiPar[subCol45MapKin]//Factor
subCol35MapKinMTP=toMomTwiPar[subCol35MapKin]//Factor
regMapKinMTP=realKinMTP-softMapKinMTP-subCol45MapKinMTP-subCol35MapKinMTP//Factor


PS3d4


{regDiff,regIncl}=evaluateMTP[regCoeff*regMapKinMTP,PS3d4,1,Infinity];


N[regIncl]


(* ::Section::Closed:: *)
(*Regulated term numerical old*)


PS3d4


evalIntegrand[s12_?NumericQ,u_?NumericQ,v_?NumericQ]:=Module[
	{Ar,Ac45,Ac35,As,integrand,sol,c\[Beta],s\[Beta],m,z,J,x,p,s,born,j,eikonal},
	p[1]=-{Sqrt[s12]/2,0,Sqrt[s12]/2,0};
	p[2]=-{Sqrt[s12]/2,0,-(Sqrt[s12]/2),0};
	p[3]={(Sqrt[s12](1+u^2))/(2 (1+u^2+v^2)),-((Sqrt[s12] u v (1+u^2+Sqrt[1+u^2+v^2]))/((1+u^2+v^2) (2+u^2+v^2+2 Sqrt[1+u^2+v^2]))),0,-((Sqrt[s12] (2+u^4+v^2+2 Sqrt[1+u^2+v^2]+u^2 (3-v^2+2 Sqrt[1+u^2+v^2])))/(2 (1+u^2+v^2) (2+u^2+v^2+2 Sqrt[1+u^2+v^2])))};
	p[4]={(Sqrt[s12](1+v^2))/(2 (1+u^2+v^2)),-((Sqrt[s12] u v (1+v^2+Sqrt[1+u^2+v^2]))/((1+u^2+v^2) (2+u^2+v^2+2 Sqrt[1+u^2+v^2]))),0,(Sqrt[s12] (-u^2 (-1+v^2)+(1+v^2) (2+v^2+2 Sqrt[1+u^2+v^2])))/(2 (1+u^2+v^2) (2+u^2+v^2+2 Sqrt[1+u^2+v^2]))};
	p[5]={(Sqrt[s12](u^2+v^2))/(2 (1+u^2+v^2)),(Sqrt[s12] u v)/(1+u^2+v^2),0,(Sqrt[s12] (u-v) (u+v))/(2 (1+u^2+v^2))};
	s[i_,j_]:=2*(p[i][[1]]*p[j][[1]]-p[i][[2]]*p[j][[2]]-p[i][[3]]*p[j][[3]]-p[i][[4]]*p[j][[4]]);
	z[3]=s[3,4]/(s[3,4]+s[4,5]);
	z[4]=s[3,4]/(s[3,4]+s[3,5]);
	j[3]:=(1+s[3,5]/(s[3,4]+s[4,5]))^2;
	j[4]:=(1+s[4,5]/(s[3,4]+s[3,5]))^2;
	m=s12*u*v/(24*Pi^3*(1 + u^2 + v^2)^3);
	Ar=(s[1,3]^2+s[1,4]^2+s[2,3]^2+s[2,4]^2)/(s[3,5]*s[4,5]*s12);
	born[3]=(s[1,4]^2+s[2,4]^2)/(s12^2);
	Ac35=-j[3]*born[3]/s[3,5]*(1+z[3]);
	eikonal=s12/(s[3,5]*s[4,5]);
	born[soft]=(s[1,4]^2+s[2,4]^2)/(s12^2);
	As=j[3]*2*born[soft]*eikonal;
	born[4]=(s[1,3]^2+s[2,3]^2)/(s12^2);
	Ac45=-j[4]*born[4]/s[4,5]*(1+z[4]);
	integrand=Ar-Ac45-Ac35-As;
	sol=integrand*m;
	Return[sol];
];


solMap = NIntegrate[evalIntegrand[1.,u,v],{u,0,1},{v,0,1},Method->"AdaptiveMonteCarlo",PrecisionGoal->3];
Print[solMap];


small=10^(-4);
evalIntegrand[1.,small,small]


(* ::Section:: *)
(*Regulated term numerical MTP*)


regCoeff=\[Sigma]0*3*2^5*Pi^2*\[Alpha]s*CF/s;
includeCoeff=regCoeff/preFactor


PS3d4//InputForm


regMapKinMTP//InputForm


(*All s12's cancel in the integrand*)
evalIntegrandMTP[u_?NumericQ,v_?NumericQ]:=Module[
	{PS,kin,sol},
	PS=u*v/(24*Pi^3*(1 + u^2 + v^2)^3);
	kin=-(u^2 + v^2 + 2*u^2*v^2)/(2*(1 + u^2)*(1 + v^2));
	sol=192*Pi^3*kin*PS;
	Return[sol];
];


lim=Infinity;
solMTP = NIntegrate[evalIntegrandMTP[u,v],{u,0,lim},{v,0,lim},Method->"AdaptiveMonteCarlo",PrecisionGoal->4];
Print[solMTP];


(* ::Chapter:: *)
(*Mapping Born + eikonal only*)


(* ::Section::Closed:: *)
(*Soft*)


realCoeff=(96*CF*Pi^2*\[Alpha]s*\[Sigma]0)/s;
softCoeff=realCoeff*2/s
softKinBE=m3[(s[1,3]^2+s[1,4]^2)*s[3,4]/s[3,5]/s[4,5]]//FullSimplify


{softDiffBE,softInclBE}=evaluate[softCoeff*softKinBE,PS3,H];


aimBE=realIncl-softInclBE-2*subCol45Incl//Simplify


(* ::Chapter:: *)
(*Mapping all*)


(* ::Section::Closed:: *)
(*(1-s_5)c_{45}*)


realCoeff=(96*CF*Pi^2*\[Alpha]s*\[Sigma]0)/s;
collCoeff=realCoeff/s
z4[]:=s[3,4]/(s[3,4]+s[3,5]);
subCol45KinAll=-m4[(s[1,3]^2+s[2,3]^2)/s[4,5]*(1+z4[])]//FullSimplify


{subCol45DiffAll,subCol45InclAll}=evaluate[collCoeff*subCol45KinAll,PS3,H];




(* ::Package:: *)

dotRep=dot[i_List,j_List]:>(i[[1]]*j[[1]]-i[[2]]*j[[2]]-i[[3]]*j[[3]]-i[[4]]*j[[4]]); 


qRepInit={
	q[1]->-{1,0,1,0},
	q[2]->-{1,0,-1,0},
	q[3]->{( (1+u^2)),-(2 u v (1+u^2+Sqrt[1+u^2+v^2])/( 2+u^2+v^2+2 Sqrt[1+u^2+v^2])),0,-((2+u^4+v^2+2 Sqrt[1+u^2+v^2]+u^2 (3-v^2+2 Sqrt[1+u^2+v^2]))/(2+u^2+v^2+2 Sqrt[1+u^2+v^2]))},
	q[4]->{( (1+v^2)),-(2 u v (1+v^2+Sqrt[1+u^2+v^2])/( 2+u^2+v^2+2 Sqrt[1+u^2+v^2])),0,(-u^2 (-1+v^2)+(1+v^2) (2+v^2+2 Sqrt[1+u^2+v^2]))/(2+u^2+v^2+2 Sqrt[1+u^2+v^2])},
	q[5]->{( (u^2+v^2)),2 u v,0,(u^2-v^2)}
	}//FullSimplify;


{(q[#]//MatrixForm)&/@Range[5]/.qRepInit}//TableForm


PauliMatrixBar[i_]:=Join[{IdentityMatrix[2]},-PauliMatrix@Range[3]][[i]]


qwaadInit=qwaad[i_,ad_,a_]:>Plus@@(If[#>1,-1,1]*PauliMatrixBar[#][[ad,a]]*(q[i]/.qRepInit)[[#]]&/@Range[4]);
qwInit=qw[i_]:>Table[(qwaad[i,ad,a]/.qwaadInit),{ad,2},{a,2}];
qw[1]/.qwInit//MatrixForm


l[1]={1,Exp[I*phi[1]]}


l[1]/.Sin[i_]:>(Exp[I*i]-Exp[-I*i])/2/I/.Cos[i_]:>(Exp[I*i]+Exp[-I*i])/2


qwRep=qw[i_]:>If[i<3,-1,1]*Simplify[KroneckerProduct[Conjugate[l[i]],l[i]],Assumptions->Im[v[i]]==0&&Im[u[i]]==0&&Im[phi[i]]==0&&Im[theta[i]]==0];
qRepTmp=q[i_]:>FullSimplify[Tr[Dot[PauliMatrix[#],(qw[i]/.qwRep)]]/2&/@Range[0,3]];
q2Rep=q[2]->Plus@@(-q/@Drop[Range[1,5],{2}]/.qRepTmp)//FullSimplify;
qRep={q2Rep,qRepTmp};


qw[1]/.qwRep//MatrixForm
q[1]/.qRep//MatrixForm


qRep2=q[2]->Module[{a=q[1]/.qRep},Join[{a[[1]]},-a[[2;;]]]];


(q[1]/.qRep)+(q[2]/.qRep2)//FullSimplify


2*dot[q[1]/.qRep,q[2]/.qRep2]/.dotRep//FullSimplify

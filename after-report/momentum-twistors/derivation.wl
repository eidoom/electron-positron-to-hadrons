(* ::Package:: *)

unpack={q[i_,j__]:>q[i]+q[j],p[i_,j__]:>p[i]+p[j]};
dotRep=dot[i_List,j_List]:>(i[[1]]*j[[1]]-i[[2]]*j[[2]]-i[[3]]*j[[3]]-i[[4]]*j[[4]]); 


l[3]={0,1};
l[4]={1,0};
l[5]={u[5],v[5]};(*u,v are real*)
l[1]={u[1]*Exp[I*phi[1]],v[1]};
z=Sqrt[1+u[5]^2+v[5]^2];
x=Sqrt[s]/z;


qRepTmp=q[i_]:>If[i<3,-1,1]*FullSimplify[Tr[Dot[PauliMatrix[#],Simplify[KroneckerProduct[Conjugate[l[i]],l[i]],Assumptions->Im[v[i]]==0&&Im[u[i]]==0&&Im[phi[i]]==0]]]/2&/@Range[0,3]];
q2Rep=q[2]->Plus@@(-q/@Drop[Range[1,5],{2}]/.qRepTmp)//FullSimplify;
qRep={q2Rep,qRepTmp};
pRep=p[i_]:>x*q[i]/.qRep;


{(q[#]/.qRep//MatrixForm)&/@Range[5]}//TableForm


(*Masslessness*)
Table[0,{i,5}]==(dot[q[#],q[#]]/.qRep/.dotRep&/@Range[5]//FullSimplify)


cpRepTmp[expr_]:=expr/.Sin[phi[1]]->Sqrt[1-Cos[phi[1]]^2]/.Cos[phi[1]]->cp
m2alt=dot[q[2],q[2]]/.qRep/.dotRep//FullSimplify
m2=m2alt//cpRepTmp
cpSol=Solve[m2==0,{cp}][[1]]
Solve[m2alt==0,{#}]&/@{u[1],v[1],u[5],v[5]}


m2/.cpSol//FullSimplify


cpRep[expr_]:=cpRepTmp[expr]/.cpSol;
q[2]/.qRep//cpRep//FullSimplify


(*Momentum conservation*)
Table[0,{i,4}]==Plus@@(q/@Range[5])/.qRep//FullSimplify


Plus@@(q/@Range[1,2])/.qRep//FullSimplify;
dot[%,%]/.dotRep//FullSimplify


Plus@@q/@Range[3,5]/.qRep//FullSimplify;
dot[%,%]/.dotRep//FullSimplify


small=10^-3;
big=2;
Manipulate[
	Show[
	Graphics3D[
		{#[[1]],Thick,Line[{{0,0,0},(q[#[[2]]]/.qRep)[[2;;]]}]},
		Axes->True,
		Ticks->False,
		AxesLabel->{"x","y","z"}
		]&/@Transpose[{Join[Table[Blue,{i,1,2}],Table[Red,{i,3,5}]],Range[1,5]}]/.{u[5]->u5,v[5]->v5,u[1]->u1,v[1]->v1,phi[1]->phi1}
		],
	{{u5,1},small,big},
	{{v5,1},small,big},
	{{u1,1},small,big},
	{{v1,1},small,big},
	{{phi1,1},small,2*Pi}
	]


(*s[1,2]*)
2*dot[q[1],q[2]]/.qRep/.dotRep//cpRep//FullSimplify


Manipulate[
	cp/.cpSol/.{u[5]->u5,v[5]->v5,u[1]->u1,v[1]->v1,phi[1]->phi1},
	{{u1,1},small,big},
	{{v1,1},small,big},
	{{u5,1},small,big},
	{{v5,1},small,big}
]

\documentclass{article}
\usepackage[utf8]{inputenc}
\usepackage{amsmath}
\usepackage{breqn}
\usepackage{braket}
\usepackage{bbold}
\usepackage{setspace} 
\doublespacing

\title{Momentum Twistors}
\author{fsarandrea }
\date{February 2019}

\begin{document}

\maketitle

\section{Derivation of Amplitudes in Grassmaninan Coordinates} 
\subsection{Phase-Space Measure} \label{firstSection}
We start with the matrix:
\begin{equation}
  M=  \begin{pmatrix}
0 && 1 && u_{3} e^{i \phi_{3}} \\
1 && 0 && v_{3}
\end{pmatrix}
\end{equation}
where $u_{3}, v_{3}$ are real parameters. \\
This means that we have a scattering process with 3 final states expressed by the spinors
\begin{dmath}
    \begin{split}
    \lambda_{1}= (0,1) \\
    \lambda_{2}= (1,0) \\
    \lambda_{3}= (u_{3}e^{i \phi_{3}},v_{3}) \\
    \end{split}
\end{dmath}
These give the spinorial representation of a set of momenta in the CoM frame of $q-\bar{q}$, with their invariant mass normalised to 1.
Knowing the relations:
\begin{dmath} \label{dualToMink}
\begin{split}
q_{\dot{a}a}=\tilde{\lambda}_{\dot{a}}\lambda_{a}\\
q_{\dot{a}a}=q^{\mu} \sigma_{\dot{a}a}
\end{split}
\end{dmath}
We derive the four-vector form of the momenta:
\begin{dmath} \label{momenta}
\begin{split}
q_{1}=\frac{1}{2}(1,0,0,1)\\
q_{2}=\frac{1}{2}(1,0,0,-1)\\
q_{3}=\frac{1}{2}(u_{3}^2+v_{3}^2,2u_{3}v_{3}\cos{\phi_{3}},2u_{3}v_{3}\sin{\phi_{3}},u_{3}^2-v_{3}^2)
\end{split}
\end{dmath}
In this representation the phase $\phi_{3}$ can be set to zero via an appropriate gauge fixing, therefore we will set $\phi_{3}=0$ for the reminder of this paper. \\ 
The expression for the $d$-dimensional phase-space measure in these coordinates is given in REFERENCE:
\begin{dmath}
d\boldsymbol{\Phi}_{3}=(2\pi)^{d-3(d-1)}m^{2d-6}\frac{d\Omega^{d-2}}{2^{d-1}}\frac{1}{z^{3(d-2)}}\frac{d^{d-1}\boldsymbol{q}_{3}}{2E_{3}}
\end{dmath}
It is then possible to make a change of variables:
\begin{dmath}
d^{d-1}\boldsymbol{q}_{3}=d^{d-2}\boldsymbol{q^{\parallel}}_{3}dq^{z}_{3}=dq^{z}_{3}|\boldsymbol{q^{\parallel}}_{3}|^{d-3} d|\boldsymbol{q^{\parallel}}_{3}|d^{d-3}\Omega
\end{dmath}
We know:
\begin{dmath}
\begin{split}
|\boldsymbol{q^{\parallel}}_{3}|=u_{3}v_{3}\\
q^{z}_{3}=\frac{1}{2}(u_{3}^2-v_{3}^2)
\end{split}
\end{dmath}
and:
\begin{dmath}
J=det \begin{pmatrix}
\frac{\partial q^{z}_{3}}{\partial u_{3}} && \frac{\partial q^{z}_{3}}{\partial v_{3}} \\
\frac{\partial \boldsymbol{q^{\parallel}}_{3}}{\partial u_{3}} && \frac{\partial \boldsymbol{q^{\parallel}}_{3}}{\partial u_{3}}
\end{pmatrix}= u_{3}^2+v_{3}^2=2E_{3}
\end{dmath}
Hence:
\begin{dmath}
\frac{d^{d-1}\boldsymbol{q}_{3}}{2E_{3}}=(u_{3}v_{3})^{d-3} du_{3}dv_{3}d^{d-3}\Omega
\end{dmath}
It is therefore obtained an expression of the phase-space in terms of the spinor variables:
\begin{dmath}
d\boldsymbol{\Phi}_{3}=(2\pi)^{d-3(d-1)}m^{2d-6}\frac{d\Omega^{d-2}}{2^{d-1}}\frac{(u_{3}v_{3})^{d-3}}{z^{3(d-2)}}du_{3}dv_{3}d^{d-3}\Omega
\end{dmath}
For $d=4-2 \epsilon$:
\begin{dmath}
d\boldsymbol{\Phi}_{3}=(2\pi)^{-5+4 \epsilon}m^{2-4 \epsilon}\frac{d\Omega^{2-2 \epsilon}}{2^{3-2 \epsilon}}(\frac{u_{3}^2 v_{3}^2}{z^6})^{-\epsilon}(\frac{u_{3} v_{3}}{z^6})du_{3}dv_{3}d^{1-2 \epsilon}\Omega
\end{dmath}
\subsection{Amplitudes}
\subsection{1 $\rightarrow n$ decays}
The first process that we look at its the first order correction to the toy-model decay of a photon into a quark-antiquark pair: $ \gamma \rightarrow q \bar{q} g $.
\paragraph{}
The amplitude for the process is computed summing the squared Feynman diagrams calculated in the usual way. For the purpose of this example, the photon is taken to have a non-negative mass $ m^{2}=s $ . This will be the value of $m$ appearing in the cross section equation: $m^{2-4\epsilon}=s^{1-2\epsilon}$. The final result is:
\begin{dmath}
\braket{|M(\gamma \rightarrow q \bar{q} g)|^2}= \frac{8(s_{23}^{2}+s_{13}^{2}-s_{12}(s_{12}+s_{13}+s_{23}))}{s s_{13} s_{23}}
\end{dmath}
It is simple to rewrite this matrix element in terms of the variables $u_{3},v_{3}$ since we have the expressions in \ref{momenta}:
\begin{dmath}
\braket{|M(\gamma \rightarrow q \bar{q} g)|^2}=\frac{8 (-1 - u3^2 + u3^4 - v3^2 + v3^4)}{s u3^2 v3^2}
\end{dmath}
\paragraph{}
It is then possible to compute the decay cross-section using the previously found phase space:
\begin{dmath}
\sigma(\gamma \rightarrow q \bar{q} g)=(2\pi)^{-5+4 \epsilon} \int \frac{d\Omega^{2-2 \epsilon}}{2^{3-2 \epsilon}}d^{1-2 \epsilon}\Omega \int_{0}^{\infty}\int_{0}^{\infty}du_{3} dv_{3} \frac{8 (-1 - u3^2 + u3^4 - v3^2 + v3^4)}{s u3^3 v3^3}(\frac{u_{3}^2 v_{3}^2}{z^6})^{1-\epsilon}
\end{dmath}
The solid-angle integrals are computed using the Euler formula:
\begin{dmath}
\int d^{d}\Omega = \frac{2 \pi^{(d+1)/2}}{\Gamma(\frac{d+1}{2})}
\end{dmath}
The final result is consistent with the cross-section expression computed with canonical variables:
\begin{dmath}
\sigma(\gamma \rightarrow q \bar{q} g)= 2^{-5 + 6 \epsilon} \pi^{-5/2} + 2 \epsilon s^{-2 \epsilon}
  \frac{\Gamma(3 - \epsilon) \Gamma(-\epsilon))}{\epsilon \Gamma(
  3 - 3 \epsilon) \Gamma(3/2 - \epsilon)}
\end{dmath}



\subsection{$e^{+}e^{-} \rightarrow q \bar{q} g$}

Expressing the matrix element $ \braket{|M(e^{+}e^{-} \rightarrow q \bar{q} g)|^2} $ in terms of the $u_{3}, v_{3}$ variables is not as immediate as in the case of the decay studied previously, since the momenta of the incoming electron and positron are not determined in our initial set-up. Those expressions are obtained by performing the scaling and inverse Lorentz transformation which move us back to the more familiar CoM frame of the $e^{+}-e^{-}$ particles. 

We express the momenta in the new frame as $p_{i}$:
\begin{dmath}
p_{i}^{\mu}= x \Lambda(\boldsymbol{b})^{\mu}_{\nu}q_{i}^{\nu}
\end{dmath}
Where the boost is $\boldsymbol{b}=\frac{1}{\sum_{i} q^0_{i}} \sum_{j} \boldsymbol{q}_{j}$ and the scaling is $x=\sqrt{\frac{s}{z^2}}$. \\
The Lorentz matrix has the form:
\begin{equation}
\Lambda(\boldsymbol{b})^{\mu}_{\nu}=\begin{pmatrix}
-\gamma & -\gamma \boldsymbol{b} \\
-\gamma \boldsymbol{b} & \mathbb{1} + a \gamma \boldsymbol{b}\boldsymbol{b} \\
\end{pmatrix}
\end{equation}
In this new frame of reference, the four momenta of the positron-electron pair are:
\begin{dmath}
\begin{split}
p_{a}=\frac{\sqrt{s}}{2}(1,0,1,0) \\
p_{b}=\frac{\sqrt{s}}{2}(1,0,-1,0)
\end{split}
\end{dmath}
This choice ensures that the CoM frame energy is $(p_{a}+p_{b})^2=s$. \\
The momenta of the final states in this frame are more complicated than the ones in \ref{momenta}, but the matrix element assumes a simple form:
\begin{dmath}
\braket{|M(e^{+}e^{-} \rightarrow q \bar{q} g)|^2}=8 g_{e}^4 q_{s}^2  N_{c}C_{F}(\frac{s_{1a}^2+s_{1b}^2+s_{2a}^2+s_{2b}^2}{s_{ab}s_{13}s_{23}})
\end{dmath}
which in terms of $u_ {3}$ and $v_{3}$ is:
\begin{dmath}
\braket{|M(e^{+}e^{-} \rightarrow q \bar{q} g)|^2}=8 g_{e}^4 q_{s}^2  N_{c}C_{F} \frac{2 + 2 u3^2 + u3^4 + 2 v3^2 + v3^4}{2 s u3^2 v3^2}
\end{dmath}
We observe that the above expression has poles for $ u_{3}=0,v_{3}=0$. This is consistent with the expectations since those are the values for which the gluon becomes collinear with the quark or anti-quark, or it goes soft:
\begin{dmath}
\begin{split}
    u_{3} \rightarrow 0 \implies g || q \\
    v_{3} \rightarrow 0 \implies \bar{q} || q \\
    u_{3} \rightarrow 0 \And v_{3} \rightarrow 0 \implies E_{3} \rightarrow 0
\end{split}
\end{dmath}

This expression is integrated in the same way as the decay one was. We first show the result of the $u_{3},v_{3}$ integration:
\begin{dmath}
\int_{0}^{\infty}\int_{0}^{\infty}du_{3} dv_{3} \frac{2 + 2 u3^2 + u3^4 + 2 v3^2 + v3^4}{2 s u3^3 v3^3}(\frac{u_{3}^2 v_{3}^2}{z^6})^{1-\epsilon}=\frac{(1 - 2 \epsilon) \Gamma(
  2 - \epsilon) \Gamma(-\epsilon)^2}{2 s \Gamma(3 - 3 \epsilon)}
\end{dmath}
We then compute the remaining part of the expression:
\begin{dmath}
\int s^{1-2\epsilon}(2\pi)^{-5+4 \epsilon}\frac{d\Omega^{2-2 \epsilon}}{2^{3-2 \epsilon}} d\Omega^{2-2 \epsilon} = s^{1-2\epsilon}(2^{-3+2\epsilon})\frac{(4\pi^{5/2-2\epsilon})}{(2\pi)^{5-4\epsilon}2^{-1+2\epsilon}\sqrt{\pi}\Gamma(2-2\epsilon)}
\end{dmath}
As the last step, we combine the expressions and divide by the total flux $ F=2s$ to obtain the total cross section:
\begin{dmath}
\sigma(e^{+}e^{-} \rightarrow q \bar{q} g)=\sigma_{0} \frac{\alpha_{s}}{2 \pi} H(\epsilon)(\frac{2}{\epsilon^2}+ \frac{3}{ \epsilon} +\frac{17}{2}- \pi^2 + O(\epsilon))
\end{dmath}
Where $H(s)$ is an expression of order $ 1+ O(\epsilon)$ and $\sigma_{0}$ is the LO cross section
\begin{dmath}
\sigma_{0}= \frac{4 \pi N_{c} \alpha^2}{3s}
\end{dmath}

Therefore we recover the expected pole structure in the first order correction to the $2 \rightarrow 2 $ scattering.
\subsection{Alternative $e^{+}e^{-} \rightarrow q \bar{q} g$}
In order to compute the matrix element $ \braket{|M(e^{+}e^{-} \rightarrow q \bar{q} g)|^2} $, we need to find convenient expressions for the momenta of the incoming electron and positron, which are not determined by our initial set up. \\
We find that it is more convenient to pick complex momenta for the $e^{+}$ and $e^{-}$ particles, as this allows to work only with rational expressions. In practice this consists in choosing four independent holomorphic and anti-holomorphic spinors $\lambda_{a},\lambda_{b},\tilde{\lambda}_{a},\tilde{\lambda}_{b}$ and imposing the momentum conservation condition:
\begin{dmath} \label{momCons}
p_{a}+p_{b}=p_{1}+p_{2}+p_{3}
\end{dmath}
The centre of mass energy is set to be equal to $s$, so all five the momenta are scaled by the factor $ x=\sqrt{\frac{s}{(1+u_{3}^2+v_{3}^2)}}$, which ensures energy conservation without the need to impose an additional relation between the momenta. \\
Condition \ref{momCons} leaves the freedom to choose 4 out of the 8 components of the spinors. Our choice is:
\begin{dmath}
\begin{split}
    \lambda_{a}=x (1,0) \\
    \lambda_{b}=x (0,1) \\
    \tilde{\lambda}_{a}= \begin{pmatrix}
                           1+ u_{3}^2  \\
                           uv  \\
                          \end{pmatrix}  \\
    \tilde{\lambda}_{b}= \begin{pmatrix}
                           uv \\
                           1 +v_{3}^2 \\
                          \end{pmatrix} 
\end{split}
\end{dmath}
The scaling factor $ x$ in this case is chosen to be put in front of the holomorphic spinors. \\
Equations \ref{dualToMink} are then used to compute the vectors:
\begin{dmath}
\begin{split}
    p_{a}=\frac{1}{2}\sqrt{\frac{s}{(1+u_{3}^2+v_{3}^2)}}((1 + u_{3}^2 ), u_{3} v_{3}, - i u_{3} v_{3} , (1 + u_{3}^2)) \\
    p_{b}=\frac{1}{2}\sqrt{\frac{s}{(1+u_{3}^2+v_{3}^2)}}((1 +v_{3}^2), u_{3} v_{3} ,  i u_{3} v_{3} , (-1 - v_{3}^2))
\end{split}
\end{dmath}
We can easily verify that $p_{a}$ and $p_{b}$ are both massless vector which satisfy momentum and energy conservation. \\
It is then possible to express the squared amplitude in terms of the spinor variables:
\begin{dmath}
\braket{|M(e^{+}e^{-} \rightarrow q \bar{q} g)|^2}=4 g_{e}^4 q_{s}^2  N_{c}C_{F} \frac{2 + 2 u_{3}^2 + u_{3}^4 + 2 v_{3}^2 + v_{3}^4}{s u_{3}^2 v_{3}^2}
\end{dmath}
\paragraph{}
For this choice of variables, the phase space assumes the form:



\section{Construction of the momentum Twistor}
In order to write down the momentum twistor, we need to introduce the new coordinates, $\mu_{i}, \tilde{\mu}_{i}$. In order to do so, we first define the points in the \textit{momentum dual-space}:
\begin{dmath}
y^{\dot{a}a}_{i}= p^{\dot{a}a}_{i+1}-p^{\dot{a}a}_{i}
\end{dmath}
The introduction of the $y$-coordinates is convenient since they naturally encode momentum conservation: \\
INSERT IMAGE HERE
\\
The $\mu$ and $\tilde{\mu}$ variables are defined as functions of $y$ and $\lambda$:
\begin{dmath}
\begin{split}
\mu= \lambda_{\dot{a}} y^{\dot{a}a} \\
\tilde{\mu}= y^{\dot{a}a}\tilde{\lambda}_{a} 
\end{split}
\end{dmath}

The first step consists then in using equations \ref{dualToMink} to find the expressions $p^{\dot{a}a}_{i}$ from their four-vector form. It is then possible to compute the dual-coordinates and the spinors, to wirte down the 2 matrices:
\begin{dmath}
\begin{split}
Z=\begin{pmatrix} 
\lambda \\
\tilde{\mu}
\end{pmatrix} =
\sqrt{\frac{s}{(1+u_{3}^2+v_{3}^2)}}\begin{pmatrix}
1 && 0 && 1 && 0 && v_{3} \\
0 && 1 && 0 && 1 && u_{3} \\
0 && u_{3}(1+u_{3}^2)v_{3} && (1+u_{3}^2) && u_{3}v_{3} && u_{3}(2+u_{3}^2+v_{3}^2) \\
0 && u_{3}^2v_{3}^2 && u_{3}v_{3} && 1+v_{3}^2 && v_{3}(2+u_{3}^2+v_{3}^2) \\
\end{pmatrix} \\
W=\begin{pmatrix} 
\mu \\
\tilde{\lambda}
\end{pmatrix} =\begin{pmatrix}
0 && \frac{s u_{3} v_{3}}{1+u_{3}^2+v_{3}^2} && \frac{s(1+u_{3}^2)}{1+u_{3}^2+v_{3}^2} && \frac{s(u_{3} v_{3})}{1+u_{3}^2+v_{3}^2} && s u_{3}(1+\frac{1}{1+u_{3}^2+v_{3}^2})  \\
0 && 0 && \frac{s(u_{3} v_{3})}{1+u_{3}^2+v_{3}^2} && \frac{s(1+v_{3}^2)}{1+u_{3}^2+v_{3}^2} && s u_{3}(1+\frac{1}{1+u_{3}^2+v_{3}^2}) \\
1+u_{3}^2  && u_{3} v_{3} && 1 && 0 && v_{3}\\
u_{3} v_{3} && 1+v_{3}^2 && 0 && 1 && u_{3}\\
\end{pmatrix} 
\end{split}
\end{dmath}
\paragraph{}
It is seen explicitly that the spinors are rational functions of the variables $u_{3}, v_{3}$. As a result of dropping the reality condition for the incoming momenta, both the holomorphic and anti-holomorphic spinors are real.
This allows to write spinor amplitudes as simple expressions of the variables, which can be easily manipulated and integrated.   


\end{document}
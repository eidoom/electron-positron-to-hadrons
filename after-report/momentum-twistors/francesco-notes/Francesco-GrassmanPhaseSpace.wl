(* ::Package:: *)

length[a_]:=\[Sqrt](a.a)  
Z={{0,1,u3},{1,0,v3}}            
angle[i_]:={Z[[1,i]],Z[[2,i]]}
ZBar={{0,1},{1,0},{u3,v3}}
square[i_]:={ZBar[[i,1]],ZBar[[i,2]]} 
z2=FullSimplify[Det[Z.ZBar]]       
grassMom[i_]:=Table[square[i][[a]]*angle[i][[b]],{a,2},{b,2}]   
fourPauli={
 {1, 0, 0, 1},
 {0, 1, -I, 0},
 {0, 1, I, 0},
 {1, 0, 0, -1} }    
 contraction[a_,b_]:=Dot[Dot[LeviCivitaTensor[2],a],b] 
 minkDot[a_,b_]:={{1,0,0,0},{0,-1,0,0},{0,0,-1,0},{0,0,0,-1}}.a.b   
 sGrass[i_,j_]:=contraction[angle[i],angle[j]]*contraction[square[i],square[j]] 
 q[i_]:=LinearSolve[fourPauli,Flatten[grassMom[i]]]     
 s[a_,b_]:=2*minkDot[a,b]  
yRotation[a_]:={{Cos[a],0,Sin[a]},{0,1,0},{-Sin[a],0,Cos[a]}}
zRotation[a_]:={{Cos[a],-Sin[a],0},{Sin[a],Cos[a],0},{0,0,1}}  
pa=(\[Sqrt]s/2)*{1,0,1,0}
pb=(\[Sqrt]s/2)*{1,0,-1,0}      


(* ::InheritFromParent:: *)
(* *)


grassMom[3] 


phaseSpace=(1/(64*\[Pi]^4))*Integrate[(u3*v3)/(z2^3),{u3,0,\[Infinity]},{v3,0,\[Infinity]},{\[Phi]3,0,2*\[Pi]}]    


check=1/(32*8*(\[Pi])^3)      


gamma[v_]:=1/(\[Sqrt](1-(v.v)))
fact[v_]:=((gamma[v])^2)/(1+gamma[v])
lorentzTrans[v_]:={Flatten[{gamma[v],-gamma[v]*v}],{-gamma[v]*v[[1]],1+fact[v]*v[[1]]*v[[1]],fact[v]*v[[2]]*v[[1]],fact[v]*v[[3]]*v[[1]]},{-gamma[v]*v[[2]],fact[v]*v[[1]]*v[[2]],1+fact[v]*v[[2]]*v[[2]],fact[v]*v[[3]]*v[[2]]},{-gamma[v]*v[[3]],fact[v]*v[[1]]*v[[3]],fact[v]*v[[2]]*v[[3]],1+fact[v]*v[[3]]*v[[3]]}}    
qSum=\[Sqrt](s/z2)* (q[1]+q[2]+q[3])      
Simplify[minkDot[qSum,qSum]]                      


(* ::InheritFromParent:: *)
(**)


(* ::InheritFromParent:: *)
(**)


kSolved=2/(Sqrt[s/(1+u3^2+v3^2)]*(2+u3^2+v3^2))
boost=kSolved*{qSum[[2]],qSum[[3]],qSum[[4]]}
trans=Simplify[lorentzTrans[boost]]  
p[i_]:=\[Sqrt](s/z2)*(trans.q[i])  
Simplify[trans.lorentzTrans[-boost]] 
Simplify[trans.qSum,{u3,v3}\[Element]Reals]         
gammaExpr=s^(1-2\[Epsilon])*(2^(-3+2\[Epsilon]))*(4*\[Pi]^(5/2-2\[Epsilon]))/((2\[Pi])^(5-4\[Epsilon])*2^(-1+2\[Epsilon])*\[Sqrt]\[Pi]*Gamma[2-2\[Epsilon]])         


(* ::InheritFromParent:: *)
(**)


(* ::InheritFromParent:: *)
(**)


(* ::InheritFromParent:: *)
(**)


(* ::InheritFromParent:: *)
(**)


ampl= FullSimplify[((s[p[1],pa])^2+(s[p[1],pb])^2+(s[p[2],pa])^2+(s[p[2],pb])^2-\[Epsilon](s[p[3],pa]^2+s[p[3],pb]^2))/(s[pa,pb]*s[p[1],p[3]]*s[p[2],p[3]]),Assumptions->{0<u3&&v3>0,u3 \[Element]Reals, v3 \[Element] Reals}]      
otherAmpl= FullSimplify[((s[p[1],pa])^2+(s[p[1],pb])^2+(s[p[2],pa])^2+(s[p[2],pb])^2)/(s[pa,pb]*s[p[1],p[3]]*s[p[2],p[3]]),Assumptions->{0<u3&&v3>0,u3 \[Element]Reals,v3 \[Element] Reals}]   


(* ::InheritFromParent:: *)
(**)


(* ::InheritFromParent:: *)
(**)


Integrate[(((2+2 u3^2+u3^4+2 v3^2+v3^4)/(2 s u3^2 v3^2)*u3*v3)/(z2^3))*((u3^2*v3^2/z2^3)^(-\[Epsilon])),{u3,0,\[Infinity]},{v3,0,\[Infinity]},Assumptions->Re[\[Epsilon]]<0]
Series[8*((1-2 \[Epsilon]) Gamma[2-\[Epsilon]] Gamma[-\[Epsilon]]^2)/(2 s Gamma[3-3 \[Epsilon]]),{\[Epsilon],0,0}]


(* ::InheritFromParent:: *)
(**)


(* ::InheritFromParent:: *)
(**)


decayAmp=FullSimplify[8*(s[q[2],q[3]]^2+s[q[1],q[3]]^2-s[q[1],q[2]]*(s[q[1],q[2]]+s[q[1],q[3]]+s[q[2],q[3]]))/(s*s[q[2],q[3]]*s[q[1],q[3]])]   
trialExpr=Integrate[gammaExpr*((decayAmp*u3*v3)/(z2^3))*((u3^2*v3^2/z2^3)^(-\[Epsilon])),{u3,0,\[Infinity]},{v3,0,\[Infinity]},Assumptions->Re[\[Epsilon]]<0]            


Series[trialExpr,{\[Epsilon],0,0}]  



(* ::InheritFromParent:: *)
(**)


pa
pb
FullSimplify[p[1],Assumptions->{u3,v3} \[Element] Reals && u3>0 && v3>0 ]
FullSimplify[p[2],Assumptions->{u3,v3} \[Element] Reals && u3>0 && v3>0 ]
FullSimplify[p[3],Assumptions->{u3,v3} \[Element] Reals && u3>0 && v3>0 ]
(*FullSimplify[s[pb,p[1]], Assumptions->{u3,v3} \[Element] Reals]
FullSimplify[s[pa,p[2]], Assumptions->{u3,v3} \[Element] Reals]
FullSimplify[s[pa,pb]*s[p[1],p[3]]*s[p[2],p[3]],Assumptions->{u3,v3} \[Element] Reals]*)





PS3=gammaExpr*u3*v3/(z2^3)*((u3^2*v3^2/z2^3)^(-\[Epsilon]))

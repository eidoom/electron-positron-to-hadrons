(* ::Package:: *)

(* ::Chapter:: *)
(*Start*)


(* ::Section:: *)
(*Setup common*)


SetAttributes[s,Orderless];
unpackS=s[i_,j_,k__]:>Plus@@DeleteDuplicates[s@@@Permutations[{i,j,k},{2}]];
sToDot=s[i_Integer,j_Integer]:>2*dot[p[i],p[j]];
dotToN=dot[i_List,j_List]:>(i[[1]]*j[[1]]-i[[2]]*j[[2]]-i[[3]]*j[[3]]-i[[4]]*j[[4]]);


w[3]=s[3,5]/(s[3,4]+s[4,5]);
w[4]=s[4,5]/(s[3,4]+s[3,5]);
m3[expr_]:=expr/.s[i_,4]:>s[i,p4]/.s[i_,3]:>s[i,p3]/.
	s[i_,p4]:>s[i,4]*(1+w[3])/.s[i_,p3]:>s[i,3]-w[3]*s[i,4];
m4[expr_]:=expr/.s[i_,3]:>s[i,p3]/.s[i_,4]:>s[i,p4]/.
	s[i_,p3]->s[i,3]*(1+w[4])/.s[i_,p4]:>s[i,4]-w[4]*s[i,3];


preFactor=\[Sigma]0*CF*\[Alpha]s/2/Pi


(* ::Section::Closed:: *)
(*Setup traditional*)


zHat ={0,0,1};
Ry[a_]:={{Cos[a],0,Sin[a]},{0,1,0},{-Sin[a],0,Cos[a]}};
Rz[a_]:={{Cos[a],-Sin[a],0},{Sin[a],Cos[a],0},{0,0,1}};
fixedMomentaReal={
	p[1]->Sqrt[s[1,2]]/2 Prepend[zHat,-1],
	p[2]->Sqrt[s[1,2]]/2 Prepend[-zHat,-1],
	p[3]->Sqrt[s[1,2]]/2 x[1] Prepend[Ry[theta].zHat,1],
	p[4]->Sqrt[s[1,2]]/2 x[2] Prepend[Ry[theta].Rz[alpha].Ry[beta].zHat,1]
};
unpackMomenta=p[i_,j__]:>p[i]+p[j];
momentumConservation[{a__},n_]:=p[a]->-p@@Delete[Range@n,{#}&/@{a}]//.unpackMomenta;
momentaReal=Append[fixedMomentaReal,momentumConservation[{5},5]/.fixedMomentaReal];
cosBeta=Cos[beta]->1+2*(1-x[1]-x[2])/(x[1] x[2]);
sinBeta=Sin[beta]->Sqrt[1-Power[Cos[beta],2]];
PS3trad=(4^(-4 + 3*eps)*Pi^(-7/2 + 2*eps)*d[Cos[alpha]]*d[Cos[theta]]*d[x[1]]*d[x[2]]*s[1, 2]^(1 - 2*eps)*Sin[alpha]^(-1 - 2*eps))/
 (Gamma[1/2 - eps]*Gamma[1 - eps]*Sin[theta]^(2*eps)*((-1 + x[1])*(-1 + x[2])*(-1 + x[1] + x[2]))^eps);
PS3tradD4=PS3trad/.eps->0;


evaluateDiffTrad[expr_,PS_]:=Module[
	{start1,start2,tmp1,tmp2,differential},
	start1=(expr/.sToDot/.momentaReal/.dotToN/.sinBeta/.cosBeta)//Factor;
	start2=Factor[PS*start1]/.Csc[x_]->1/Sqrt[1-Power[Cos[x],2]]/.Cot[x_]->Cos[x]/Sqrt[1-Power[Cos[x],2]]/.
		Sin[x_]:>Sqrt[1-Power[Cos[x],2]]/.Cos[theta]->cosTheta/.Cos[alpha]->cosAlpha;
	tmp1=Integrate[start2/d[cosAlpha],{cosAlpha,-1,1},Assumptions->Re[eps]<1/2]//Factor;
	tmp2=Integrate[tmp1/d[cosTheta],{cosTheta,-1,1},Assumptions->Re[eps]<1/2]//Factor;
	differential=tmp2/.d[x[i_]]:>d[y[i]]/.x[i_]:>1-y[i]//FullSimplify;
	Return[differential];
	];
evaluateInclTrad[differential_]:=Module[
	{tmp,inclusive,expansion},
	tmp=Integrate[differential/d[y[2]],{y[2],0,1-y[1]},Assumptions->Re[eps]<0&&y[1]<1]//Factor;
	inclusive=Integrate[tmp/d[y[1]],{y[1],0,1},Assumptions->Re[eps]<0&&s[1,2]>0]//Factor;
	expansion=Series[inclusive,{eps,0,0}]//FullSimplify;
	Return[expansion];
	];
(*evaluateInclAlt[differential_]:=Module[
	{tmp,inclusive,expansion},
	tmp=Integrate[differential/d[y[1]],{y[1],0,1-y[2]},Assumptions->Re[eps]<0]//Factor;
	inclusive=Integrate[tmp/d[y[2]],{y[2],0,1},Assumptions->Re[eps]<0]//Factor;
	expansion=Series[inclusive,{eps,0,0}]//FullSimplify;
	Return[expansion];
	];*)


(* ::Section:: *)
(*Setup IPMV*)


(*all outgoing*)
pwr=1/2;
momenta={
	p[1]->-Power[s[1,2]/(1 + u^2 + v^2),pwr]*{(1 + u^2)/2, u*v/2, -I/2*u*v, (1 + u^2)/2},
	p[2]->-Power[s[1,2]/(1 + u^2 + v^2),pwr]*{(1 + v^2)/2, u*v/2, I/2*u*v, -(1 + v^2)/2},
	p[3]->Power[s[1,2]/(1 + u^2 + v^2),pwr]*{1/2, 0, 0, -1/2},
	p[4]->Power[s[1,2]/(1 + u^2 + v^2),pwr]*{1/2, 0, 0, 1/2},
	p[5]->Power[s[1,2]/(1 + u^2 + v^2),pwr]*{(u^2 + v^2)/2, u*v, 0, (u^2 - v^2)/2}
	};
PS3IPMVe=d[u]*d[v]*(2^(-8 + 6*eps)*s[1,2]^(1 - 2*eps)*Pi^(-5 + 4*eps)*d[Omega, 1 - 2*eps]*d[Omega, 2 - 2*eps]*u*v)/
	((1 + u^2 + v^2)^3*((u^2*v^2)/(1 + u^2 + v^2)^3)^eps);
solidAngle=d[Omega,n_]:>2*Power[Pi,(n+1)/2]/Gamma[(n+1)/2];
PS3IPMV=PS3IPMVe/.solidAngle//FullSimplify;
PS3IPMVd4=PS3IPMV/.eps->0;


PS3IPMVe/.eps->0


Print["Momenta"];
{(p[#]//MatrixForm)&/@Range[5]/.momenta}//TableForm
Print["Mometum conservation"];
p[5]==Plus@@(-p[#]&/@Range[4])/.momenta//FullSimplify
Print["Masslessness"];
Table[0,{i,5}]==(dot[p[#],p[#]]&/@Range[5]/.momenta/.dotToN//FullSimplify)
Print["Phase space d=4-2eps"];
PS3IPMV
Print["Phase space d=4"];
PS3IPMVd4


assume=Flatten[{Im[#]==0,#>=0}&/@{u,v,s[1,2]}];
simAss[expr_]:=Simplify[expr,Assumptions->assume];


flux=1/(4*Abs[(p[2]/.momenta)[[1]](p[1]/.momenta)[[4]]-(p[1]/.momenta)[[1]](p[2]/.momenta)[[4]]]//simAss)


flux==1/(4*Abs[Sum[LeviCivitaTensor[4][[mu,2,3,nu]]*(p[1]/.momenta)[[mu]]*(p[2]/.momenta)[[nu]],{mu,4},{nu,4}]]//simAss)


toMomTwiPar[expr_]:=expr/.sToDot/.momenta/.dotToN//FullSimplify;
evaluateDiffIPMV[expr_,PS_]:=PS*toMomTwiPar[expr]//FullSimplify;
evaluateInclIPMV[diff_,lim_]:=Module[{incl},
	incl=Series[Integrate[diff/d[u]/d[v],{u,0,lim},{v,0,lim},Assumptions->Re[eps]<0],{eps,0,0}]//FullSimplify;
	Return[incl];
];


(* ::Section::Closed:: *)
(*Real momenta*)


momenta={
	p[1]->-{Sqrt[s[1,2]]/2,0,Sqrt[s[1,2]]/2,0},
	p[2]->-{Sqrt[s[1,2]]/2,0,-(Sqrt[s[1,2]]/2),0},
	p[3]->{(Sqrt[s[1,2]] (1+u^2))/(2 (1+u^2+v^2)),-((Sqrt[s[1,2]] u v (1+u^2+Sqrt[1+u^2+v^2]))/((1+u^2+v^2) (2+u^2+v^2+2 Sqrt[1+u^2+v^2]))),0,-((Sqrt[s[1,2]] (2+u^4+v^2+2 Sqrt[1+u^2+v^2]+u^2 (3-v^2+2 Sqrt[1+u^2+v^2])))/(2 (1+u^2+v^2) (2+u^2+v^2+2 Sqrt[1+u^2+v^2])))},
	p[4]->{(Sqrt[s[1,2]] (1+v^2))/(2 (1+u^2+v^2)),-((Sqrt[s[1,2]] u v (1+v^2+Sqrt[1+u^2+v^2]))/((1+u^2+v^2) (2+u^2+v^2+2 Sqrt[1+u^2+v^2]))),0,(Sqrt[s[1,2]] (-u^2 (-1+v^2)+(1+v^2) (2+v^2+2 Sqrt[1+u^2+v^2])))/(2 (1+u^2+v^2) (2+u^2+v^2+2 Sqrt[1+u^2+v^2]))},
	p[5]->{(Sqrt[s[1,2]] (u^2+v^2))/(2 (1+u^2+v^2)),(Sqrt[s[1,2]] u v)/(1+u^2+v^2),0,(Sqrt[s[1,2]] (u-v) (u+v))/(2 (1+u^2+v^2))}
	};


(* ::Section::Closed:: *)
(*Alt init mom*)


pwr=1/2;
momenta={
	p[1]->-Power[s[1,2]/(1 + u^2 + v^2),pwr]*{1/2 (1+u^2-u v),1/2 (-1+u v-v^2),-(1/2) I (-1+u v-v^2),1/2 (1+u^2-u v)},
	p[2]->-Power[s[1,2]/(1 + u^2 + v^2),pwr]*{1/2 (1+u v+v^2),1/2 (1+u v+v^2),1/2 I (-1+u v-v^2),1/2 (-1+u v-v^2)},
	p[3]->Power[s[1,2]/(1 + u^2 + v^2),pwr]*{1/2, 0, 0, -1/2},
	p[4]->Power[s[1,2]/(1 + u^2 + v^2),pwr]*{1/2, 0, 0, 1/2},
	p[5]->Power[s[1,2]/(1 + u^2 + v^2),pwr]*{(u^2 + v^2)/2, u*v, 0, (u^2 - v^2)/2}
	};


(* ::Section::Closed:: *)
(*Alt mom*)


new={p[a]->Sqrt[s[1, 2]/(1 + u^2 + v^2)]{-1, -(u*v)/2, (I/2)*u*v+1, 0},
	 p[b]->Sqrt[s[1, 2]/(1 + u^2 + v^2)]{-((u^2 + v^2))/2, -(u*v)/2, (-I/2)*u*v, -((u^2 - v^2))/2}}


p[a]+p[b]/.new//Simplify
Plus@@(-p[#]&/@Range[3,5])/.momenta//FullSimplify


(dot[p[#],p[#]]&/@{a,b}/.new/.dotToN//FullSimplify)/(s[1, 2]/(1 + u^2 + v^2))


(* ::Section::Closed:: *)
(*Bispinors*)


p[1]/.momenta
sigmaBar=Prepend[(-PauliMatrix/@Range[3]),IdentityMatrix[2]]
pWeyl=pw[i_]:>sigmaBar[[1]]*(p[i]/.momenta)[[1]]-Plus@@(sigmaBar[[#]]*(p[i]/.momenta)[[#]]&/@Range[2,4])


pws=(pw/@Range[5]/.pWeyl//FullSimplify)


Do[Print[pws[[i]]//MatrixForm],{i,5}]


(* ::Chapter:: *)
(*Tests*)


(* ::Section:: *)
(*Phase space volume*)


vol3=s[1,2]/2^8/Pi^3


integrandPS3IPMVd4=evaluateDiffIPMV[1,PS3IPMVd4]
solPS3IPMVd4=evaluateInclIPMV[integrandPS3IPMVd4,Infinity]


integrandPS3IPMV=evaluateDiffIPMV[1,PS3IPMV]
solPS3IPMV=evaluateInclIPMV[integrandPS3IPMV,Infinity]


(* ::Section::Closed:: *)
(*Born*)


(* ::Subsection:: *)
(*Traditional*)


flux=2*s[1,2]


PS2=d[Cos[theta]]/(16*Pi)


bornFixedMomenta={
	p[1]->Sqrt[s[1,2]]/2 Prepend[zHat,-1],
	p[2]->Sqrt[s[1,2]]/2 Prepend[-zHat,-1],
	p[3]->Sqrt[s[1,2]]/2 Prepend[Ry[theta].zHat,1]
};
bornMomenta=Append[bornFixedMomenta,momentumConservation[{4},4]/.bornFixedMomenta];


bornCoeff=32*alpha^2*Nc*Pi^2*Qq^2;
bornKin=(s[1, 3]^2 + s[1, 4]^2)/s[1, 2]^2;
bornInput=bornCoeff*bornKin


bornDiff=PS2*bornInput/flux/.sToDot/.bornMomenta/.dotToN//Factor


bornIncl=Integrate[(bornDiff/.Cos[theta]->cosTheta)/d[cosTheta],{cosTheta,-1,1}]


(* ::Subsection:: *)
(*IPMV*)


(* ::Section::Closed:: *)
(*Invariants*)


toMomTwiPar[s[1,2]]


dot[Plus@@(p[#]&/@Range[3,5]),Plus@@(p[#]&/@Range[3,5])]/.momenta/.dotToN//FullSimplify


s34=toMomTwiPar[s[3,4]]
s35=toMomTwiPar[s[3,5]]
s45=toMomTwiPar[s[4,5]]


s13=toMomTwiPar[s[1,3]]
s14=toMomTwiPar[s[1,4]]
s23=toMomTwiPar[s[2,3]]
s24=toMomTwiPar[s[2,4]]
s15=toMomTwiPar[s[1,5]]
s25=toMomTwiPar[s[2,5]]


large=10^6;
{s34,s35,s45}/.u->0/.v->0/.s[1,2]->1//N//Chop


s24/.u->0/.v->0
s24/.u->0/.v->1
s24/.u->1/.v->0
s24/.u->1/.v->1
s24/.u->0/.v->large
s24/.u->large/.v->0//N//Chop
s24/.u->large/.v->large//N
s24/.u->large/.v->1//N//Chop
s24/.u->1/.v->large//N


s13+s15+s35//FullSimplify
s24


Plot3D[1-ss45-ss35,{ss35,0,1},{ss45,0,1},PlotRange->{{0,1},{0,1},{0,1}},AxesLabel->{"s35","s45","s34"}]


(* ::Section::Closed:: *)
(*Test 0 (d=4)*)


power=3;
testZeroKin=(s[3,5]^power)/s[1,2]^(power+1)/vol3*s[1,2]
toMomTwiPar[testZeroKin]


testZeroDiffTrad=evaluateDiffTrad[testZeroKin,PS3tradD4]
testZeroInclTrad=evaluateInclTrad[testZeroDiffTrad]


testZeroDiffIPMV=evaluateDiffIPMV[testZeroKin,PS3IPMVd4]
testZeroInclIMPV=evaluateInclIPMV[testZeroDiffIPMV,Infinity]


(* ::Section:: *)
(*Dim reg*)


realCoeff=(96*CF*Pi^2*\[Alpha]s*\[Sigma]0)/s[1,2]
realKinematics=(s[1, 3]^2 + s[1, 4]^2 + s[2, 3]^2 + s[2, 4]^2)/(s[3, 5]*s[4, 5])
realInput=realCoeff*realKinematics/preFactor;


realDiffTrad=evaluateDiffTrad[realInput,PS3trad]


realInclTrad=evaluateInclTrad[realDiffTrad]


realDiffIPMV=evaluateDiffIPMV[realInput,PS3IPMV]


realInclIPMV=evaluateInclIPMV[realDiff,Infinity]


(* ::Section::Closed:: *)
(*Test 1 (d=4-2\[Epsilon])*)


x1= (1+u^2)/(1+u^2+v^2);
x2= (1+v^2)/(1+u^2+v^2);
testOneKin=(x1^2+x2^2)/((1-x1)*(1-x2))//Simplify
toMomTwiPar[testOneKin]


testOneDiffTrad=evaluateDiffTrad[testOneKin,PS3trad]
testOneInclTrad=evaluateInclTrad[testOneDiffTrad]


testOneDiffIPMV=evaluateDiffIPMV[testOneKin,PS3IPMV]
testOneInclIMPV=evaluateInclIPMV[testOneDiffIPMV,Infinity]


testOneInclTrad-testOneInclIMPV//FullSimplify

(* ::Package:: *)

(* ::Section:: *)
(*Setup*)


(*1,2 incoming; 3,4,5 outgoing*)
momenta={
	p[1]->{Sqrt[s]/2,0,Sqrt[s]/2,0},
	p[2]->{Sqrt[s]/2,0,-(Sqrt[s]/2),0},
	p[3]->{(Sqrt[s] (1+u^2))/(2 (1+u^2+v^2)),-((Sqrt[s] u v (1+u^2+Sqrt[1+u^2+v^2]))/((1+u^2+v^2) (2+u^2+v^2+2 Sqrt[1+u^2+v^2]))),0,-((Sqrt[s] (2+u^4+v^2+2 Sqrt[1+u^2+v^2]+u^2 (3-v^2+2 Sqrt[1+u^2+v^2])))/(2 (1+u^2+v^2) (2+u^2+v^2+2 Sqrt[1+u^2+v^2])))},
	p[4]->{(Sqrt[s] (1+v^2))/(2 (1+u^2+v^2)),-((Sqrt[s] u v (1+v^2+Sqrt[1+u^2+v^2]))/((1+u^2+v^2) (2+u^2+v^2+2 Sqrt[1+u^2+v^2]))),0,(Sqrt[s] (-u^2 (-1+v^2)+(1+v^2) (2+v^2+2 Sqrt[1+u^2+v^2])))/(2 (1+u^2+v^2) (2+u^2+v^2+2 Sqrt[1+u^2+v^2]))},
	p[5]->{(Sqrt[s] (u^2+v^2))/(2 (1+u^2+v^2)),(Sqrt[s] u v)/(1+u^2+v^2),0,(Sqrt[s] (u-v) (u+v))/(2 (1+u^2+v^2))}
	};
sToDot=s[i_Integer,j_Integer]:>2*dot[p[i],p[j]];
dotToN=dot[i_List,j_List]:>(i[[1]]*j[[1]]-i[[2]]*j[[2]]-i[[3]]*j[[3]]-i[[4]]*j[[4]]);
PS3=4/3*(2^(-5 + 4*\[Epsilon])*Pi^(-3 + 2*\[Epsilon])*s^(1 - 2*\[Epsilon])*u*v)/(((u^2*v^2)/(1 + u^2 + v^2)^3)^\[Epsilon]*
  (1 + u^2 + v^2)^3*Gamma[2 - 2*\[Epsilon]])*d[u]*d[v];
PS3d4=PS3/.\[Epsilon]->0;


PS3
PS3d4


H=(4 \[Pi])^(2\[Epsilon])*s^(-2\[Epsilon])/Gamma[2-2 \[Epsilon]]
Series[H,{\[Epsilon],0,0}]
preFactor=\[Sigma]0*CF*\[Alpha]s/2/Pi


evaluate[expr_,PS_,H_]:=Module[
	{one,two},
	one=PS*(expr/.sToDot/.momenta/.dotToN)/H/preFactor//FullSimplify;
	Print[one];
	two=Integrate[one/d[u]/d[v],{u,0,x},{v,0,x},Assumptions->Re[\[Epsilon]]<0&&Im[x]==0&&x>=0];
	Return[Series[two,{\[Epsilon],0,0}]];
	];


(* ::Section::Closed:: *)
(*Dim reg*)


realCoeff=(96*CF*Pi^2*\[Alpha]s*\[Sigma]0)/s
realKinematics=(s[1, 3]^2 + s[1, 4]^2 + s[2, 3]^2 + s[2, 4]^2)/(s[3, 5]*s[4, 5])


realResult=evaluate[realKinematics*realCoeff,PS3,H]


(* ::Section::Closed:: *)
(*Soft*)


realCoeff=(96*CF*Pi^2*\[Alpha]s*\[Sigma]0)/s;
softCoeff=realCoeff*2/s
softKinematics=(s[1,3]^2+s[1,4]^2)*s[3,4]/s[3,5]/s[4,5]


softResult=evaluate[softCoeff*softKinematics,PS3,H]


(* ::Section::Closed:: *)
(*(1-s_5)c_{45}*)


realCoeff=(96*CF*Pi^2*\[Alpha]s*\[Sigma]0)/s;
collCoeff=realCoeff/s
z4[]:=s[3,4]/(s[3,4]+s[3,5]);
subCol45Kinematics=(s[1,3]^2+s[2,3]^2)/s[4,5]*(-1-z4[])


subCol45Result=evaluate[collCoeff*subCol45Kinematics,PS3,H]


subCol45Result=subCol45Result//FullSimplify


(* ::Section::Closed:: *)
(*(1-s_5)c_{35}*)


realCoeff=(96*CF*Pi^2*\[Alpha]s*\[Sigma]0)/s;
collCoeff=realCoeff/s;
z3[]:=s[3,4]/(s[3,4]+s[4,5]);
subCol35Kinematics=(s[1,4]^2+s[2,4]^2)/s[3,5]*(-1-z3[])


subCol35Result=evaluate[collCoeff*subCol35Kinematics,PS3,H]


(* ::Section:: *)
(*Aim*)


realResult


softResult


subCol45Result


subCol35Result


(*regulated term = dim reg answer - global counterterms*)
aim=realResult-softResult-subCol45Result-subCol35Result//Simplify
aimN=N[aim]


(* ::Section::Closed:: *)
(*Regulated term analytical d=4*)


regCoeff=\[Sigma]0*3*2^5*Pi^2*\[Alpha]s*CF/s
regKinematics=realKinematics-softKinematics*2/s-subCol35Kinematics/s-subCol45Kinematics/s


regResult=evaluate[regCoeff*regKinematics,PS3d4,1]


(* measure=d[\[Alpha]]*d[\[Theta]]*d[x[3]]*d[x[4]]; *)
evalIntegrand[s12_?NumericQ,\[Alpha]_?NumericQ,\[Theta]_?NumericQ,y3_?NumericQ,y4_?NumericQ]:=Module[
	{Ar,Ac45,Ac35,As,integrand,sol,c\[Beta],s\[Beta],m,z,J,x,p,s,born,j,eikonal},
	p[1]={Sqrt[s]/2,0,Sqrt[s]/2,0};
	p[2]={Sqrt[s]/2,0,-(Sqrt[s]/2),0};
	p[3]={(Sqrt[s] (1+u^2))/(2 (1+u^2+v^2)),-((Sqrt[s] u v (1+u^2+Sqrt[1+u^2+v^2]))/((1+u^2+v^2) (2+u^2+v^2+2 Sqrt[1+u^2+v^2]))),0,-((Sqrt[s] (2+u^4+v^2+2 Sqrt[1+u^2+v^2]+u^2 (3-v^2+2 Sqrt[1+u^2+v^2])))/(2 (1+u^2+v^2) (2+u^2+v^2+2 Sqrt[1+u^2+v^2])))};
	p[4]={(Sqrt[s] (1+v^2))/(2 (1+u^2+v^2)),-((Sqrt[s] u v (1+v^2+Sqrt[1+u^2+v^2]))/((1+u^2+v^2) (2+u^2+v^2+2 Sqrt[1+u^2+v^2]))),0,(Sqrt[s] (-u^2 (-1+v^2)+(1+v^2) (2+v^2+2 Sqrt[1+u^2+v^2])))/(2 (1+u^2+v^2) (2+u^2+v^2+2 Sqrt[1+u^2+v^2]))};
	p[5]={(Sqrt[s] (u^2+v^2))/(2 (1+u^2+v^2)),(Sqrt[s] u v)/(1+u^2+v^2),0,(Sqrt[s] (u-v) (u+v))/(2 (1+u^2+v^2))};
	s[i_,j_]:=2*(p[i][[1]]*p[j][[1]]-p[i][[2]]*p[j][[2]]-p[i][[3]]*p[j][[3]]-p[i][[4]]*p[j][[4]]);
	z[3]=s[3,4]/(s[3,4]+s[4,5]);
	z[4]=s[3,4]/(s[3,4]+s[3,5]);
	m=s12*Sin[\[Theta]]/Power[4*Pi,4];
	Ar=(s[1,3]^2+s[1,4]^2+s[2,3]^2+s[2,4]^2)/(s[3,5]*s[4,5]*s12);
	born[3]=(s[1,4]^2+s[2,4]^2)/(s12^2);
	Ac35=-born[3]/s[3,5]*(1+z[3]);
	eikonal=s[3,4]/(s[3,5]*s[4,5]);
	born[soft]=(s[1,3]^2+s[1,4]^2)/(s12^2);
	As=2*born[soft]*eikonal;
	born[4]=(s[1,3]^2+s[2,3]^2)/(s12^2);
	Ac45=-born[4]/s[4,5]*(1+z[4]);
	integrand=Ar-Ac45-Ac35-As;
	sol=integrand*m;
	Return[sol];
];


(* ::Section:: *)
(*Regulated term d=4-2\[Epsilon]*)


regCoeff=\[Sigma]0*3*2^5*Pi^2*\[Alpha]s*CF/s
realKinematics=(s[1, 3]^2 + s[1, 4]^2 + s[2, 3]^2 + s[2, 4]^2)/(s[3, 5]*s[4, 5]);
softKinematics=(s[1,3]^2+s[1,4]^2)*s/s[3,5]/s[4,5];
z4[]:=s[3,4]/(s[3,4]+s[3,5]);
subCol45Kinematics=(s[1,3]^2+s[2,3]^2)/s[4,5]*(-1-z4[]);
z3[]:=s[3,4]/(s[3,4]+s[4,5]);
subCol35Kinematics=(s[1,4]^2+s[2,4]^2)/s[3,5]*(-1-z3[]);
regKinematics=realKinematics-softKinematics*2/s-subCol35Kinematics/s-subCol45Kinematics/s


unRegResult=evaluate[regCoeff*regKinematics,PS3,H]


unRegResult-aim


evaluate[regCoeff*(realKinematics),PS3,H]


(* ::Section::Closed:: *)
(*Regulated term numerical*)


evalIntegrand[s12_?NumericQ,u_?NumericQ,v_?NumericQ]:=Module[
	{Ar,Ac45,Ac35,As,integrand,sol,c\[Beta],s\[Beta],m,z,J,x,p,s,born,j,eikonal},
	p[1]={Sqrt[s12]/2,0,Sqrt[s12]/2,0};
	p[2]={Sqrt[s12]/2,0,-(Sqrt[s12]/2),0};
	p[3]={(Sqrt[s12](1+u^2))/(2 (1+u^2+v^2)),-((Sqrt[s12] u v (1+u^2+Sqrt[1+u^2+v^2]))/((1+u^2+v^2) (2+u^2+v^2+2 Sqrt[1+u^2+v^2]))),0,-((Sqrt[s12] (2+u^4+v^2+2 Sqrt[1+u^2+v^2]+u^2 (3-v^2+2 Sqrt[1+u^2+v^2])))/(2 (1+u^2+v^2) (2+u^2+v^2+2 Sqrt[1+u^2+v^2])))};
	p[4]={(Sqrt[s12](1+v^2))/(2 (1+u^2+v^2)),-((Sqrt[s12] u v (1+v^2+Sqrt[1+u^2+v^2]))/((1+u^2+v^2) (2+u^2+v^2+2 Sqrt[1+u^2+v^2]))),0,(Sqrt[s12] (-u^2 (-1+v^2)+(1+v^2) (2+v^2+2 Sqrt[1+u^2+v^2])))/(2 (1+u^2+v^2) (2+u^2+v^2+2 Sqrt[1+u^2+v^2]))};
	p[5]={(Sqrt[s12](u^2+v^2))/(2 (1+u^2+v^2)),(Sqrt[s12] u v)/(1+u^2+v^2),0,(Sqrt[s12] (u-v) (u+v))/(2 (1+u^2+v^2))};
	s[i_,j_]:=2*(p[i][[1]]*p[j][[1]]-p[i][[2]]*p[j][[2]]-p[i][[3]]*p[j][[3]]-p[i][[4]]*p[j][[4]]);
	z[3]=s[3,4]/(s[3,4]+s[4,5]);
	z[4]=s[3,4]/(s[3,4]+s[3,5]);
	m=s12*u*v/(24*Pi^3*(1 + u^2 + v^2)^3);
	Ar=(s[1,3]^2+s[1,4]^2+s[2,3]^2+s[2,4]^2)/(s[3,5]*s[4,5]*s12);
	born[3]=(s[1,4]^2+s[1,3]^2)/(s12^2);
	Ac35=-born[3]/s[3,5]*(1+z[3]);
	eikonal=s[3,4]/(s[3,5]*s[4,5]);
	born[soft]=(s[2,4]^2+s[1,4]^2)/(s12^2);
	As=2*born[soft]*eikonal;
	born[4]=(s[1,3]^2+s[2,3]^2)/(s12^2);
	Ac45=-born[4]/s[4,5]*(1+z[4]);
	integrand=Ar-Ac45-Ac35-As;
	sol=integrand*m;
	Return[sol];
];


sol = NIntegrate[evalIntegrand[1.,u,v],{u,0,Infinity},{v,0,Infinity},Method->"AdaptiveMonteCarlo",PrecisionGoal->2];
Print[sol];




(* ::Package:: *)

dOneMinusTwoEpsOmega=2*Power[Pi,1-eps]/Gamma[1-eps]


(*a=sh35,b=sh45*)
globalSoftIntegrand=Power[2*Pi,2*eps-3]/4*Power[-s34,1-eps]*Power[a*b,-eps]*dOneMinusTwoEpsOmega
globalSoft=Integrate[globalSoftIntegrand,{a,0,1},{b,0,1},Assumptions->Re[eps]<1]


Series[globalSoft,{eps,0,0}]


globalSoftIntegrand/.eps->0

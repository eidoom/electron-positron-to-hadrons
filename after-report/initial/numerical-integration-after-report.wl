(* ::Package:: *)

zHat={0,0,1};
Rx[a_]:={{1,0,0},{0,Cos[a],-Sin[a]},{0,Sin[a],Cos[a]}};
Ry[a_]:={{Cos[a],0,Sin[a]},{0,1,0},{-Sin[a],0,Cos[a]}};
Rz[a_]:={{Cos[a],-Sin[a],0},{Sin[a],Cos[a],0},{0,0,1}};
(* measure=d[\[Alpha]]*d[\[Theta]]*d[x[3]]*d[x[4]]; *)
evalIntegrand[s12_?NumericQ,\[Alpha]_?NumericQ,\[Theta]_?NumericQ,y3_?NumericQ,y4_?NumericQ]:=Module[
	{Ar,Ac45,Ac35,As,integrand,sol,c\[Beta],s\[Beta],m,z,J,x,p,s,born,j,eikonal},
	x[3]=1-y3;
	x[4]=1-y4;
	c\[Beta]=1+2*(1-x[3]-x[4])/(x[3]*x[4]);
	s\[Beta]=Sqrt[1-Power[c\[Beta],2]];
	RyBeta=Ry[\[Beta]]/.Sin[\[Beta]]->s\[Beta]/.Cos[\[Beta]]->c\[Beta];
	p[1]=Sqrt[s12]/2*Prepend[zHat,-1];
	p[2]=Sqrt[s12]/2*Prepend[-zHat,-1];
	p[3]=Sqrt[s12]/2*x[3]*Prepend[Ry[\[Theta]].zHat,1];
	p[4]=Sqrt[s12]/2*x[4]*Prepend[Ry[\[Theta]].Rz[\[Alpha]].RyBeta.zHat,1];
	p[5]=-p[1]-p[2]-p[3]-p[4];
	s[i_,j_]:=2*(p[i][[1]]*p[j][[1]]-p[i][[2]]*p[j][[2]]-p[i][[3]]*p[j][[3]]-p[i][[4]]*p[j][[4]]);
	z[3]=s[3,4]/(s[3,4]+s[4,5]);
	z[4]=s[3,4]/(s[3,4]+s[3,5]);
    j[3]=Power[(s[3,4]+s[3,5]+s[4,5])/(s[3,4]+s[4,5]),2];
	j[4]=Power[(s[3,4]+s[3,5]+s[4,5])/(s[3,4]+s[3,5]),2];
	m=s12*Sin[\[Theta]]/Power[4*Pi,4];
	Ar=(s[1,3]^2+s[1,4]^2+s[2,3]^2+s[2,4]^2)/(s[3,5]*s[4,5]*s12);
	born[3]=(s[1,4]^2+s[2,4]^2)/(s12^2);
	Ac35=-j[3]*born[3]/s[3,5]*(1+z[3]);
	eikonal=(s[3,4]+s[4,5])^2/(s[3,5]*s[4,5]*s[3,4]);
	(*eikonal=s[3,4]/(s[3,5]*s[4,5]);*)
	(*p[p35p]=p[3]+p[5]-(s[4,5]/(s[3,4]+s[3,5]))*p[4];
	born[alt3]=(s[1,p35p]^2+s[2,p35p]^2)/(s12^2);
	As=2*born[alt3]*eikonal;*)
	As=j[3]*2*born[3]*eikonal;
	born[4]=(s[1,3]^2+s[2,3]^2)/(s12^2);
	Ac45=-j[4]*born[4]/s[4,5]*(1+z[4]);
	integrand=Ar-Ac45-Ac35-As;
	sol=integrand*m;
	Return[sol];
];


eps = 1.*10^(-16);
sol = NIntegrate[evalIntegrand[1.,a,th,y3,y4],{a,0.,N[Pi]},{th,0.,N[Pi]},{y3,eps,1.-eps},{y4,eps,1.-y3-eps},Method->"AdaptiveMonteCarlo",PrecisionGoal->2];
Print[sol];


-0.062447256238452875`
-0.059018660315481634`


evalIntegrand[1.,0.3,0.2,0.4,0.1]

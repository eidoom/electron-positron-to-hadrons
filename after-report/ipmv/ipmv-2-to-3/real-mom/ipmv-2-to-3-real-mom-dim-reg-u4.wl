(* ::Package:: *)

SymbolForm[expr_] := DisplayForm[expr//.{
	(f:(u|q|qp|s|l|lt))[i__]:>Subscript[ToString[f],StringJoin[ToString/@{i}]],
	eps->\[Epsilon]
	}];
JacobianMatrix[f_List?VectorQ,x_List]:=Outer[D,f,x]/;Equal@@(Dimensions/@{f, x})
JacobianDeterminant[f_List?VectorQ,x_List]:=Det[JacobianMatrix[f,x]]/;Equal@@(Dimensions/@{f,x})
lRep={
	l[1]->Power[s,1/4]*{0,1},
	l[2]->Power[s,1/4]*{1,0},
	l[3]->Power[s,1/4]*{u[1],u[2]},
	l[4]->Power[s,1/4]*{u[3]*Exp[I*phi],u[4]}
	};
assume=Append[Flatten[{Im[#]==0,#>=0}&/@Join[u/@Range[5],{s,phi}]],phi<2*Pi];
simAss[expr_]:=Simplify[expr,Assumptions->assume];
ltRep={
	lt[i_]:>simAss[Conjugate[l[i]]/.lRep]
	};
in=2;
m=5;
qpRep=qp[i_]:>FullSimplify[Tr[Dot[PauliMatrix[#],simAss[KroneckerProduct[lt[i],l[i]]/.lRep/.ltRep]]]/2&/@Range[0,3]]/;i<m;
qRep=q[i_]:>If[i<=in,-1,1]*(qp[i]/.qpRep)/;i<5;
sRep=s[i_,j_]:>2*dot[q[i],q[j]];
dotRep=dot[i_List,j_List]:>(i[[1]]*j[[1]]-i[[2]]*j[[2]]-i[[3]]*j[[3]]-i[[4]]*j[[4]]); 
unpack=q[i_,j__]:>q[i]+q[j];


{MatrixForm[l[#]/.lRep]&/@Range[m-1]}//TableForm//SymbolForm
{MatrixForm[lt[#]/.ltRep]&/@Range[m-1]}//TableForm//SymbolForm
{MatrixForm[q[#]/.qRep]&/@Range[m-1]}//TableForm//SymbolForm


qMRep=q[m]->(-q@@Range[m-1]//.unpack/.qRep//FullSimplify);
q[m]/.qMRep//MatrixForm//SymbolForm
qMsq=dot[q[m],q[m]]/.qMRep/.dotRep//FullSimplify;
qMsq//SymbolForm
var=u[4];
vSols=Solve[0==qMsq,{var}];
vSols//SymbolForm
vSol=vSols[[2,1]];
vSol//SymbolForm
der=simAss[Abs[D[qMsq,Cos[phi]]]]


u[4]/.vSol/.phi->Pi/2/.u[1]->2


qSolRep={
	q[i_]:>(q[i]/.qRep/.qMRep/.Sin[a_]:>Sqrt[1-Cos[a]^2]/.vSol)//FullSimplify
};
{MatrixForm[q[#]]&/@Range[m]/.qSolRep}//TableForm//SymbolForm


Plus@@q/@Range[3,5];
zSq=dot[%,%]/.qSolRep/.dotRep//Factor


(*pRep={
	p[i_]:>(q[i]/.qSolRep)/;i<=2,
	p[3]->(q[3]/.qSolRep)//FullSimplify,
	p[4]->(q[4]/.qSolRep)//FullSimplify,
	p[5]->(q[5]/.qSolRep)//FullSimplify
};
{MatrixForm[p[#]]&/@Range[m]/.pRep}//TableForm//SymbolForm*)


Manipulate[
{MatrixForm[q[#]]&/@Range[in+1,m]/.qSolRep/.u[1]->u1/.u[2]->u2/.u[3]->u3/.u[4]->u4}//TableForm//SymbolForm,
{u1,u1LoLim,a},{u2,u2LoLim,a},{u3,u3LoLim,u3UpLim},{u4,u4LoLim,a}
]/.a->10^3


s@@@DeleteDuplicates[Sort/@Permutations[Range[5],{2}]];
Transpose[{%,%/.sRep/.qSolRep/.dotRep//FullSimplify}]//TableForm//SymbolForm


preFactor=\[Sigma]0*CF*\[Alpha]s/2/Pi;
realCoeff=(96*CF*Pi^2*\[Alpha]s*\[Sigma]0)/s[1,2];
realKinematics=(s[1, 3]^2 + s[1, 4]^2 + s[2, 3]^2 + s[2, 4]^2)/(s[3, 5]*s[4, 5]);
realInput=realCoeff*realKinematics/preFactor/.sRep/.qSolRep/.dotRep//FullSimplify;
%//SymbolForm


q[4]/.qRep//MatrixForm


en[4]=(q[4]/.qRep)[[1]];
q4DirOne=4;
q4ValOne=(q[4]/.qRep)[[q4DirOne]];
q4DirPar=Complement[Range[2,4],{q4DirOne}];
q4ValPar=simAss[Norm[(q[4]/.qRep)[[q4DirPar]]]];
jac4=Factor[JacobianDeterminant[{q4ValOne,q4ValPar},(u/@{3,4})]]*Times@@(d[u[#]]&/@{3,4});
en[3]=(q[3]/.qRep)[[1]];
q3DirOne=4;
q3ValOne=(q[3]/.qRep)[[q3DirOne]];
q3DirPar=Complement[Range[2,4],{q3DirOne}];
q3ValPar=simAss[Norm[(q[3]/.qRep)[[q3DirPar]]]];
jac3=Factor[JacobianDeterminant[{q3ValOne,q3ValPar},u/@{1,2}]]*Times@@(d[u[#]]&/@{1,2});
solidAngle=d[n_,Omega,___]:>2*Pi^((n+1)/2)/Gamma[(n+1)/2];
recurseAngle[i_]:=d[n_,Omega,i]:>d[n-1,Omega,i]*d[Cos[phi]]*Power[Sin[phi],n-2];


en[3]
en[4]


Product[d[n,q[i]]*Power[2*\[Pi],1-n]*delta["+",Power[q[i],2]],{i,3,5}]*delta[n,q@@Range[m]]*Power[2*\[Pi],n];
%/.d[n,q[3]]*delta["+",q[3]^2]->d[n-1,q[3]]/2/en[3];
%/.d[n-1,q[3]]->d[n-3,Omega,3]*d[q3[q3DirOne]]*d[q3[par]]*q3ValPar^(n-3);
%/.d[q3[q3DirOne]]*d[q3[par]]->jac3;
%/.d[n,q[4]]*delta["+",q[4]^2]->d[n-1,q[4]]/2/en[4];
%/.d[n-1,q[4]]->d[n-3,Omega,4]*d[q4[q4DirOne]]*d[q4[par]]*q4ValPar^(n-3);
%/.d[q4[q4DirOne]]*d[q4[par]]->jac4;
%/.recurseAngle[4];
%/.delta[n,q@@Range[m]]*d[n,q[m]]->1/.q[m]^2->qMsq;
%/.delta["+",qMsq]->delta["+",var-(var/.vSol)]/der;
%/.delta["+",var-(var/.vSol)]*d[var]->1;
%/.Sin[a_]:>Sqrt[1-Power[Cos[a],2]];
%/.vSol;
%/.n->4-2*eps;
%/.solidAngle;
%//simAss;
d[Phi]=%;
d[Phi]//SymbolForm
d[4,Phi]=d[Phi]/.eps->0;
d[4,Phi]//SymbolForm


lims=Reduce[Append[u[#]>=0&/@Range[4],-1<=(Cos[phi]/.vSol)<1],u/@Range[4]][[1]];
u1Lims=lims[[1,1;;5]]
u2Lims=lims[[2,1,1]]
u3Lims=lims[[2,1,2,1,1]]
u4Lims=lims[[2,1,2,1,2]]
(*u4LoLim=lims[[3,1]];
u4UpLim=lims[[3,5]];
u3LoLim=lims[[2,1]];
u3UpLim=lims[[2,5]];
u2LoLim=lims[[1,2]];
u2UpLim=Infinity;
u1LoLim=0;
u1UpLim=Infinity;*)


int1=Integrate[d[4,Phi]/d[u[4]],{u[4],u4LoLim,u4UpLim},Assumptions->Join[assume,{u[2]>u2LoLim,u[3]<u3UpLim,u[1]>u1LoLim}]]


int2=Integrate[int1/d[u[3]],{u[3],u3LoLim,u3UpLim},Assumptions->Join[assume,{u[2]>u2LoLim,u[1]>u1LoLim}]]


(*int3=Integrate[int2/d[u[2]],{u[2],u2LoLim,u2UpLim},Assumptions\[Rule]assume]*)


int4=Integrate[int2/d[u[1]],{u[1],u1LoLim,u1UpLim},Assumptions->assume]


int5=Integrate[int4/d[u[2]],{u[2],u2LoLim,u2UpLim},Assumptions->assume]


Integrate[d[4,Phi]/(Times@@(d[u[#]]&/@Range[4])),{u[1],u1LoLim,u1UpLim},{u[2],u2LoLim,u2UpLim},{u[3],u3LoLim,u3UpLim},{u[4],u4LoLim,u4UpLim},Assumptions->assume]


Integrate[bornCoeff*d[Phi]*bornKinIPMV/d[u]/2/s,{u,0,1}]
Series[%,{eps,0,1}]//FullSimplify










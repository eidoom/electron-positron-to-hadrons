(* ::Package:: *)

SymbolForm[expr_] := DisplayForm[expr//.{
	(f:(u|q|qp|s|l|lt))[i__]:>Subscript[ToString[f],StringJoin[ToString/@{i}]],
	eps->\[Epsilon]
	}];
JacobianMatrix[f_List?VectorQ,x_List]:=Outer[D,f,x]/;Equal@@(Dimensions/@{f, x})
JacobianDeterminant[f_List?VectorQ,x_List]:=Det[JacobianMatrix[f,x]]/;Equal@@(Dimensions/@{f,x})
lRep={
	l[3]->{0,1},
	l[4]->{1,0},
	l[1]->{u[1],u[2]},
	l[2]->{u[3],-u[4]},
	lt[3]->{0,1},
	lt[4]->{1,0},
	lt[1]->{-u[5],1},
	lt[2]->{1,1}
	};
in=2;
m=5;
assume=Flatten[{Im[#]==0,#>=0}&/@Prepend[u/@Range[5],s]];
simAss[expr_]:=Simplify[expr,Assumptions->assume];
qpRep=qp[i_]:>FullSimplify[Tr[Dot[PauliMatrix[#],simAss[KroneckerProduct[lt[i],l[i]]/.lRep]]]/2&/@Range[0,3]]/;i<m;
qRep=q[i_]:>If[i<=in,-1,1]*(qp[i]/.qpRep)/;i<5;
sRep=s[i_,j_]:>2*dot[p[i],p[j]];
dotRep=dot[i_List,j_List]:>(i[[1]]*j[[1]]-i[[2]]*j[[2]]-i[[3]]*j[[3]]-i[[4]]*j[[4]]); 
unpack=q[i_,j__]:>q[i]+q[j];


{MatrixForm[l[#]/.lRep]&/@Range[m-1]}//TableForm//SymbolForm
{MatrixForm[lt[#]/.lRep]&/@Range[m-1]}//TableForm//SymbolForm
{MatrixForm[q[#]/.qRep]&/@Range[m-1]}//TableForm//SymbolForm


qMRep=q[m]->(-q@@Range[m-1]//.unpack/.qRep//FullSimplify);
q[m]/.qMRep//MatrixForm//SymbolForm
qMsq=dot[q[m],q[m]]/.qMRep/.dotRep//FullSimplify;
qMsq//SymbolForm
Solve[0==qMsq,{u[#]}]&/@Range[m]//SymbolForm
mm=m;
vSol=Solve[0==qMsq,{u[mm]}][[1,1]];
vSol//SymbolForm
der=simAss[Abs[D[qMsq,u[mm]]]]


qSolRep={
	q[i_]:>FullSimplify[q[i]/.qRep/.vSol]/;i<5,
	q[5]->(q[5]/.qMRep/.vSol)//FullSimplify
};
{MatrixForm[q[#]]&/@Range[m]/.qSolRep}//TableForm//SymbolForm


Plus@@q/@Range[3,5];
zSq=dot[%,%]/.qSolRep/.dotRep//Factor


pRep={
	p[i_]:>(q[i]/.qSolRep)*Sqrt[s]/Sqrt[zSq]
};
{MatrixForm[p[#]]&/@Range[m]/.pRep}//TableForm//SymbolForm


Manipulate[
{MatrixForm[p[#]]&/@Range[in+1,m]/.pRep/.u[1]->u1/.u[2]->u2/.u[3]->u3/.u[4]->u4}//TableForm//SymbolForm,
{u1,u1LoLim,a},{u2,u2LoLim,a},{u3,u3LoLim,u3UpLim},{u4,u4LoLim,a}
]/.a->10^3


s@@@DeleteDuplicates[Sort/@Permutations[Range[5],{2}]];
Transpose[{%,%/.sRep/.pRep/.dotRep//FullSimplify}]//TableForm//SymbolForm


preFactor=\[Sigma]0*CF*\[Alpha]s/2/Pi;
realCoeff=(96*CF*Pi^2*\[Alpha]s*\[Sigma]0)/s[1,2];
realKinematics=(s[1, 3]^2 + s[1, 4]^2 + s[2, 3]^2 + s[2, 4]^2)/(s[3, 5]*s[4, 5]);
realInput=realCoeff*realKinematics/preFactor/.sRep/.pRep/.dotRep//FullSimplify;
%//SymbolForm


{MatrixForm[q[#]/.qRep/.qMRep]&/@Range[m]}//TableForm//SymbolForm


q4DirOne=4;
q4DirPar=Drop[Range[2,4],{q4DirOne-1}];
q4ValPar=simAss[Norm[(q[4]/.qRep)[[q4DirPar]]]];
q3DirOne=2;
q3DirTwo=4;
q3DirPar=3;
q3ValPar=simAss[Norm[(q[3]/.qRep)[[q3DirPar]]]];
Product[d[n,p[i]]*Power[2*\[Pi],1-n]*delta["+",Power[p[i],2]],{i,3,5}]*delta[n,p@@Range[m]]*Power[2*\[Pi],n];
%*Power[zSq/s,n/2*(1-(m-in))+(m-in)]/.d[n,p[i_]]->d[n,q[i]]/.delta["+",Power[p[i_],2]]->delta["+",Power[q[i],2]]/.delta[n,p@@Range[m]]->delta[n,q@@Range[m]]
%/.delta[n,q@@Range[m]]*d[n,q[3]]->1/.q[3]^2->q3sq;

(*%/.delta["+",qMsq]->delta["+",u[m]-(u[m]/.vSol)]/der;
%/.d[n,q[3]]*delta["+",q[3]^2]->d[n-1,q[3]]/2/(q[3]/.qRep)[[1]];
%/.d[n,q[4]]*delta["+",q[4]^2]->d[n-1,q[4]]/2/(q[4]/.qRep)[[1]];
%/.d[n-1,q[4]]->d[n-3,Omega]*d[q4[q4DirOne]]*d[q4[par]]*q4ValPar^(n-3);
%/.d[q4[q4DirOne]]*d[q4[par]]->Factor[JacobianDeterminant[{(q[4]/.qRep)[[q4DirOne]],q4ValPar},(u/@Range[3,4])]]*Times@@(d[u[#]]&/@Range[3,4]);
%/.d[n-1,q[3]]->d[n-4,Omega]*d[q3[q3DirOne]]*d[q3[q3DirTwo]]*d[q3[par]]*q3ValPar^(n-4);
%/.d[q3[q3DirOne]]*d[q3[q3DirTwo]]*d[q3[par]]->Factor[JacobianDeterminant[{(q[3]/.qRep)[[q3DirTwo]],(q[3]/.qRep)[[q3DirOne]],q3ValPar},u/@{1,2,5}]]*Times@@(d[u[#]]&/@{1,2,5});
%/.delta["+",u[m]-(u[m]/.vSol)]*d[u[m]]->1/.vSol;
%/.n->4-2*eps;
%//Factor;
%/.d[n_,Omega]:>2*Pi^((n+1)/2)/Gamma[(n+1)/2];
%//simAss;
%//FullSimplify;
d[Phi]=%;
d[Phi]//SymbolForm
d[4,Phi]=d[Phi]/.eps->0;
d[4,Phi]//SymbolForm*)


lims=Reduce[Append[u[#]>0&/@Range[4],(u[5]/.vSol)>0],u/@Range[4]][[2,2]]
u4LoLim=lims[[3,1]];
u4UpLim=lims[[3,5]];
u3LoLim=lims[[2,1]];
u3UpLim=lims[[2,5]];
u2LoLim=lims[[1,2]];
u2UpLim=Infinity;
u1LoLim=0;
u1UpLim=Infinity;


int1=Integrate[d[4,Phi]/d[u[4]],{u[4],u4LoLim,u4UpLim},Assumptions->Join[assume,{u[2]>u2LoLim,u[3]<u3UpLim,u[1]>u1LoLim}]]


int2=Integrate[int1/d[u[3]],{u[3],u3LoLim,u3UpLim},Assumptions->Join[assume,{u[2]>u2LoLim,u[1]>u1LoLim}]]


(*int3=Integrate[int2/d[u[2]],{u[2],u2LoLim,u2UpLim},Assumptions\[Rule]assume]*)


int4=Integrate[int2/d[u[1]],{u[1],u1LoLim,u1UpLim},Assumptions->assume]


int5=Integrate[int4/d[u[2]],{u[2],u2LoLim,u2UpLim},Assumptions->assume]


Integrate[d[4,Phi]/(Times@@(d[u[#]]&/@Range[4])),{u[1],u1LoLim,u1UpLim},{u[2],u2LoLim,u2UpLim},{u[3],u3LoLim,u3UpLim},{u[4],u4LoLim,u4UpLim},Assumptions->assume]


Integrate[bornCoeff*d[Phi]*bornKinIPMV/d[u]/2/s,{u,0,1}]
Series[%,{eps,0,1}]//FullSimplify










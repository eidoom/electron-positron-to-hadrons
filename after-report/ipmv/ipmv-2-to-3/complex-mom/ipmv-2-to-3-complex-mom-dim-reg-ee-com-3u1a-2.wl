(* ::Package:: *)

SymbolForm[expr_] := DisplayForm[expr//.{
	(f:(u|q|qp|s|l|lt))[i__]:>Subscript[ToString[f],StringJoin[ToString/@{i}]],
	eps->\[Epsilon]
	}];
JacobianMatrix[f_List?VectorQ,x_List]:=Outer[D,f,x]/;Equal@@(Dimensions/@{f, x})
JacobianDeterminant[f_List?VectorQ,x_List]:=Det[JacobianMatrix[f,x]]/;Equal@@(Dimensions/@{f,x})
lRep={
	l[1]->Power[s,1/4]*{0,1},
	l[2]->Power[s,1/4]*{1,0},
	l[3]->Power[s,1/4]*{u[1],u[2]},
	l[4]->Power[s,1/4]*Exp[I*phi]*{u[3],u[4]},
	lt[1]->Power[s,1/4]*{0,1},
	lt[2]->Power[s,1/4]*{1,0},
	lt[3]->Power[s,1/4]*{1,1},
	lt[4]->Power[s,1/4]*{1,-1}
	};
in=2;
m=5;
assume=Flatten[{Im[#]==0,#>=0}&/@Join[u/@Range[5],{s,phi}]];
simAss[expr_]:=Simplify[expr,Assumptions->assume];
qpRep=qp[i_]:>FullSimplify[Tr[Dot[PauliMatrix[#],simAss[KroneckerProduct[lt[i],l[i]]/.lRep]]]/2&/@Range[0,3]]/;i<m;
qRep=q[i_]:>If[i<=in,-1,1]*(qp[i]/.qpRep);
sRep=s[i_,j_]:>2*dot[q[i],q[j]];
dotRep=dot[i_List,j_List]:>(i[[1]]*j[[1]]-i[[2]]*j[[2]]-i[[3]]*j[[3]]-i[[4]]*j[[4]]); 
unpack=q[i_,j__]:>q[i]+q[j];


{MatrixForm[l[#]/.lRep]&/@Range[m-1]}//TableForm//SymbolForm
{MatrixForm[lt[#]/.lRep]&/@Range[m-1]}//TableForm//SymbolForm
{MatrixForm[q[#]/.qRep]&/@Range[m-1]}//TableForm//SymbolForm


1/(4*Abs[(q[2]/.qRep)[[1]](q[1]/.qRep)[[4]]-(q[1]/.qRep)[[1]](q[2]/.qRep)[[4]]]//simAss)


qMRep=q[m]->(-q@@Range[m-1]//.unpack/.qRep//FullSimplify);
q[m]/.qMRep//MatrixForm//SymbolForm
qMsq=dot[q[m],q[m]]/.qMRep/.dotRep//FullSimplify;
qMsq//SymbolForm
Solve[0==qMsq,{#}]&/@Join[(u/@Range[m-1]),{phi}]//SymbolForm
var=u[1];
vSol=Solve[0==qMsq,{var}][[1,1]];
vSol//SymbolForm
der=simAss[Abs[D[qMsq,var]]]


qSolRep={
	q[i_]:>(q[i]/.qRep)/;i<=2,
	q[3]->(q[3]/.qRep/.vSol)//FullSimplify,
	q[4]->(q[4]/.qRep/.vSol)//FullSimplify,
	q[5]->(q[5]/.qMRep/.vSol)//FullSimplify
};
{MatrixForm[q[#]]&/@Range[in+1,m]/.qSolRep}//TableForm//SymbolForm


Plus@@q/@Range[3,5];
zSq=dot[%,%]/.qSolRep/.dotRep//Factor


s@@@DeleteDuplicates[Sort/@Permutations[Range[5],{2}]];
Transpose[{%,%/.sRep/.qSolRep/.dotRep//FullSimplify}]//TableForm//SymbolForm


preFactor=\[Sigma]0*CF*\[Alpha]s/2/Pi;
realCoeff=(96*CF*Pi^2*\[Alpha]s*\[Sigma]0)/s[1,2];
realKinematics=(s[1, 3]^2 + s[1, 4]^2 + s[2, 3]^2 + s[2, 4]^2)/(s[3, 5]*s[4, 5]);
realInput=realCoeff*realKinematics/preFactor/.sRep/.qSolRep/.dotRep//simAss;
%//SymbolForm


en[4]=(q[4]/.qRep)[[1]];
q4DirOne=2;
q4ValOne=(q[4]/.qRep)[[q4DirOne]];
q4DirPar=Complement[Range[2,4],{q4DirOne}];
q4ValPar=simAss[Norm[(q[4]/.qRep)[[q4DirPar]]]];
jac4=simAss[JacobianDeterminant[{q4ValOne,q4ValPar},(u/@{3,4})]]*Times@@(d[u[#]]&/@{3,4});
en[3]=(q[3]/.qRep)[[1]];
q3DirOne=4;
q3ValOne=(q[3]/.qRep)[[q3DirOne]];
q3DirPar=Complement[Range[2,4],{q3DirOne}];
q3ValPar=simAss[Norm[(q[3]/.qRep)[[q3DirPar]]]];
jac3=simAss[JacobianDeterminant[{q3ValOne,q3ValPar},u/@{1,2}]]*Times@@(d[u[#]]&/@{1,2});
solidAngle=d[n_,Omega,___]:>2*Pi^((n+1)/2)/Gamma[(n+1)/2];
recurseAngle[i_]:=d[n_,Omega,i]:>d[n-1,Omega,i]*d[phi]*Power[Sin[phi],n-1];


q[4]/.qRep//MatrixForm
q4ValOne
q4ValPar
jac4


Product[d[n,q[i]]*Power[2*\[Pi],1-n]*delta["+",Power[q[i],2]],{i,3,5}]*delta[n,q@@Range[m]]*Power[2*\[Pi],n];
%/.d[n,q[3]]*delta["+",q[3]^2]->d[n-1,q[3]]/2/en[3];
%/.d[n-1,q[3]]->d[n-3,Omega,3]*d[q3[q3DirOne]]*d[q3[par]]*q3ValPar^(n-3);
%/.d[q3[q3DirOne]]*d[q3[par]]->jac3;
%/.d[n,q[4]]*delta["+",q[4]^2]->d[n-1,q[4]]/2/en[4];
%/.d[n-1,q[4]]->d[n-3,Omega,4]*d[q4[q4DirOne]]*d[q4[par]]*q4ValPar^(n-3);
%/.d[q4[q4DirOne]]*d[q4[par]]->jac4;
%/.recurseAngle[4];
%/.delta[n,q@@Range[m]]*d[n,q[m]]->1/.q[m]^2->qMsq;
%/.delta["+",qMsq]->delta["+",var-(var/.vSol)]/der;
%/.delta["+",var-(var/.vSol)]*d[var]->1;
%/.Sin[a_]:>Sqrt[1-Power[Cos[a],2]];
%/.vSol;
%/.n->4-2*eps;
%/.solidAngle;
%//simAss;
d[Phi]=%;
d[Phi]//SymbolForm
d[4,Phi]=d[Phi]/.eps->0;
d[4,Phi]//SymbolForm


lims=Reduce[Append[(u[#]/.vSol)>=0&/@Range[4],0<=phi<2*Pi],Append[u/@Range[4],phi]]
u4LoLim=0;
u4UpLim=Infinity;
u3LoLim=0;
u3UpLim=Infinity;
u2LoLim=0;
u2UpLim=Infinity;
phiLoLim=0;
phiUpLim=2*Pi;


Integrate[d[4,Phi]/(Times@@(d[u[#]]&/@Range[4])),{phi,phiLoLim,phiUpLim},{u[2],u2LoLim,u2UpLim},{u[3],u3LoLim,u3UpLim},{u[4],u4LoLim,u4UpLim},Assumptions->assume]


Integrate[bornCoeff*d[Phi]*bornKinIPMV/d[u]/2/s,{u,0,1}]
Series[%,{eps,0,1}]//FullSimplify


 

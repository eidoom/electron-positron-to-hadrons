(* ::Package:: *)

SymbolForm[expr_] := DisplayForm[expr//.{
	(f:(x))[i__]:>Subscript[ToString[f],StringJoin[ToString/@{i}]]
	}];
in=2;
m=5;
spARep=spA[i_,j_]:>Dot[l[i],l[j]];
spSRep=spS[i_,j_]:>Dot[lt[i],lt[j]];
(*muRep={
	mu[1]->{0,0},
	mu[2]->{0,0},
	mu[3]->{0,1},
	mu[4]->{x[4],1},
	mu[5]->{1,x[5]/x[4]}
};
lRep={
	l[1]->{1,0},
	l[2]->{0,1},
	l[3]->{1/x[1],1},
	l[4]->{1/x[1]+1/x[2],1},
	l[5]->{1/x[1]+1/x[2]+1/x[3],1}
};*)
muRep={
	mu[1]->{0,0},
	mu[2]->{0,0},
	mu[3]->{0,1},
	mu[4]->{1,1},
	mu[5]->{1,0}
};
lRep={
	l[1]->Power[s,-1/2]*{1,0},
	l[2]->Power[s,-1/2]*{0,1},
	l[3]->Power[s,-1/2]*{x[1],1},
	l[4]->Power[s,-1/2]*{x[2],x[3]},
	l[5]->Power[s,-1/2]*{1,x[4]}
};
ltRep=lt[i_]:>Module[
	{iPlus=Mod[i+1,m,1],iMinus=Mod[i-1,m,1]},
	(spA[i,iPlus]*mu[iMinus]+spA[iPlus,iMinus]*mu[i]+spA[iMinus,i]*mu[iPlus])/(spA[i,iPlus]*spA[iMinus,i])/.muRep/.spARep/.lRep//FullSimplify
	];
qpRep=qp[i_]:>FullSimplify[Tr[Dot[PauliMatrix[#],Simplify[KroneckerProduct[lt[i],l[i]]/.lRep/.ltRep,Assumptions->Im[u[i]]==0&&Im[phi]==0&&Im[s]==0&&s>=0]]]/2&/@Range[0,3]]/;i<=m;
qRep=q[i_]:>If[i<=in,-1,1]*(qp[i]/.qpRep)//FullSimplify;
sRep=s[i_,j_]:>2*dot[q[i],q[j]];
dotRep=dot[i_List,j_List]:>(i[[1]]*j[[1]]-i[[2]]*j[[2]]-i[[3]]*j[[3]]-i[[4]]*j[[4]]); 
unpack=q[i_,j__]:>q[i]+q[j];
Z={{"Z =",{l/@#,mu/@#}&[Range[m]]/.lRep/.muRep//MatrixForm}}//TableForm//SymbolForm
Transpose[{lt/@#,MatrixForm[lt[#]]&/@#/.ltRep}&[Range[m]]]//TableForm//SymbolForm
Transpose[{q/@#,MatrixForm[q[#]]&/@#/.qRep}&[Range[m]]]//TableForm//SymbolForm
s@@@DeleteDuplicates[Sort/@Permutations[Range[5],{2}]];
Transpose[{%,%/.sRep/.qRep/.dotRep//FullSimplify}]//TableForm//SymbolForm
realKinematics=(s[1, 3]^2 + s[1, 4]^2 + s[2, 3]^2 + s[2, 4]^2)/(s[3, 5]*s[4, 5]);
realKinematics/.sRep/.qRep/.dotRep//FullSimplify//SymbolForm


enRep=en[i_]:>(q[i]/.qRep)[[1]];


Product[d[n,q[i]]*Power[2*\[Pi],1-n]*delta["+",Power[q[i],2]],{i,3,5}]*delta[n,q@@Range[m]]*Power[2*\[Pi],n]
%//.d[n,q[i_]]*delta["+",q[i_]^2]:>d[n-1,q[i]]/2/en[i]





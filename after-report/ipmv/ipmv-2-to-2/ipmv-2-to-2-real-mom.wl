(* ::Package:: *)

lRep={
	l[1]->Power[s,1/4]*{0,1},
	l[2]->Power[s,1/4]*{1,0},
	l[3]->Power[s,1/4]*{u[3],v[3]}(*,
	l[i_]\[RuleDelayed]Power[s,1/4]*{u[i]*Exp[I*phi[i]],v[i]}/;i\[GreaterEqual]3*)
	};
qpRep=qp[i_]:>FullSimplify[Tr[Dot[PauliMatrix[#],Simplify[KroneckerProduct[Conjugate[l[i]],l[i]]/.lRep,Assumptions->Im[v[i]]==0&&Im[u[i]]==0&&Im[phi[i]]==0&&Im[s]==0&&s>=0]]]/2&/@Range[0,3]]/;i<4;
sRep=s[i_,j_]:>2*dot[q[i],q[j]];
dotRep=dot[i_List,j_List]:>(i[[1]]*j[[1]]-i[[2]]*j[[2]]-i[[3]]*j[[3]]-i[[4]]*j[[4]]); 
unpack={q[i_,j__]:>q[i]+q[j],p[i_,j__]:>p[i]+p[j]};


{MatrixForm[qp[#]/.qpRep]&/@Range[4]}//TableForm
2*dot[qp[1],qp[2]]/.qpRep/.dotRep


qRep=q[i_]:>If[i<3,-1,1]*(qp[i]/.qpRep);


{MatrixForm[q[#]/.qRep]&/@Range[4]}//TableForm


q4Rep=q[4]->(-q[1,2,3]//.unpack/.qRep//FullSimplify);
q[4]/.q4Rep//MatrixForm


dot[q[3],q[3]]/.qRep/.dotRep
%//FullSimplify


q4sq=dot[q[4],q[4]]/.q4Rep/.dotRep//FullSimplify


v3sol=Solve[0==q4sq,{v[3]}][[2]]


q[4]/.q4Rep/.v3sol//FullSimplify//MatrixForm


q[3]/.qRep/.v3sol//FullSimplify//MatrixForm


qSolRep={
	q[3]->(q[3]/.qRep/.v3sol),
	q[4]->(q[4]/.q4Rep/.v3sol),
	q[i_]:>(q[i]/.qRep)/;i<3
};


q[1,2,3]//.unpack/.qRep//FullSimplify


JacobianMatrix[f_List?VectorQ,x_List]:=Outer[D,f,x]/;Equal@@(Dimensions/@{f, x})
JacobianDeterminant[f_List?VectorQ,x_List]:=Det[JacobianMatrix[f,x]]/;Equal@@(Dimensions/@{f,x})


JacobianDeterminant[
	(q[3]/.qRep)[[2;;]],
	{u[3],v[3],phi[3]}
	]//FullSimplify


Simplify[Sqrt[Dot[(q[3]/.qRep)[[2;;]],(q[3]/.qRep)[[2;;]]]//FullSimplify],Im[u[3]]==0&&Im[v[3]]==0]


(2*Pi)^(-2)*delta[4,q[1,2,3,4]]*d[4,q[3]]*delta["+",q[3]^2]*d[4,q[4]]*delta["+",q[4]^2]
%/.delta[4,q[1,2,3,4]]*d[4,q[4]]->1/.q[4]^2->q4sq
%/.delta["+",q4sq]->delta["+",v[3]-(v[3]/.v3sol)]/Simplify[Abs[D[q4sq,v[3]]],Assumptions->s>=0&&v[3]>=0]
%/.d[4,q[3]]*delta["+",q[3]^2]->d[3,q[3]]/2/(q[3]/.qRep)[[1]]
%/.d[3,q[3]]->2*Pi*d[q3z]*d[q3par]*(q[3]/.qRep)[[2]]
%/.d[q3z]*d[q3par]->Factor[JacobianDeterminant[(q[3]/.qRep)[[#]]&/@{4,2},{u[3],v[3]}]]*d[u[3]]*d[v[3]]
%/.delta["+",v[3]-(v[3]/.v3sol)]*d[v[3]]->1/.v3sol
d[Phi]=%;


bornCoeff=32*alpha^2*Nc*Pi^2*Qq^2;
bornKin=(s[1, 3]^2 + s[1, 4]^2)/s[1, 2]^2;


bornKinIPMV=bornKin/.sRep/.qSolRep/.dotRep//FullSimplify


s[1,2]/.sRep/.qSolRep/.dotRep//FullSimplify


Integrate[bornCoeff*d[Phi]*bornKinIPMV/d[u[3]]/2/s,{u[3],0,1}]

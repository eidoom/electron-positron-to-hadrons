(* ::Package:: *)

JacobianMatrix[f_List?VectorQ,x_List]:=Outer[D,f,x]/;Equal@@(Dimensions/@{f, x})
JacobianDeterminant[f_List?VectorQ,x_List]:=Det[JacobianMatrix[f,x]]/;Equal@@(Dimensions/@{f,x})
lRep={
	l[1]->Power[s,1/4]*{0,1},
	l[2]->Power[s,1/4]*{1,0},
	l[3]->Power[s,1/4]*{u,v},
	lt[1]->Power[s,1/4]*{0,1},
	lt[2]->Power[s,1/4]*{1,0},
	lt[3]->Power[s,1/4]*{1,1}
	};
qpRep=qp[i_]:>FullSimplify[Tr[Dot[PauliMatrix[#],Simplify[KroneckerProduct[lt[i],l[i]]/.lRep,Assumptions->Im[v]==0&&Im[u]==0&&Im[s]==0&&s>=0]]]/2&/@Range[0,3]]/;i<4;
qRep=q[i_]:>If[i<3,-1,1]*(qp[i]/.qpRep);
sRep=s[i_,j_]:>2*dot[q[i],q[j]];
dotRep=dot[i_List,j_List]:>(i[[1]]*j[[1]]-i[[2]]*j[[2]]-i[[3]]*j[[3]]-i[[4]]*j[[4]]); 
unpack={q[i_,j__]:>q[i]+q[j],p[i_,j__]:>p[i]+p[j]};


{MatrixForm[l[#]/.lRep]&/@Range[3]}//TableForm
{MatrixForm[lt[#]/.lRep]&/@Range[3]}//TableForm


{MatrixForm[q[#]/.qRep]&/@Range[3]}//TableForm


q4Rep=q[4]->(-q[1,2,3]//.unpack/.qRep//FullSimplify);
q[4]/.q4Rep//MatrixForm
q4sq=dot[q[4],q[4]]/.q4Rep/.dotRep//FullSimplify
vSol=Solve[0==q4sq,{v}][[1,1]]


lowLim=0;
upLim=u/.Solve[0==v/.vSol,{u}][[1]];
{lowLim,upLim}


qSolRep={
	q[3]->(q[3]/.qRep/.vSol)//FullSimplify,
	q[4]->(q[4]/.q4Rep/.vSol)//FullSimplify,
	q[i_]:>(q[i]/.qRep)/;i<3
};
{MatrixForm[q[#]]&/@Range[3,4]/.qSolRep}//TableForm


Manipulate[{MatrixForm[q[#]]&/@Range[3,4]/.qSolRep}/.u->uu//TableForm//MatrixForm,{{uu,0.},lowLim,upLim}]


q[3]+q[4]/.qSolRep//FullSimplify


{s[1,2],s[1,3],s[1,4],s[2,3],s[2,4],s[3,4]}/.sRep/.qSolRep/.dotRep//FullSimplify


bornCoeff=32*alpha^2*Nc*Pi^2*Qq^2;
bornKin=(s[1, 3]^2 + s[1, 4]^2)/s[1, 2]^2;
bornKinIPMV=bornKin/.sRep/.qSolRep/.dotRep//Expand


specDir=4;
parDir=Drop[Range[2,4],{specDir-1}];
parVal=Simplify[Norm[(q[3]/.qRep)[[parDir]]],Assumptions->s>=0&&v>=0&&u>=0];
(2*Pi)^(2-n)*delta[n,q[1,2,3,4]]*d[n,q[3]]*delta["+",q[3]^2]*d[n,q[4]]*delta["+",q[4]^2]
%/.delta[n,q[1,2,3,4]]*d[n,q[4]]->1/.q[4]^2->q4sq
%/.delta["+",q4sq]->delta["+",v-(v/.vSol)]/Simplify[Abs[D[q4sq,v]],Assumptions->s>=0&&v>=0&&u>=0]
%/.d[n,q[3]]*delta["+",q[3]^2]->d[n-1,q[3]]/2/(q[3]/.qRep)[[1]]
%/.d[n-1,q[3]]->d[n-3,Omega]*d[q3[specDir]]*d[q3par]*parVal^(n-3)
%/.d[q3[specDir]]*d[q3par]->Factor[JacobianDeterminant[{(q[3]/.qRep)[[specDir]],parVal},{u,v}]]*d[u]*d[v]
%/.delta["+",v-(v/.vSol)]*d[v]->1/.vSol
%/.n->4-2*eps
%//Factor
%/.d[n_,Omega]:>2*Pi^((n+1)/2)/Gamma[(n+1)/2]
%//Simplify
d[Phi]=%;


Integrate[d[Phi]/d[u],{u,lowLim,upLim}]
Series[%,{eps,0,1}]//FullSimplify


Integrate[bornCoeff*d[Phi]*bornKinIPMV/d[u]/2/s,{u,lowLim,upLim}]
Series[%,{eps,0,1}]//FullSimplify




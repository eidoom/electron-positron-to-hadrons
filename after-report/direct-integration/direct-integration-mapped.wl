(* ::Package:: *)

(* ::Section::Closed:: *)
(*Setup*)


PS3=(4^(-4 + 3*eps)*Pi^(-7/2 + 2*eps)*d[Cos[alpha]]*d[Cos[theta]]*d[x[1]]*d[x[2]]*s[1, 2]^(1 - 2*eps)*Sin[alpha]^(-1 - 2*eps))/
 (Gamma[1/2 - eps]*Gamma[1 - eps]*Sin[theta]^(2*eps)*((-1 + x[1])*(-1 + x[2])*(-1 + x[1] + x[2]))^eps)


SetAttributes[s,Orderless];
zHat ={0,0,1};
Ry[a_]:={{Cos[a],0,Sin[a]},{0,1,0},{-Sin[a],0,Cos[a]}};
Rz[a_]:={{Cos[a],-Sin[a],0},{Sin[a],Cos[a],0},{0,0,1}};
fixedMomentaReal={
	p[1]->Sqrt[s[1,2]]/2 Prepend[zHat,-1],
	p[2]->Sqrt[s[1,2]]/2 Prepend[-zHat,-1],
	p[3]->Sqrt[s[1,2]]/2 x[1] Prepend[Ry[theta].zHat,1],
	p[4]->Sqrt[s[1,2]]/2 x[2] Prepend[Ry[theta].Rz[alpha].Ry[beta].zHat,1]
};
unpackMomenta=p[i_,j__]:>p[i]+p[j];
momentumConservation[{a__},n_]:=p[a]->-p@@Delete[Range@n,{#}&/@{a}]//.unpackMomenta;
momentaReal=Append[fixedMomentaReal,momentumConservation[{5},5]/.fixedMomentaReal];
sToDot=s[i_,j_]:>2*dot[p[i],p[j]];
minkowskiMetric=DiagonalMatrix[{1,-1,-1,-1}];
performMomentaDots={dot[x_,y_]:>x.minkowskiMetric.y};
cosBeta=Cos[beta]->1+2*(1-x[1]-x[2])/(x[1] x[2]);
sinBeta=Sin[beta]->Sqrt[1-Power[Cos[beta],2]];
explicit[x_]:=x/.sToDot/.performMomentaDots/.momentaReal/.sinBeta/.cosBeta
preFactorNLO=sigmaZero*CF*alphaStrong/2/Pi;
Hval=3*(4*Pi)^(2*eps)*(-2+eps)*(-1+eps)*(s[1,2])^(-2*eps)/Gamma[4-2*eps];


Hval
PS3d4=PS3/.eps->0


Series[Hval,{eps,0,2}]


evaluateDiff[expr_,PS_,H_]:=Module[
	{start1,start2,tmp1,tmp2,differential},
	start1=explicit[expr]//Factor;
	start2=Factor[PS*start1]/.Sin[x_]:>Sqrt[1-Power[Cos[x],2]]/.Cos[theta]->cosTheta/.Cos[alpha]->cosAlpha;
	tmp1=Integrate[start2/d[cosAlpha],{cosAlpha,-1,1},Assumptions->Re[eps]<1/2]//Factor;
	tmp2=Integrate[tmp1/d[cosTheta],{cosTheta,-1,1},Assumptions->Re[eps]<1/2]//Factor;
	differential=Factor[tmp2/preFactorNLO/H]/.d[x[i_]]:>d[y[i]]/.x[i_]:>1-y[i]//FullSimplify;
	Return[differential];
	];
evaluateIncl[differential_]:=Module[
	{tmp,inclusive,expansion},
	tmp=Integrate[differential/d[y[2]],{y[2],0,1-y[1]},Assumptions->Re[eps]<0&&y[1]<1]//Factor;
	inclusive=Integrate[tmp/d[y[1]],{y[1],0,1},Assumptions->Re[eps]<0&&s[1,2]>0]//Factor;
	expansion=Series[inclusive,{eps,0,0}]//FullSimplify;
	Return[expansion];
	];
(*evaluateInclAlt[differential_]:=Module[
	{tmp,inclusive,expansion},
	tmp=Integrate[differential/d[y[1]],{y[1],0,1-y[2]},Assumptions->Re[eps]<0]//Factor;
	inclusive=Integrate[tmp/d[y[2]],{y[2],0,1},Assumptions->Re[eps]<0]//Factor;
	expansion=Series[inclusive,{eps,0,0}]//FullSimplify;
	Return[expansion];
	];*)
evaluate[expr_,PS_,H_]:=Module[
	{differential,inclusive},
	differential=evaluateDiff[expr,PS,H];
	Print[differential];
	inclusive=evaluateIncl[differential];
	Print[inclusive];
	Return[{differential,inclusive}];
	];


w[3]=s[3,5]/(s[3,4]+s[4,5]);
w[4]=s[4,5]/(s[3,4]+s[3,5]);
m3[expr_]:=expr/.s[i_,4]:>s[i,p4]/.s[i_,3]:>s[i,p3]/.
	s[i_,p4]:>s[i,4]*(1+w[3])/.s[i_,p3]:>s[i,3]-w[3]*s[i,4];
m4[expr_]:=expr/.s[i_,3]:>s[i,p3]/.s[i_,4]:>s[i,p4]/.
	s[i_,p3]->s[i,3]*(1+w[4])/.s[i_,p4]:>s[i,4]-w[4]*s[i,3];


(* ::Section::Closed:: *)
(*Phase space testing*)


PS3


PS3d4
vol3=s[1,2]/2^8/Pi^3
integrandPS3d4=PS3d4/( d[Cos[alpha]] d[Cos[theta]] d[x[1]] d[x[2]])/.Csc[alpha]->1/Sqrt[1-ca^2];
solPS3d4=Integrate[integrandPS3d4,{ca,-1,1},{ct,-1,1},{y[1],0,1},{y[2],0,1-y[1]}]


integrandPS3=PS3/( d[Cos[alpha]] d[Cos[theta]] d[x[1]] d[x[2]])/.
	Sin[alpha]->Sqrt[1-ca^2]/.Sin[theta]->Sqrt[1-ct^2]/.x[i_]:>1-y[i]


sol2=Integrate[integrandPS3,{ca,-1,1},{ct,-1,1},{y[1],0,1},{y[2],0,1-y[1]},Assumptions->Re[eps]<1/2]


sol2=sol2//FullSimplify


Series[sol2,{eps,0,1}]//FullSimplify


(* ::Section::Closed:: *)
(*Dim reg raw*)


realCoeff=sigmaZero*3*2^5*Pi^2*alphaStrong*CF/s[1,2]
realKin=(s[1,3]^2+s[1,4]^2+s[2,3]^2+s[2,4]^2)/(s[3,5]*s[4,5])


realDiffRaw=evaluateDiff[realCoeff*realKin,PS3,1]


realInclRaw=evaluateIncl[realDiffRaw]


(* ::Section::Closed:: *)
(*Dim reg*)


realCoeff=sigmaZero*3*2^5*Pi^2*alphaStrong*CF/s[1,2]
realKin=(s[1,3]^2+s[1,4]^2+s[2,3]^2+s[2,4]^2)/(s[3,5]*s[4,5])


realDiff=evaluateDiff[realCoeff*realKin,PS3,Hval]


realIncl=evaluateIncl[realDiff]


(* ::Section::Closed:: *)
(*Soft*)


realCoeff=sigmaZero*3*2^5*Pi^2*alphaStrong*CF/s[1,2];
softCoeff=realCoeff*2/s[1,2]
softKin=m3[(s[1,3]^2+s[1,4]^2)]*s[1,2]/s[3,5]/s[4,5]//FullSimplify


softDiff=evaluateDiff[softCoeff*softKin,PS3,Hval]


softTmp1=Integrate[softDiff/d[y[1]],{y[1],0,1-y[2]},Assumptions->Re[eps]<0]//Factor


softTmp2=Integrate[softTmp1/d[y[2]],{y[2],0,1},Assumptions->Re[eps]<0]//Factor


softIncl=Series[softTmp2,{eps,0,0}]


softResult=softIncl*Hval//FullSimplify


(* ::Section::Closed:: *)
(*(1-s_5)c_{45}*)


realCoeff=sigmaZero*3*2^5*Pi^2*alphaStrong*CF/s[1,2];
collCoeff=realCoeff/s[1,2];
z4[]:=s[3,4]/(s[3,4]+s[3,5]);
subCol45Kin=-m4[(s[1,3]^2+s[2,3]^2)]/s[4,5]*(1+z4[])


subCol45Diff=evaluateDiff[collCoeff*subCol45Kin,PS3,Hval]


subCol45Incl=evaluateIncl[subCol45Diff]


subCol45Result=subCol45Incl*Hval//FullSimplify


(* ::Section::Closed:: *)
(*(1-s_5)c_{35}*)


realCoeff=sigmaZero*3*2^5*Pi^2*alphaStrong*CF/s[1,2];
collCoeff=realCoeff/s[1,2];
z3[]:=s[3,4]/(s[3,4]+s[4,5]);
subCol35Kin=-m3[(s[1,4]^2+s[2,4]^2)]/s[3,5]*(1+z3[])


subCol35Diff=evaluateDiff[collCoeff*subCol35Kin,PS3,Hval]


subCol35Tmp1=Integrate[subCol35Diff/d[y[2]],{y[2],0,1-y[1]},Assumptions->Re[eps]<0&&Re[y[1]]>=0]//Factor


subCol35Tmp2=subCol35Tmp1//FullSimplify


subCol35Tmp3=Integrate[subCol35Tmp2/d[y[1]],{y[1],0,1}]


subCol35Result=subCol35Incl*Hval//FullSimplify


(* ::Section::Closed:: *)
(*Aim*)


realIncl


softIncl


subCol45Incl


(*regulated term = dim reg answer - global counterterms*)
aim=realIncl-softIncl-2*subCol45Incl//Simplify
aimN=N[aim]


(* ::Section::Closed:: *)
(*Regulated term analytic (intractable)*)


regCoeff=sigmaZero*3*2^5*Pi^2*alphaStrong*CF/s[1,2];
z3[]:=s[3,4]/(s[3,4]+s[4,5]);
z4[]:=s[3,4]/(s[3,4]+s[3,5]);
realKin=(s[1,3]^2+s[1,4]^2+s[2,3]^2+s[2,4]^2)/(s[3,5]*s[4,5]);
mappedSoft=m3[(s[2,4]^2+s[1,4]^2)*2*s[1,2]/s[3,5]/s[4,5]];
mappedSubCol45=m4[-(s[1,3]^2+s[2,3]^2)/s[4,5]*(1+z4[])];
mappedSubCol35=m3[-(s[1,4]^2+s[2,4]^2)/s[3,5]*(1+z3[])];
regMapKin=realKin-(mappedSoft+mappedSubCol45+mappedSubCol35)/s[1,2]//Simplify


{regDiff,regIncl}=evaluate[regCoeff*regMapKin,PS3d4,1];


(* ::Section:: *)
(*Regulated term numerical*)


regCoeff=sigmaZero*3*2^5*Pi^2*alphaStrong*CF/s[1,2];
includeCoeff=regCoeff/preFactorNLO


(*born[soft]*)
m3[(s[1,4]^2+s[2,4]^2)/(s12^2)]//Simplify//InputForm


(*born[4]*)
m4[(s[1,3]^2+s[2,3]^2)/(s12^2)]//Factor//InputForm


(*born[3]*)
m3[(s[1,4]^2+s[2,4]^2)/(s12^2)]//Factor//InputForm


(* measure=d[alpha]*d[theta]*d[x[3]]*d[x[4]]; *)
evalIntegrand[s12_?NumericQ,alpha_?NumericQ,theta_?NumericQ,y3_?NumericQ,y4_?NumericQ]:=Module[
	{A,integrand,sol,RyBeta,m,z,x,p,s,born},
	x[3]=1-y3;
	x[4]=1-y4;
	RyBeta=Ry[beta]/.Sin[beta]->Sqrt[1-Power[Cos[beta],2]]/.Cos[beta]->1+2*(1-x[3]-x[4])/(x[3]*x[4]);
	p[1]=Sqrt[s12]/2*Prepend[zHat,-1];
	p[2]=Sqrt[s12]/2*Prepend[-zHat,-1];
	p[3]=Sqrt[s12]/2*x[3]*Prepend[Ry[theta].zHat,1];
	p[4]=Sqrt[s12]/2*x[4]*Prepend[Ry[theta].Rz[alpha].RyBeta.zHat,1];
	p[5]=-p[1]-p[2]-p[3]-p[4];
	s[i_,j_]:=2*(p[i][[1]]*p[j][[1]]-p[i][[2]]*p[j][[2]]-p[i][[3]]*p[j][[3]]-p[i][[4]]*p[j][[4]]);
	m=s12*Sin[theta]/Power[4*Pi,4];
	A[r]=(s[1,3]^2+s[1,4]^2+s[2,3]^2+s[2,4]^2)/(s[3,5]*s[4,5]*s12);
	born[3]=((s[1, 4]^2 + s[2, 4]^2)*(s[3, 4] + s[3, 5] + s[4, 5])^2)/(s12^2*(s[3, 4] + s[4, 5])^2);
	A[sc35]=-born[3]/s[3,5]*(1+s[3,4]/(s[3,4]+s[4,5]));
	born[4]=((s[1, 3]^2 + s[2, 3]^2)*(s[3, 4] + s[3, 5] + s[4, 5])^2)/(s12^2*(s[3, 4] + s[3, 5])^2);
	A[sc45]=-born[4]/s[4,5]*(1+s[3,4]/(s[3,4]+s[3,5]));
	A[soft]=born[3]*2*s12/s[3,5]/s[4,5];
	integrand=A[r]-A[sc45]-A[sc35]-A[soft];
	sol=192*Pi^3/s12*integrand*m;
	Return[sol];
];


y3t=0.3;
y4t=0.4;
test=evalIntegrand[1.,0.1,0.2,y3t,y4t]
(*Show[{Plot[1-x,{x,0,1}],ListPlot[{{y3t,y4t}}]}]*)


sol = NIntegrate[evalIntegrand[1.,a,th,y3,y4],{a,0.,N[Pi]},{th,0.,N[Pi]},{y3,0.,1.},{y4,0.,1.-y3},Method->"AdaptiveMonteCarlo",PrecisionGoal->2];
Print[sol];

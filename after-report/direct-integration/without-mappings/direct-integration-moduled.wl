(* ::Package:: *)

(* ::Section::Closed:: *)
(*Setup*)


d[\[CapitalPhi]3]=(4^(-4 + 3*\[Epsilon])*Pi^(-7/2 + 2*\[Epsilon])*d[Cos[\[Alpha]]]*d[Cos[\[Theta]]]*d[x[1]]*d[x[2]]*s[1, 2]^(1 - 2*\[Epsilon])*Sin[\[Alpha]]^(-1 - 2*\[Epsilon]))/
 (Gamma[1/2 - \[Epsilon]]*Gamma[1 - \[Epsilon]]*Sin[\[Theta]]^(2*\[Epsilon])*((-1 + x[1])*(-1 + x[2])*(-1 + x[1] + x[2]))^\[Epsilon])


zHat ={0,0,1};
Ry[a_]:={{Cos[a],0,Sin[a]},{0,1,0},{-Sin[a],0,Cos[a]}};
Rz[a_]:={{Cos[a],-Sin[a],0},{Sin[a],Cos[a],0},{0,0,1}};
fixedMomentaReal={
	p[1]->Sqrt[s[1,2]]/2 Prepend[zHat,-1],
	p[2]->Sqrt[s[1,2]]/2 Prepend[-zHat,-1],
	p[3]->Sqrt[s[1,2]]/2 x[1] Prepend[Ry[\[Theta]].zHat,1],
	p[4]->Sqrt[s[1,2]]/2 x[2] Prepend[Ry[\[Theta]].Rz[\[Alpha]].Ry[\[Beta]].zHat,1]
};
unpackMomenta=p[i_,j__]:>p[i]+p[j];
momentumConservation[{a__},n_]:=p[a]->-p@@Delete[Range@n,{#}&/@{a}]//.unpackMomenta;
momentaReal=Append[fixedMomentaReal,momentumConservation[{5},5]/.fixedMomentaReal];
sToDot=s[i_,j_]:>2*dot[p[i],p[j]];
minkowskiMetric=DiagonalMatrix[{1,-1,-1,-1}];
performMomentaDots={dot[x_,y_]:>x.minkowskiMetric.y};
cosBeta=Cos[\[Beta]]->1+2*(1-x[1]-x[2])/(x[1] x[2]);
sinBeta=Sin[\[Beta]]->Sqrt[1-Power[Cos[\[Beta]],2]];
explicit[x_]:=x/.sToDot/.performMomentaDots/.momentaReal/.sinBeta/.cosBeta
preFactorNLO=\[Sigma]0*CF*\[Alpha]s/2/Pi;
Hval=3*(4 \[Pi])^(2\[Epsilon])*(-2+\[Epsilon])*(-1+\[Epsilon])*(s[1,2])^(-2\[Epsilon])/Gamma[4-2 \[Epsilon]];


d[\[CapitalPhi]3d4]=d[\[CapitalPhi]3]/.\[Epsilon]->0


evaluate[expr_,PS_,H_]:=Module[
	{start1,start2,tmp1,tmp2,tmp3,differential,inclusive,expansion,tmp},
	start1=explicit[expr]//Factor;
	start2=Factor[PS*start1]/.Sin[x_]:>Sqrt[1-Power[Cos[x],2]]/.Cos[\[Theta]]->c\[Theta]/.Cos[\[Alpha]]->c\[Alpha];
	tmp1=Integrate[start2/d[c\[Alpha]],{c\[Alpha],-1,1},Assumptions->Re[\[Epsilon]]<1/2]//Factor;
	tmp2=Integrate[tmp1/d[c\[Theta]],{c\[Theta],-1,1},Assumptions->Re[\[Epsilon]]<1/2]//Factor;
	differential=Factor[tmp2/preFactorNLO/H]/.d[x[i_]]:>d[y[i]]/.x[i_]:>1-y[i];
	Print[differential];
	tmp3=Integrate[differential/d[y[2]],{y[2],0,1-y[1]},Assumptions->Re[\[Epsilon]]<0&&y[1]<1];
	inclusive=Integrate[tmp3/d[y[1]],{y[1],0,1},Assumptions->Re[\[Epsilon]]<0&&s[1,2]>0];
	expansion=Series[inclusive,{\[Epsilon],0,0}];
	Print[expansion];
	Return[{differential,expansion}];
	];


(* ::Section::Closed:: *)
(*Dim reg*)


realCoeff=\[Sigma]0*3*2^5*Pi^2*\[Alpha]s*CF/s[1,2]
integrandReal=(s[1,3]^2+s[1,4]^2+s[2,3]^2+s[2,4]^2)/(s[3,5]*s[4,5])


{realDiff,realResult}=evaluate[realCoeff*integrandReal,d[\[CapitalPhi]3],Hval];


(* ::Section::Closed:: *)
(*Soft*)


softCoeff=realCoeff*2/s[1,2]
integrandSoft=(s[1,3]^2+s[1,4]^2)*s[3,4]/s[3,5]/s[4,5]


{softDiff,softResult}=evaluate[softCoeff*integrandSoft,d[\[CapitalPhi]3],Hval];


(* ::Section::Closed:: *)
(*(1-s_5)c_{45}*)


collCoeff=realCoeff/s[1,2];
z4[]:=s[3,4]/(s[3,4]+s[3,5]);
integrandCollinear45woS=(s[1,3]^2+s[2,3]^2)/s[4,5]*(-1-z4[])


{col45woSdiff,col45woSresult}=evaluate[collCoeff*integrandCollinear45woS,d[\[CapitalPhi]3],Hval];


(* ::Section::Closed:: *)
(*(1-s_5)c_{35}*)


collCoeff=realCoeff/s[1,2];
z3[]:=s[3,4]/(s[3,4]+s[4,5]);
integrandCollinear35woS=(s[1,4]^2+s[2,4]^2)/s[3,5]*(-1-z3[])


{col35woSdiff,col35woSresult}=evaluate[collCoeff*integrandCollinear35woS,d[\[CapitalPhi]3],Hval];


(* ::Section:: *)
(*Aim*)


realResult


softResult


col45woSresult=col45woSresult//FullSimplify


col35woSresult


(*regulated term = dim reg answer - global counterterms*)
aim=realResult-softResult-col45woSresult-col35woSresult//Simplify
aimN=N[aim]


(* ::Section:: *)
(*Regulated term analytic*)


regCoeff=\[Sigma]0*3*2^5*Pi^2*\[Alpha]s*CF/s[1,2];
z3[]:=s[3,4]/(s[3,4]+s[4,5]);
z4[]:=s[3,4]/(s[3,4]+s[3,5]);
regKinematics=integrandReal-(integrandSoft*2+integrandCollinear35woS+integrandCollinear45woS)/s[1,2]


{regDiff,regResult}=evaluate[regCoeff*regKinematics,d[\[CapitalPhi]3d4],1];


(* ::Section::Closed:: *)
(*Regulated term*)


(* measure=d[\[Alpha]]*d[\[Theta]]*d[x[3]]*d[x[4]]; *)
evalIntegrand[s12_?NumericQ,\[Alpha]_?NumericQ,\[Theta]_?NumericQ,y3_?NumericQ,y4_?NumericQ]:=Module[
	{Ar,Ac45,Ac35,As,integrand,sol,c\[Beta],s\[Beta],m,z,J,x,p,s,born,j,eikonal},
	x[3]=1-y3;
	x[4]=1-y4;
	c\[Beta]=1+2*(1-x[3]-x[4])/(x[3]*x[4]);
	s\[Beta]=Sqrt[1-Power[c\[Beta],2]];
	RyBeta=Ry[\[Beta]]/.Sin[\[Beta]]->s\[Beta]/.Cos[\[Beta]]->c\[Beta];
	p[1]=Sqrt[s12]/2*Prepend[zHat,-1];
	p[2]=Sqrt[s12]/2*Prepend[-zHat,-1];
	p[3]=Sqrt[s12]/2*x[3]*Prepend[Ry[\[Theta]].zHat,1];
	p[4]=Sqrt[s12]/2*x[4]*Prepend[Ry[\[Theta]].Rz[\[Alpha]].RyBeta.zHat,1];
	p[5]=-p[1]-p[2]-p[3]-p[4];
	s[i_,j_]:=2*(p[i][[1]]*p[j][[1]]-p[i][[2]]*p[j][[2]]-p[i][[3]]*p[j][[3]]-p[i][[4]]*p[j][[4]]);
	z[3]=s[3,4]/(s[3,4]+s[4,5]);
	z[4]=s[3,4]/(s[3,4]+s[3,5]);
	m=s12*Sin[\[Theta]]/Power[4*Pi,4];
	Ar=(s[1,3]^2+s[1,4]^2+s[2,3]^2+s[2,4]^2)/(s[3,5]*s[4,5]*s12);
	born[3]=(s[1,4]^2+s[2,4]^2)/(s12^2);
	Ac35=-born[3]/s[3,5]*(1+z[3]);
	eikonal=s[3,4]/(s[3,5]*s[4,5]);
	born[soft]=(s[1,3]^2+s[1,4]^2)/(s12^2);
	As=2*born[soft]*eikonal;
	born[4]=(s[1,3]^2+s[2,3]^2)/(s12^2);
	Ac45=-born[4]/s[4,5]*(1+z[4]);
	integrand=Ar-Ac45-Ac35-As;
	sol=integrand*m;
	Return[sol];
];


y3t=0.3;
y4t=0.4;
test=evalIntegrand[1.,0.1,0.2,y3t,y4t]
Show[{Plot[1-x,{x,0,1}],ListPlot[{{y3t,y4t}}]}];


0.00005394067503841667`


sol = NIntegrate[evalIntegrand[1.,a,th,y3,y4],{a,0.,N[Pi]},{th,0.,N[Pi]},{y3,0.,1.},{y4,0.,1.-y3},Method->"AdaptiveMonteCarlo",PrecisionGoal->2];
Print[sol];


aimN

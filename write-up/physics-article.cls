\NeedsTeXFormat{LaTeX2e}
\ProvidesClass{physics-article}[2019/05/08 Ryan's custom physics article class]

\LoadClass[10pt, a4paper]{article}

\usepackage{mathtools,amssymb,slashed,physics,graphicx}
\usepackage[colorlinks,linkcolor=blue,urlcolor=blue,citecolor=blue]{hyperref}
\usepackage[capitalize]{cleveref}
\usepackage[compat=1.1.0]{tikz-feynman}

\makeatletter
\g@addto@macro\bfseries{\boldmath}
\makeatother

\crefname{section}{Sec.}{Secs.}
\Crefname{section}{Section}{Sections}

\newcommand{\alphaqed}{{\alpha}}
\newcommand{\alphaqcd}{{\alpha_\mathrm{s}}}
\newcommand{\couplingqcd}{{\mathrm{g}_\mathrm{s}}}
\newcommand{\couplingqed}{{\mathrm{e}}}
\newcommand{\fractionalcharge}{{\mathrm{Q}_\mathrm{q}}}
\newcommand{\amplitude}{\mathcal{A}}
\newcommand{\realamplitude}{\amplitude_{\mathrm{R}}}
\newcommand{\virtualamplitude}{\amplitude_{\mathrm{V}}}
\newcommand{\numbercolours}{{\mathrm{N}_\mathrm{c}}}
\newcommand{\casimirfundamental}{{\mathrm{C}_\mathrm{F}}}
\newcommand{\spindimension}{{d_s}}
\newcommand{\crosssection}{\sigma}
\newcommand{\realcrosssection}{\crosssection_{\mathrm{R}}}
\newcommand{\virtualcrosssection}{\crosssection_{\mathrm{V}}}
\newcommand{\sigmaLO}{\crosssection_0}
\newcommand{\sigmaNLO}{\crosssection^{\mathrm{(NLO)}}}

\newcommand{\imi}{{\mathrm{i}\mkern1mu}}
\newcommand{\s}[1]{{\,\text{s}_{#1}}}
\newcommand{\sh}[1]{{\,\hat{\text{s}}_{#1}}}
\newcommand{\sumsquare}[1]{\expval{\abs{#1}^2}}
\newcommand{\deltaplus}[1]{\delta^{(+)}\qty(#1)}

\newcommand{\cutmargin}{1ex}
\newcommand{\cut}[2]{\draw [dashed] ($(#1) + (0, \cutmargin)$) -- ($(#2) + (0, -\cutmargin)$);}

\newif\ifrenderdiagrams
% Set true or false here:
\renderdiagramstrue

\newcommand{\newdiagram}[2]{
	\ifrenderdiagrams
	\newcommand{#1}{#2}
	\else
	\newcommand{#1}{\begin{center}[Diagram]\\\end{center}}
	\fi
}
